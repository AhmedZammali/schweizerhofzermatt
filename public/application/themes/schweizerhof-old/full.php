
<?php  defined('C5_EXECUTE') or die("Access Denied."); 
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();

?>
<?php $this->inc('elements/header.php'); ?>
<div id="barba-wrapper">
      <div class="barba-container">
        <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="homepage"></div>
        <!-- cover-->
        <!-- if cover_images.length-->
        <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
        <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
        <!--       +image(cover_image, 1440, 900)-->
        <!--     .cover-hover__images(data-role="cover-hover__images" )-->
        <!--       for image in cover_images-->
        <!--         -var {src} = image-->
        <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
        <!--       .cover-hover__stripes-->
        <!--         .grid-container.full-->
        <!--           .grid-x-->
        <!--             for image in cover_images-->
        <!--               .cell.small-2-->
        <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
    <section class="cover cover_full" data-title="Cover banner" data-bg="dark" data-animation="section" data-role="cover-slider" id="section-cover">
      <div class="cover__bg cover__bg_webgl-image" data-role="main-image" data-cover-slider="scene">
      	<?php
      	echo "asd"; 
      		print_r($theme->getGlobalAreaImage());
 /*     		$bannerImage = new GlobalArea('Banner Image');
      		$bannerImage->display();*/
      	?>
        <div class="visually-hidden" data-src="images/cover/1440x900/image-2_1440x900.jpg" data-type="img" data-cover-slider="slide"></div>
      </div>
      <div class="cover__overlay layers__center">
        <div class="grid-container full">
          <div class="grid-x grid-margin-x">
            <div class="cell large-10 large-offset-1">
              <h1 class="visually-hidden">Explore Zermatt like you never did</h1>
              <div class="cover-title cover-title_light" data-role="main-title">
                <div class="cover-title__title"><span class="h1 cover-hover__title cover-hover__title_main" aria-hidden="true" data-split-text="true" data-role="cover-hover__main-title">Explore</span>
                  <!-- for image in cover_images-->
                  <!--   span.h1.cover-hover__title(aria-hidden="true" data-split-text="true" data-role="cover-hover__title")= image.title-->
                </div>
                <div class="cover-title__description"><span class="cover-hover__description cover-hover__description_main" data-split-text="true" aria-hidden="true" data-role="cover-hover__main-description">Zermatt like you never did</span>
                  <!-- for image in cover_images-->
                  <!--   span(data-split-text="true" aria-hidden="true" data-role="cover-hover__description").cover-hover__description= image.description-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="cover__social">
        <div class="grid-container full">
          <div class="grid-x grid-margin-x">
            <div class="cell small-1 flex-container flex-dir-column align-right">
              <div data-role="hash-nav-container"></div>
              <ul class="contacts">
                <li class="contacts__item contacts-item">
                  <div class="contacts-item__content"><a href="#" data-custom-hover="true" title="Facebook">Facebook</a></div>
                </li>
                <li class="contacts__item contacts-item">
                  <div class="contacts-item__content"><a href="#" data-custom-hover="true" title="Instagram">Instagram</a></div>
                </li>
                <li class="contacts__item contacts-item">
                  <div class="contacts-item__content"><a href="#" data-custom-hover="true" title="Twitter">Twitter</a></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="scroll-action"></div>
    </section>
        <main class="content">
          <section class="section indent-inner-top-xl indent-inner-bot-xl sm-indent-inner-top-xl" id="section-about" data-title="Explore the hotel" data-bg="light">
            <div class="overlay-image overlay-image_bl overlay-image_deer">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 199x381, retina @2x - 398x762 -->
              <!-- please add size attributes to the image--><img data-src="images/overlays/deer_199x381.png" data-srcset="images/overlays/deer_199x381.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="199" height="381">
            </div>
            <div class="grid-container">
              <div class="grid-x grid-margin-x">
                <div class="cell small-10 small-offset-2 medium-offset-2" data-role="card-overflow-text">
                  <div class="simple-slider" data-role="simple-slider-container">
                    <div class="position-relative" data-role="slider-container">
                      <div class="swiper-controls">
                        <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                        <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                      </div>
                      <div class="grid-container">
                        <div class="grid-x grid-margin-x">
                          <div class="cell">
                            <div class="swiper-container" data-slider="simple-slider">
                              <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                  <div class="card card_800">
                                    <div class="card__image">
                                      <div class="image-inner" data-role="slide-inner">
                                        <!-- Attention: responsive image used. Require srcset -->
                                        <!-- image sizes: default - 1170x800, retina @2x - 2340x1600 --><img class="swiper-lazy" data-src="images/cards/1170x800/image-1_1170x800.jpg" data-srcset="images/cards/1170x800/image-1_1170x800.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="swiper-slide">
                                  <div class="card card_800">
                                    <div class="card__image">
                                      <div class="image-inner" data-role="slide-inner">
                                        <!-- Attention: responsive image used. Require srcset -->
                                        <!-- image sizes: default - 1170x800, retina @2x - 2340x1600 --><img class="swiper-lazy" data-src="images/cards/1170x800/image-2_1170x800.jpg" data-srcset="images/cards/1170x800/image-2_1170x800.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="swiper-slide">
                                  <div class="card card_800">
                                    <div class="card__image">
                                      <div class="image-inner" data-role="slide-inner">
                                        <!-- Attention: responsive image used. Require srcset -->
                                        <!-- image sizes: default - 1170x800, retina @2x - 2340x1600 --><img class="swiper-lazy" data-src="images/cards/1170x800/image-3_1170x800.jpg" data-srcset="images/cards/1170x800/image-3_1170x800.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-role="overflow-content">
                    <p data-role="overflow-text">sch<br>weiz<br>erh<br>of</p>
                  </div>
                </div>
              </div>
              <div class="grid-x grid-margin-x indent-inner-top-l" data-animation="section" data-trigger-offset="-200">
                <div class="cell large-4 large-offset-3 medium-10 medium-offset-1">
                  <div class="styling-dots indent-m" data-animation="fade-in-up">
                    <div class="styling-dots__dot"></div>
                    <div class="styling-dots__dot"></div>
                    <div class="styling-dots__dot"></div>
                  </div>
                  <div class="editable" data-animation="fade-in-up_text">
                    <p>Located in the very heart of Zermatt, this historic lifestyle address will become a lively hub during and after ski or hike. Thoroughly thought out for the sport addicts, it will also be a hip & amp; chic retreat for families and DINKYS looking for a sexy and memorable experience.</p>
                  </div>
                  <div class="buttons" data-animation="fade-in-up"><a href="#" data-role="dynamic-hover-arrow" data-custom-hover="true">             Discover the <br> Schweizerhof Hotel                  </a></div>
                </div>
              </div>
            </div>
          </section>
          <section class="section indent-inner-bot-xl" id="section-rooms" data-title="Our rooms" data-bg="light">
            <div class="overlay-image overlay-image_tr">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 682x407, retina @2x - 1364x814 -->
              <!-- please add size attributes to the image--><img data-src="images/overlays/matterhorn_682x407.png" data-srcset="images/overlays/matterhorn_682x407.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="682" height="407">
            </div>
            <div class="overlay-image overlay-image_bl overlay-image_matterhorn">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 509x614, retina @2x - 1018x1228 -->
              <!-- please add size attributes to the image--><img data-src="images/overlays/zermatt-2_509x614.png" data-srcset="images/overlays/zermatt-2_509x614.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="509" height="614">
            </div>
            <div class="overlay-image overlay-image_br overlay-image_birds">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 322x224, retina @2x - 644x448 -->
              <!-- please add size attributes to the image--><img data-src="images/overlays/birds_322x224.png" data-srcset="images/overlays/birds_322x224.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="322" height="224">
            </div>
            <div class="grid-container indent-l" data-animation="section">
              <div class="grid-x grid-margin-x">
                <div class="cell large-5 large-offset-1">
                  <div class="eyebrow" data-animation="title"><span data-split-text="true">/ explore</span></div>
                  <div class="title" data-animation="title">
                    <h2 data-split-text="true">Our Rooms</h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="grid-container" data-role="slider-mini-gallery slider-container">
              <div class="grid-x position-relative indent-l">
                <div class="swiper-controls">
                  <div class="swiper-button swiper-button-next nav nav_next visible" data-custom-hover="true" data-hover-type="nav next"></div>
                  <div class="swiper-button swiper-button-prev nav nav_prev visible" data-custom-hover="true" data-hover-type="nav prev"></div>
                </div>
                <div class="cell small-8" data-role="mini-gallery-main">
                  <div class="position-relative" data-role="slider-container">
                    <div class="grid-container">
                      <div class="grid-x grid-margin-x">
                        <div class="cell">
                          <div class="swiper-container" data-slider="gallery-mini_main">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">
                                <div class="card card_680">
                                  <div class="card__image">
                                    <div class="image-inner" data-role="slide-inner">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 930x680, retina @2x - 1860x1360 --><img class="swiper-lazy" data-src="images/cards/930x680/image-1_930x680.jpg" data-srcset="images/cards/930x680/image-1_930x680.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="card card_680">
                                  <div class="card__image">
                                    <div class="image-inner" data-role="slide-inner">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 930x680, retina @2x - 1860x1360 --><img class="swiper-lazy" data-src="images/cards/930x680/image-2_930x680.jpg" data-srcset="images/cards/930x680/image-2_930x680.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="card card_680">
                                  <div class="card__image">
                                    <div class="image-inner" data-role="slide-inner">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 930x680, retina @2x - 1860x1360 --><img class="swiper-lazy" data-src="images/cards/930x680/image-3_930x680.jpg" data-srcset="images/cards/930x680/image-3_930x680.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="cell small-4" data-role="mini-gallery-sub">
                  <div class="position-relative" data-role="slider-container">
                    <div class="grid-container">
                      <div class="grid-x grid-margin-x">
                        <div class="cell">
                          <div class="swiper-container" data-slider="gallery-mini_sub">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">
                                <div class="card card_square">
                                  <div class="card__image">
                                    <div class="image-inner" data-role="slide-inner">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 450x450, retina @2x - 900x900 --><img class="swiper-lazy" data-src="images/cards/450x450/image-1_450x450.jpg" data-srcset="images/cards/450x450/image-1_450x450.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="card card_square">
                                  <div class="card__image">
                                    <div class="image-inner" data-role="slide-inner">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 450x450, retina @2x - 900x900 --><img class="swiper-lazy" data-src="images/cards/450x450/image-2_450x450.jpg" data-srcset="images/cards/450x450/image-2_450x450.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="card card_square">
                                  <div class="card__image">
                                    <div class="image-inner" data-role="slide-inner">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 450x450, retina @2x - 900x900 --><img class="swiper-lazy" data-src="images/cards/450x450/image-3_450x450.jpg" data-srcset="images/cards/450x450/image-3_450x450.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-x grid-margin-x">
                <!--.cell.large-4.large-offset-2(data-animation="fade-in-up")-->
                <div class="cell large-4 large-offset-8">
                  <div class="mini-gallery__titles layers">
                    <div class="mini-gallery__title" data-role="slide-title">
                      <div class="title">
                        <h4>Executive Luxury Suite</h4>
                      </div>
                      <div class="editable">
                        <p>
                          <!--  capitalize the first letter of the first word;-->Fusce eget urna mauris tincidunt sem sed arcu 
                        </p>
                      </div>
                      <div class="buttons"><a href="single-room.html" data-role="dynamic-hover-arrow" data-custom-hover="true">Explore more</a></div>
                    </div>
                    <div class="mini-gallery__title" data-role="slide-title">
                      <div class="title">
                        <h4>Double Suite with Matterhorn view</h4>
                      </div>
                      <div class="editable">
                        <p>
                          <!--  capitalize the first letter of the first word;-->Donec vitae arcu donec ipsum massa ullamcorper in 
                        </p>
                      </div>
                      <div class="buttons"><a href="single-room.html" data-role="dynamic-hover-arrow" data-custom-hover="true">Explore more</a></div>
                    </div>
                    <div class="mini-gallery__title" data-role="slide-title">
                      <div class="title">
                        <h4>Luxury Single Suite</h4>
                      </div>
                      <div class="editable">
                        <p>
                          <!--  capitalize the first letter of the first word;-->Donec ipsum massa ullamcorper in auctor et scelerisque 
                        </p>
                      </div>
                      <div class="buttons"><a href="single-room.html" data-role="dynamic-hover-arrow" data-custom-hover="true">Explore more</a></div>
                    </div>
                  </div>
                </div>
                <div class="cell large-8 large-offset-2">
                  <div class="mini-gallery__info" data-animation="fade-in-up">
                    <div class="slide-info">
                      <div class="swiper-pagination" data-role="slider-pagination"></div>
                      <div class="swiper-progress" data-role="progress"><span class="line"></span></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="section bg-gray indent-inner-top-l indent-inner-bot-l indent-xl" id="section-contact" data-title="Contact us" data-bg="light" data-animation="section">
            <!--div(data-role="particles")-->
            <div class="grid-container">
              <div class="grid-x grid-margin-x">
                <div class="cell large-4 large-offset-4 flex-container align-center indent-inner-bot-m">
                  <div class="eyebrow" data-animation="fade-in-up">
                    <h2 class="visually-hidden">Book your stay</h2>
                    <p>/ book your stay</p>
                  </div>
                </div>
                <div class="cell large-8 large-offset-2">
                  <form class="form form_text" action="#" method="POST">
                    <fieldset class="fieldset form__content-block">
                      <div class="form__row" data-animation="fade-in-up_text">Hello Schweizerhof,</div>
                      <div class="form__row" data-animation="fade-in-up_text">I would like to book
                        <div class="input-block input-block_null input-block_select input-block_dynamic-width" data-animation="fade-in-up" data-role="input-block" data-type="select" data-custom-hover="true" data-hover-text="Chose">
                          <div class="input-block__input input-block__input_dropdown input-block__input_custom-select">
                            <select class="input" id="input-block-0" name="input-block-0" data-role="custom-select input">
                              <option value="Room">Room</option>
                              <option value="Restaurant">Restaurant</option>
                              <option value="SPA">SPA</option>
                            </select><span class="input-decoration"></span>
                          </div>
                          <div class="input-block__error" data-role="error"></div>
                        </div>for
                        <div class="input-block input-block_null input-block_select input-block_dynamic-width" data-animation="fade-in-up" data-role="input-block" data-type="select" data-custom-hover="true" data-hover-text="Chose">
                          <div class="input-block__input input-block__input_dropdown input-block__input_custom-select">
                            <select class="input" id="input-block-1" name="input-block-1" data-role="custom-select input">
                              <option value="1 adult">1 adult</option>
                              <option value="2 adults">2 adults</option>
                              <option value="3 adults">3 adults</option>
                              <option value="4 adults">4 adults</option>
                              <option value="5 adults">5 adults</option>
                            </select><span class="input-decoration"></span>
                          </div>
                          <div class="input-block__error" data-role="error"></div>
                        </div>and
                        <div class="input-block input-block_null input-block_select input-block_dynamic-width" data-animation="fade-in-up" data-role="input-block" data-type="select" data-custom-hover="true" data-hover-text="Chose">
                          <div class="input-block__input input-block__input_dropdown input-block__input_custom-select">
                            <select class="input" id="input-block-2" name="input-block-2" data-role="custom-select input">
                              <option value="1 kid">1 kid</option>
                              <option value="2 kids">2 kids</option>
                              <option value="3 kids">3 kids</option>
                              <option value="4 kids">4 kids</option>
                              <option value="5 kids">5 kids</option>
                            </select><span class="input-decoration"></span>
                          </div>
                          <div class="input-block__error" data-role="error"></div>
                        </div>
                      </div>
                      <div class="form__row" data-animation="fade-in-up_text">Check-In
                        <div class="input-block input-block_null input-block_date" data-animation="fade-in-up" data-role="input-block" data-type="date" data-required="true" data-custom-hover="true" data-hover-text="Chose">
                          <div class="input-block__input">
                            <input class="input" id="input-block-3" name="input-block-3" data-role="input" autocomplete="off" type="date" value="2018-11-29" min="2018-11-29" data-min="2018-11-29" data-connect-with="check-out" required="true">
                            <!--type= validation || type || 'text'--><span class="input-decoration"></span>
                          </div>
                          <div class="input-block__error" data-role="error"></div>
                        </div>Check-out
                        <div class="input-block input-block_null input-block_date" data-animation="fade-in-up" data-role="input-block" data-type="date" data-required="true" data-custom-hover="true" data-hover-text="Chose">
                          <div class="input-block__input">
                            <input class="input" id="input-block-4" name="input-block-4" data-role="input" autocomplete="off" type="date" value="2018-11-30" min="2018-11-30" data-min="2018-11-30" data-connect="check-out" required="true">
                            <!--type= validation || type || 'text'--><span class="input-decoration"></span>
                          </div>
                          <div class="input-block__error" data-role="error"></div>
                        </div>
                      </div>
                    </fieldset>
                    <div class="buttons buttons_center" data-animation="fade-in-up">
                      <button class="button button_border" type="submit" title="Check Availability" data-custom-hover="true" data-hover-text="Check"><span>Check Availability</span></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </section>
          <section class="section indent-inner-bot-xl" id="section-offers" data-title="Our Offers" data-bg="light" data-animation="section">
            <div class="overlay-image overlay-image_br">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 610x414, retina @2x - 1220x828 -->
              <!-- please add size attributes to the image--><img data-src="images/overlays/zermatt_610x414.png" data-srcset="images/overlays/zermatt_610x414.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="610" height="414">
            </div>
            <div class="grid-container indent-l">
              <div class="grid-x grid-margin-x">
                <div class="cell large-5 large-offset-1">
                  <div class="eyebrow" data-animation="title"><span data-split-text="true">/ explore</span></div>
                  <div class="title" data-animation="title">
                    <h2 data-split-text="true">Offers & Packages</h2>
                  </div>
                </div>
                <div class="cell large-6 flex-container align-bottom" data-animation="fade-in-right">
                  <div class="tab-extra-link tab-extra-link_bottom"><a href="offers.html" data-custom-hover="true">Explore All Offers</a></div>
                </div>
              </div>
            </div>
            <div class="slider-diff-indents">
              <div class="position-relative" data-role="slider-container">
                <div class="swiper-controls">
                  <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                  <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                </div>
                <div class="grid-container">
                  <div class="grid-x grid-margin-x">
                    <div class="cell  small-10 small-offset-1 medium-offset-1">
                      <div class="swiper-container" data-slider="another-items">
                        <div class="swiper-wrapper">
                          <div class="swiper-pagination"></div>
                          <div class="swiper-slide">
                            <div class="card card_simple card_650" data-animation="fade-in-up" lazy>
                              <div class="card__image">
                                <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                                  <!-- Attention: responsive image used. Require srcset -->
                                  <!-- image sizes: default - 570x650, retina @2x - 1140x1300 --><img class="swiper-lazy" data-src="images/cards/570x650/image-10_570x650.jpg" data-srcset="images/cards/570x650/image-10_570x650.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                </div>
                                <div class="swiper-lazy-preloader"></div>
                              </div>
                              <div class="card__content">
                                <div class="card__top"></div>
                                <div class="card__label">
                                  <h5>Week-End Suite Deals</h5>
                                </div>
                                <div class="card__description">
                                  <div class="editable editable_text-sm">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->Nulla est nullam rhoncus aliquam metus praesent dapibus integer malesuada etiam sapien elit consequat eget 
                                    </p>
                                  </div>
                                </div>
                                <div class="card__buttons">
                                  <div class="buttons"><a class="button button_simple button_with-icon-left" href="single-offer.html" title="Week-End Suite Deals" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><span>Discover</span></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="swiper-slide">
                            <div class="card card_simple card_380" data-animation="fade-in-up" lazy>
                              <div class="card__image">
                                <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                                  <!-- Attention: responsive image used. Require srcset -->
                                  <!-- image sizes: default - 570x380, retina @2x - 1140x760 --><img class="swiper-lazy" data-src="images/cards/570x380/image-16_570x380.jpg" data-srcset="images/cards/570x380/image-16_570x380.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                </div>
                                <div class="swiper-lazy-preloader"></div>
                              </div>
                              <div class="card__content">
                                <div class="card__top"></div>
                                <div class="card__label">
                                  <h5>Stay Longer &amp; Save</h5>
                                </div>
                                <div class="card__description">
                                  <div class="editable editable_text-sm">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->In dapibus augue non sapien praesent in mauris eu tortor porttitor accumsan suspendisse potenti duis 
                                    </p>
                                  </div>
                                </div>
                                <div class="card__buttons">
                                  <div class="buttons"><a class="button button_simple button_with-icon-left" href="single-offer.html" title="Stay Longer &amp; Save" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><span>Discover</span></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="swiper-slide">
                            <div class="card card_simple card_650" data-animation="fade-in-up" lazy>
                              <div class="card__image">
                                <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                                  <!-- Attention: responsive image used. Require srcset -->
                                  <!-- image sizes: default - 570x650, retina @2x - 1140x1300 --><img class="swiper-lazy" data-src="images/cards/570x650/image-11_570x650.jpg" data-srcset="images/cards/570x650/image-11_570x650.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                </div>
                                <div class="swiper-lazy-preloader"></div>
                              </div>
                              <div class="card__content">
                                <div class="card__top"></div>
                                <div class="card__label">
                                  <h5>Traditional Zermatt Offer</h5>
                                </div>
                                <div class="card__description">
                                  <div class="editable editable_text-sm">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->In vulputate urna eu arcu suspendisse nisl fusce consectetuer risus a nunc sed convallis magna 
                                    </p>
                                  </div>
                                </div>
                                <div class="card__buttons">
                                  <div class="buttons"><a class="button button_simple button_with-icon-left" href="single-offer.html" title="Traditional Zermatt Offer" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><span>Discover</span></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="swiper-slide">
                            <div class="card card_simple card_380" data-animation="fade-in-up" lazy>
                              <div class="card__image">
                                <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                                  <!-- Attention: responsive image used. Require srcset -->
                                  <!-- image sizes: default - 570x380, retina @2x - 1140x760 --><img class="swiper-lazy" data-src="images/cards/570x380/image-2_570x380.jpg" data-srcset="images/cards/570x380/image-2_570x380.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                </div>
                                <div class="swiper-lazy-preloader"></div>
                              </div>
                              <div class="card__content">
                                <div class="card__top"></div>
                                <div class="card__label">
                                  <h5>Swiss Mountains Chill</h5>
                                </div>
                                <div class="card__description">
                                  <div class="editable editable_text-sm">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->Sed elit dui pellentesque a faucibus vel interdum nec diam nullam at arcu a est 
                                    </p>
                                  </div>
                                </div>
                                <div class="card__buttons">
                                  <div class="buttons"><a class="button button_simple button_with-icon-left" href="single-offer.html" title="Swiss Mountains Chill" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><span>Discover</span></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="swiper-slide">
                            <div class="card card_simple card_650" data-animation="fade-in-up" lazy>
                              <div class="card__image">
                                <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                                  <!-- Attention: responsive image used. Require srcset -->
                                  <!-- image sizes: default - 570x650, retina @2x - 1140x1300 --><img class="swiper-lazy" data-src="images/cards/570x650/image-1_570x650.jpg" data-srcset="images/cards/570x650/image-1_570x650.jpg 2x" draggable="false" alt="Schweizerhof Zermatt">
                                </div>
                                <div class="swiper-lazy-preloader"></div>
                              </div>
                              <div class="card__content">
                                <div class="card__top"></div>
                                <div class="card__label">
                                  <h5>Amazing Ski</h5>
                                </div>
                                <div class="card__description">
                                  <div class="editable editable_text-sm">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->In convallis fusce aliquam vestibulum ipsum in dapibus augue non sapien aenean placerat mauris pretium 
                                    </p>
                                  </div>
                                </div>
                                <div class="card__buttons">
                                  <div class="buttons"><a class="button button_simple button_with-icon-left" href="single-offer.html" title="Amazing Ski" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><span>Discover</span></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="section indent-l bg-gray" id="section-services" data-title="Our services" data-bg="light" data-animation="section">
            <div class="home-services layers" data-role="home-services">
              <div class="layers__center">
                <div class="layers">
                  <div class="position-relative" data-role="slider-container">
                    <div class="swiper-controls">
                      <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                      <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                    </div>
                    <div class="grid-container">
                      <div class="grid-x grid-margin-x">
                        <div class="cell small-6 large-offset-3 small-offset-1 medium-offset-1">
                          <div class="swiper-container" data-slider="home-services-titles">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide" data-role="services-title" aria-hidden="true" data-custom-hover="true" data-hover-text="View">
                                <div class="home-services__close"><i class="icon-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></i></div>
                                <div class="home-services__title" data-role="services-animated-title">
                                  <div class="eyebrow"><span>/ explore</span></div>
                                  <div class="title" data-swiper-parallax="-35%">
                                    <h2>Party</h2>
                                  </div>
                                </div>
                                <div class="home-services__content">
                                  <div class="editable editable_text-left" data-limit-rows="4">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->In sem justo commodo ut suscipit at pharetra vitae orci nulla quis diam aliquam ante aliquam erat volutpat pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas integer imperdiet lectus quis justo 
                                    </p>
                                  </div>
                                  <div class="buttons"><a class="is-accent" href="party.html" title="Party" data-role="dynamic-hover-arrow" data-custom-hover="true">Explore more</a></div>
                                </div>
                              </div>
                              <div class="swiper-slide" data-role="services-title" aria-hidden="true" data-custom-hover="true" data-hover-text="View">
                                <div class="home-services__close"><i class="icon-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></i></div>
                                <div class="home-services__title" data-role="services-animated-title">
                                  <div class="eyebrow"><span>/ explore</span></div>
                                  <div class="title" data-swiper-parallax="-35%">
                                    <h2>Chill</h2>
                                  </div>
                                </div>
                                <div class="home-services__content">
                                  <div class="editable editable_text-left" data-limit-rows="4">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->Nunc auctor praesent vitae arcu tempor neque lacinia pretium integer pellentesque quam vel velit fusce consectetuer risus a nunc fusce tellus maecenas ipsum velit consectetuer eu lobortis ut dictum at dui fusce suscipit libero eget elit phasellus et lorem id felis nonummy placerat phasellus rhoncus 
                                    </p>
                                  </div>
                                  <div class="buttons"><a class="is-accent" href="chill.html" title="Chill" data-role="dynamic-hover-arrow" data-custom-hover="true">Explore more</a></div>
                                </div>
                              </div>
                              <div class="swiper-slide" data-role="services-title" aria-hidden="true" data-custom-hover="true" data-hover-text="View">
                                <div class="home-services__close"><i class="icon-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></i></div>
                                <div class="home-services__title" data-role="services-animated-title">
                                  <div class="eyebrow"><span>/ explore</span></div>
                                  <div class="title" data-swiper-parallax="-35%">
                                    <h2>Shop</h2>
                                  </div>
                                </div>
                                <div class="home-services__content">
                                  <div class="editable editable_text-left" data-limit-rows="4">
                                    <p>
                                      <!--  capitalize the first letter of the first word;-->Duis condimentum augue id magna semper rutrum in vulputate urna eu arcu nunc posuere aliquam erat volutpat fusce wisi ut tempus purus at lorem fusce dui 
                                    </p>
                                  </div>
                                  <div class="buttons"><a class="is-accent" href="shop.html" title="Shop" data-role="dynamic-hover-arrow" data-custom-hover="true">Explore more</a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="position-relative" data-role="slider-container">
                    <div class="grid-container">
                      <div class="grid-x grid-margin-x">
                        <div class="cell small-6 large-offset-3 small-offset-1 medium-offset-1">
                          <div class="swiper-container" data-slider="home-services-titles_colored">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">
                                <div class="home-services__title" data-role="services-animated-title">
                                  <div class="eyebrow"><span>/ explore</span></div>
                                  <div class="title" data-swiper-parallax="-35%">
                                    <h2>Party</h2>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="home-services__title" data-role="services-animated-title">
                                  <div class="eyebrow"><span>/ explore</span></div>
                                  <div class="title" data-swiper-parallax="-35%">
                                    <h2>Chill</h2>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="home-services__title" data-role="services-animated-title">
                                  <div class="eyebrow"><span>/ explore</span></div>
                                  <div class="title" data-swiper-parallax="-35%">
                                    <h2>Shop</h2>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-container">
                <div class="grid-x grid-margin-x">
                  <div class="cell medium-8 medium-offset-4">
                    <div class="home-services__images layers">
                      <div class="home-services__image" data-role="home-services-image">
                        <div class="mask-in">
                          <div class="mask-out" data-custom-hover="true" data-hover-text="View">
                            <div class="overlay-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></div>
                            <div class="card card_870">
                              <div class="card__image">
                                <!-- Attention: responsive image used. Require srcset -->
                                <!-- image sizes: default - 930x870, retina @2x - 1860x1740 --><img data-src="images/cards/930x870/image-1_930x870.jpg" data-srcset="images/cards/930x870/image-1_930x870.jpg 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="home-services__image" data-role="home-services-image">
                        <div class="mask-in">
                          <div class="mask-out" data-custom-hover="true" data-hover-text="View">
                            <div class="overlay-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></div>
                            <div class="card card_870">
                              <div class="card__image">
                                <!-- Attention: responsive image used. Require srcset -->
                                <!-- image sizes: default - 930x870, retina @2x - 1860x1740 --><img data-src="images/cards/930x870/image-2_930x870.jpg" data-srcset="images/cards/930x870/image-2_930x870.jpg 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="home-services__image" data-role="home-services-image">
                        <div class="mask-in">
                          <div class="mask-out" data-custom-hover="true" data-hover-text="View">
                            <div class="overlay-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></div>
                            <div class="card card_870">
                              <div class="card__image">
                                <!-- Attention: responsive image used. Require srcset -->
                                <!-- image sizes: default - 930x870, retina @2x - 1860x1740 --><img data-src="images/cards/930x870/image-3_930x870.jpg" data-srcset="images/cards/930x870/image-3_930x870.jpg 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
<div class="body-lines">
<div class="grid-container full">
<div class="grid-x">
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
</div>
</div>
</div>
        </main>
        <?php $this->inc('elements/footer.php'); ?>