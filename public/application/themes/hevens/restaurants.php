<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
$logoColor = $p->getAttribute('logo_color'); 
?>
<?php $this->inc('elements/header.php'); ?>


<div id="barba-wrapper">
      <div class="barba-container">

          <div data-role="dynamic-classes" data-header-classes="header_light" data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-logo-color="<?=$logoColor?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
             }"></div>

        <section class="cover cover_light" data-title="Cover banner" data-bg="dark" data-animation="section" id="section-cover">
        	<?php if($c->isEditMode()) { 
        				$cls = ""; 
        			}
        			else
        			{
        				$cls = "cover__overlay";
        			}
        		?>
              <div class="<?php echo $cls; ?> layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <h1 class="visually-hidden"><?= $name ?></h1>
                    <div class="cover-title cover-title_dark cover-title_left cover-title_subpage cell small-10 small-offset-1 medium-offset-1" data-role="multi-title">
                      	<div class="cover-title__eyebrow" data-role="multi-title__eyebrow">
                        <div class="eyebrow"><span>
                        	<?php 
	                            $explore = new GlobalArea('Explore');
	                            $explore->display();
                        	?>
                        </span></div>
                      </div>
                      
                      <div class="cover-title__title layers" data-role="multi-title__titles">
                      	<?php if(!$c->isEditMode()) { ?>
                      	<span class="h1" aria-hidden="true" data-role="multi-title__main-title">
                      	<?php } ?>
                      	<?php 
                            $title = new Area('Title Text');
                            $title->display();
                        ?>
                        <?php if(!$c->isEditMode()) { ?>
                      </span>
                      <?php } ?>
                  </div>
                  

                  	
                      <div class="cover-title__description" data-role="multi-title__descriptions">
                      	<?php if(!$c->isEditMode()) { ?>
                        <p data-role="multi-title__main-description">
                       <?php } ?> 	
                        <?php 
                            $smallDesc = new Area('Small Description');
                            $smallDesc->display();
                        ?>
                        <?php if(!$c->isEditMode()) { ?>
                        </p>
                         <?php } ?>
                      </div>

                      <div class="cover-title__link" data-role="multi-title__link">
                        <div class="buttons">
                        	<?php if(!$c->isEditMode()) { ?>
                        	<a href="#" data-role="dynamic-hover-arrow" data-custom-hover="true">
                        	<?php } ?>
                           <?php 
                            $exploreMore = new GlobalArea('Explore More');
                            $exploreMore->display();
                            ?> 
                            <?php if(!$c->isEditMode()) { ?>
                          </a>
                      <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<!--               <div class="cover__social">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-1 flex-container flex-dir-column align-right">
                      <ul class="contacts">
                        <?php 
                            $socialshare = new GlobalArea('Social Link');
                            $socialshare->display();
                        ?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="scroll-action"></div>
              <div class="<?php if (!$c->isEditMode()) { ?>overlay-image<?php } ?> overlay-image_br overlay-image_zermatt-610x414">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 610x414, retina @2x - 1220x828 -->
                <!-- please add size attributes to the image-->
                <?php 
                    $zermatt = new GlobalArea('zermatt Image');
                    $zermatt->display();
                ?>
              </div>
              <!-- <div class="layers__end" data-role="multi-title__direct-links">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-10 large-offset-1">
                      <ul class="tab-links" data-role="animated-list-line">

                      	<?php 
		                    $menu = new Area('Menu');
		                    $menu->display();
	                	?>
                        <span class="tab-links__line" data-role="forward-line">
                          <div class="styling-dots">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div></span>
                      </ul>
                    </div>
                  </div>
                </div>
              </div> -->
            </section>

            

		<main class="content">
              <div class="<?php if (!$c->isEditMode()) { ?>overlay-image<?php } ?> overlay-image_cl overlay-image_mammoth-center">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 400x715, retina @2x - 800x1430 -->
                <!-- please add size attributes to the image-->
                <?php 
		                $hillImage = new GlobalArea('Mammoth Overlay Image');
		                $hillImage->display();
	                ?>
              </div>
              <?php if(!$c->isEditMode()) { ?>
              <div class="grid-container full indent-inner-top-m" data-role="multi-title__items">
              <?php } ?>
              <?php if(!$c->isEditMode()) { ?>
                <div class="grid-x align-right">
                <?php } ?>    	
                	<?php 
		                $restaurantLists = new Area('Restaurants List');
		                $restaurantLists->display();
	                ?>
	            <?php if(!$c->isEditMode()) { ?>    
                </div>
            <?php } ?>
                <?php if(!$c->isEditMode()) { ?>
              </div>
          <?php } ?>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>

<?php $this->inc('elements/footer.php'); ?>