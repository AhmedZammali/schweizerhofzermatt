<?php  defined('C5_EXECUTE') or die("Access Denied.");
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker; 
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$name = $p->getCollectionName();
$img = $p->getAttribute('banner_image');
$imageHelper = Core::make('helper/image');
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
?>
<?php 
    $srcImg = $imageHelper->getThumbnail($p->getAttribute('banner_image'),'','',flase)->src;
    $srcImg2 = $imageHelper->getThumbnail($p->getAttribute('banner_image'),1220,612,true)->src;
?>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" header_light" data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
                }">
            </div>  
           
            <section class="cover cover_small cover_overlay-bottom cover_light" data-title="Cover banner" data-bg="dark" data-animation="section" id="section-cover">
             <?php if($c->isEditMode()) { 
                $cls = ""; 
                $split= '';
                $gridcls='';
                $overlay ="";
              }
              else
              {
                $cls = "cover__overlay";
                $split ='data-split-text="true"';
                $gridcls ='grid-x grid-margin-x';
                $overlay ="overlay-image";
              }
            ?>
              <div class="<?=$cls?> layers__center">
                <div class="grid-container full">
                  <div class="<?=$gridcls?>">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_left cover-title_subpage" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow"><span <?=$split?>><?php $text = new Area("See Bredcrumb Text");
                                $text->display();
                            ?></span></div>
                        </div>
                        <div class="cover-title__title">
                          <h1 <?=$split?>>
                            <?php $title = new Area("See Title");
                                $title->display();
                            ?>
                          </h1>
                        </div>
                        <div class="cover-title__description">
                          <?php  if(!$c->isEditMode()) { ?> 
                          <p data-split-text="true">
                            <?php } else{?><p><?php }?>
                            <?php $headerText = new GlobalArea("Header Text See Page");
                                   $headerText->display(); 
                             ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
              <div class="<?=$overlay?> overlay-image_br overlay-image_zermatt-bottom">
                <?php
                    $gallery_overlay= new GlobalArea("Gallery Overlay Image");
                    $gallery_overlay->display();
                 ?>
              </div>
            </section>
            <main class="content">
              <section class="section indent-inner-top-l indent-l" data-role="grid-with-filters" data-animation="section" data-offset="150">
                <div class="grid-container">
                  <div class="grid-x grid-margin-x visually-hidden indent-l">
                    <div class="cell large-10 large-offset-1">
                      <ul class="tab-links" data-role="animated-list-linefilters-container">
                        <li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide"><a href="#" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link filters-item" data-filter="*">All</a></li><span class="tab-links__line" data-role="forward-line">
                          <div class="styling-dots">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div></span>
                      </ul>
                    </div>
                  </div>
                  <div class="<?=$gridcls?>">
                      <?php
                        $gallery = new GlobalArea("Gallery List");
                        $gallery->display();
                       ?>
                  </div>
                </div>
              </section>
              <div class="body-lines">
                <div class="grid-container full">
                  <div class="grid-x">
                    <div class="body-lines__line cell small-2"></div>
                    <div class="body-lines__line cell small-2"></div>
                    <div class="body-lines__line cell small-2"></div>
                    <div class="body-lines__line cell small-2"></div>
                    <div class="body-lines__line cell small-2"></div>
                  </div>
                </div>
              </div>
        </main>

<?php $this->inc('elements/footer.php'); ?>