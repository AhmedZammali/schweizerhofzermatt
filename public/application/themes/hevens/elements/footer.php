<!-- this should be rendered at every page.-->
<!-- this is placed here so that user can edit the content from Concrete5.-->
<section class="hidden-non-edit-mode">
  <div class="grid-container cookies-placeholder">
    <!-- following will be appended as child of body to make animation of cookies message possible-->
    <div class="editable">
      <h3>
        <?php 
          $cookieTitle = new GlobalArea("Footer Cookies Title");
          $cookieTitle->display();
        ?>
      </h3>
      <p>
        <?php 
          $cookieMessage = new GlobalArea("Footer Cookies Message");
          $cookieMessage->display();
        ?>
      </p>
    </div>
    <div class="buttons">
      <button class="button" data-custom-hover="true">
        <span>
          <?php 
            $cookieButton = new GlobalArea("Footer Cookies Button");
            $cookieButton->display();
          ?>
        </span>
      </button>
    </div>
  </div>
</section>
          <nav class="breadcrumbs" role="breadcrumbs">
              <div class="grid-container">
                <!-- <ul>
                  <li><a href="index.html" data-custom-hover="true">Home</a></li>
                  <li><span>News</span></li>
                </ul> -->
                  <?php 
                    $breadcrumbs = new GlobalArea("Breadcrumbs");
                    $breadcrumbs->display();
                  ?>                
              </div>
            </nav>
          </div>
        </div>
        <!-- footer-->
        <footer class="footer" role="contentinfo">
          <div class="footer__image">
            <!-- Attention: responsive image used. Require srcset -->
            <!-- image sizes: default - 1440x900, retina @2x - 2880x1800 -->
            <?php    
                $footerImage = new GlobalArea("Footer Image");
                $footerImage->display();
            ?>
          </div>
          <div class="grid-container full">
            <div class="footer__top indent-inner-top-m indent-l">
              <div class="grid-x grid-margin-x align-center">
                <div class="cell large-2">
                  <div class="footer__logo">
                    <div class="logo">
                        <?php    
                            $footerLogo= new GlobalArea("Footer Logo");
                            $footerLogo->display();
                        ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <section class="footer__main" aria-label="Site directory, Contacts, Social links">
              <div class="grid-x grid-margin-x">
                <div class="cell large-8 large-offset-2 indent-m">
                  <nav class="footer__nav" aria-label="Site Directory" role="navigation">
                   <?php    
                        $footerMenu= new GlobalArea("Footer Menu");
                        $footerMenu->display();
                    ?>
                    
                  </nav>
                </div>
                <div class="cell large-4 large-offset-4 medium-8 medium-offset-2 small-10 small-offset-1 indent-l">
                  <div class="editable editable_text-center">
                    <p>
                    <?php    
                        $footerText= new GlobalArea("Footer Text");
                        $footerText->display();
                    ?>
                      <!--  capitalize the first letter of the first word;-->
                    </p>
                  </div>
                </div>
                <div class="cell large-2 large-offset-3 medium-4 indent-m">
                  <div class="footer__contacts">
                    <div class="title">
                      <h6>
                          <?php    
                            $addressText= new GlobalArea("Address Text");
                            $addressText->display();
                        ?>
                      </h6>
                    </div>
                    <ul class="contacts">
                       <?php    
                            $addressFooterText= new GlobalArea("Address Footer Text");
                            $addressFooterText->display();
                        ?>
                      
                    </ul>
                  </div>
                </div>
                <div class="cell large-2 medium-4 indent-m">
                  <div class="footer__contacts">
                    <div class="title">
                      <h6>
                        <?php    
                            $emailAndPhoneText= new GlobalArea("Email And Phone Text Footer");
                            $emailAndPhoneText->display();
                        ?>
                     </h6>
                    </div>
                    <ul class="contacts">
                      <?php    
                            $emailCallFooterText= new GlobalArea("Email & Phone Footer Text");
                            $emailCallFooterText->display();
                        ?>
                      
                    </ul>
                  </div>
                </div>
                <div class="cell large-2 medium-4 indent-m">
                  <div class="footer__contacts">
                    <div class="title">
                      <h6><?php    
                            $socialText= new GlobalArea("social Footer Text");
                            $socialText->display();
                        ?></h6>
                    </div>
                      <?php    
                            $socialLinks= new GlobalArea("Social Footer Links");
                            $socialLinks->display();
                      ?>
                    
                  </div>
                </div>
              </div>
              <div class="grid-x grid-margin-x">
                <div class="cell large-2 large-offset-3 small-6 indent-m">
                  <div class="footer__sub-logo footer__sub-logo_mr">
                   <?php    
                            $footerSubLogo1= new GlobalArea("Footer Sub Logo1");
                            $footerSubLogo1->display();
                    ?>     
                    </div>
                </div>
                <div class="cell large-2 large-offset-2 small-6 indent-m">
                  <div class="footer__sub-logo footer__sub-logo_slh">
                   <?php    
                            $footerSubLogo2= new GlobalArea("Footer Sub Logo2");
                            $footerSubLogo2->display();
                    ?>
                    </div>
                </div>
              </div>
            </section>
            <section class="footer__bottom" aria-label="Legal-links, creator">
              <div class="grid-x grid-margin-x">
                <div class="cell large-5 large-offset-1">
                  <div class="footer__copyright">
                    <p><?php    
                            $copyRightText= new GlobalArea("CopyRight Footer Text");
                            $copyRightText->display();
                        ?></p>
                  </div>
                  <div class="footer__legal-links">
                    <ul>
                      <li>
                        <?php    
                          $privcy_policy= new GlobalArea("Privacy Policy Footer Text");
                          $privcy_policy->display();
                        ?>
                      </li>
                      <li><a href="#" title="Cookies" data-custom-hover="true">Cookies</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="cell large-5 small-order-1 large-order-2 creator"> <?php    
                            $madeBy= new GlobalArea("Made By Footer Text");
                            $madeBy->display();
                        ?></div>
              </div>
            </section>
          </div>
        </footer>
      </div>
      <?php    
        $bookNowMenu= new GlobalArea("Side Book Now");
        $bookNowMenu->display();
      ?>
      
<!--       <div class="application application_cta">
        <div class="cta" data-role="cta-block">
            <div class="cta__block cta__block_light">
              <span class="cta__main-text">
                <?php    
                  $bookNow= new GlobalArea("Book Now Footer Text");
                  $bookNow->display();
                ?>
              </span>
                <?php    
                  $bookNowMenu= new GlobalArea("Book Now Menu");
                  $bookNowMenu->display();
                ?>
              
            </div>
          .cta__block.cta__block_dark
           a(href="#" title="Book Now" data-custom-hover="true")-->
          <!--    span The App
        </div>

      </div> -->
      <!-- Modals-->
      <div class="application application_modals">
        <div class="video-modal" data-role="video-modal-element">
          <div class="video-modal__spinner">
            <div class="my-loader">
              <div class="my-spinner">
                <div class="right-side">
                  <div class="bar"></div>
                </div>
                <div class="left-side">
                  <div class="bar"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="video-modal__top opened" data-role="modal-top"><i class="icon-close" data-role="video-modal-close"></i></div>
          <div class="video-modal__content" data-role="video-modal-content"></div>
        </div>
        <div class="gallery-modal" data-role="gallery-modal-element">
          <div class="gallery-modal__spinner">
            <div class="my-loader">
              <div class="my-spinner">
                <div class="right-side">
                  <div class="bar"></div>
                </div>
                <div class="left-side">
                  <div class="bar"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="gallery-modal__top opened" data-role="modal-top">
            <div class="logo">
            <?php    
                $gallleryLogo= new GlobalArea("Galllery Logo");
                $gallleryLogo->display();
            ?>
            </div><i class="icon-close" data-role="modal-close" data-custom-hover="true"></i>
          </div>
          <div class="gallery-modal__content">
            <div class="gallery-modal__slider simple-slider">
              <div class="position-relative" data-role="slider-container">
                <div class="swiper-controls" data-role="slider-controls">
                  <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                  <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                </div>
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1 card-overflow-text">
                      <div class="swiper-container swiper-container_indent-md" data-slider="modal-gallery" data-role="modal-main">
                        <div class="swiper-wrapper" data-role="slides-container"></div>
                      </div>
                      <div class="card-overflow-text__content">
                        <div class="card-overflow-text__text layers" data-role="slider-pagination"></div>
                      </div>
                      <div class="card-overflow-text__text card-overflow-text__text_outer layers" data-role="slider-pagination-outer"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-container" data-role="modal-bottom">
                <div class="grid-x grid-margin-x">
                  <div class="cell large-4 large-offset-4 medium-8 medium-offset-2">
                    <div class="swiper-bottom">
                      <div class="styling-dots">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                      </div>
                      <div class="slide-title layers" data-role="slider-titles"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="menu" data-role="menu">
          <div class="menu__assets" data-role="menu-assets" data-default-asset="<?=$view->getThemePath()?>/images/menu/512x512/image-2_512x512.png"></div>
          <div class="menu__inner" data-role="menu-scroll-container">
            <div class="grid-container full flex-child-grow flex-container flex-dir-column">
              <div class="grid-x grid-margin-x flex-child-grow">
                <div class="cell large-3 large-offset-1 show-for-large">
                  <div class="menu__section menu__section_contacts">
                    <div class="title menu__top-item">
                      <h3 <?php if (!$c->isEditMode()) { ?> data-split-text="true" <?php }?> >
                        <?php    
                                    $contactText= new GlobalArea("Contacts Text");
                                    $contactText->display();
                        ?></h3>
                      <div class="styling-dots styling-dots_left">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                      </div>
                    </div>
                    <ul class="contacts contacts_column">
                       <?php    
                                $address= new GlobalArea("Address Header Menu");
                                $address->display();
                        ?>
                        <?php    
                                $call= new GlobalArea("Phone No. Header Menu");
                                $call->display();
                        ?>
                        <?php    
                                $mail= new GlobalArea("Mail Header Menu");
                                $mail->display();
                        ?>
                        <li class="contacts__item contacts-item contacts-item_social menu__top-item">
                          <div class="contacts-item__title"><span><?php    
                            $socialText= new GlobalArea("social Footer Text");
                            $socialText->display();
                        ?></span></div>
                          <div class="contacts-item__content">
                            <?php
                              $headerSocial = new GlobalArea("Header Social Links");
                              $headerSocial->display(); 
                             ?>
                            
                          </div>
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="cell large-6 large-offset-1 flex-container flex-dir-column">
                  <div class="menu__section menu__section_categories" data-role="menu-list-container">
                      <?php
                        $hamburgerBarMenu = new GlobalArea("Hamburger Bar Menu");
                        $hamburgerBarMenu->display();
                       ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
<!--<script async src="https://widgets.healcode.com/javascripts/healcode.js" type="text/javascript"></script>-->
<script type="text/javascript">
  if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent))
    document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.min.js"><\/script>');
</script>
    <!-- js is appended here by HtmlWebpackPlugin automatically-->
  <script type="text/javascript" src="<?=$view->getThemePath()?>/js/common.js"></script>
<!--   <script type="text/javascript" src="<?=$view->getThemePath()?>/js/custom.js"></script> --></body>
<?php 
 $u = new User();

if ($u -> isLoggedIn ()) { ?>
  <?= Loader::element('footer_required'); ?>
<?php }
?>
</html>