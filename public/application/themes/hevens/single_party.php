<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
    $pageUrlSlug = implode("/", $pageSlug);
} else {
    $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
$lang = Localization::activeLanguage();
$ih = Loader::helper('image');  
$dh = Core::make('helper/date');
?>
<?php $this->inc('elements/header.php'); ?>
<div id="barba-wrapper">
<div class="barba-container">     
<div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
  &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
  &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
  &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
  }"></div>

  <section class="cover cover_full cover_overlay-bottom" data-title="Cover banner" data-bg="dark" data-animation="parallax-img section" data-use-native-height="true" data-trigger-hook="onLeave" data-offset="0" id="section-cover">
    <div class="cover__bg cover__bg_single-image" data-animation="parallax-item" data-role="main-image">
   <!--  <div class="visually-hidden" data-src="images/cover/1440x900/image-17_1440x900.jpg" data-type="img" data-cover-slider="slide"></div> -->
    <?php 
        $bannerImage = new Area("Party Banner Image");
        $bannerImage->display();
    ?>
  </div>
  <div class="cover__overlay layers__center">
    <div class="grid-container full">
    </div>
  </div>
  <div class="scroll-action"></div>
</section>
<main class="content">
  <section class="section indent-inner-top-l indent-xl">
    <h1 class="visually-hidden"><?=$name ?></h1>
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <!-- block title-->
        <div class="cell large-8 large-offset-2 medium-10 medium-offset-1 indent-l" data-animation="section">
          <div class="eyebrow eyebrow_center eyebrow_article" data-animation="fade-in-up">
            <?php 
              $eventStartDate = $c->getAttribute('start_date');
              $eventEndDate = $c->getAttribute('end_date');
              if($eventStartDate)
                $startDate  = $eventStartDate->format('Y-m-d');
              if($eventEndDate) {
                $endDate  = $eventEndDate->format('Y-m-d');
              }
                
            ?>
            <time datetime="<?=$startDate?>">
              <?php 
                $pageName = new Area("Event Date");
                $pageName->display();

                if($endDate) { ?>
                    - <time datetime="<?=$endDate?>"><?=$eventEndDate->format('d F Y')?></time>
                    <?php }                 
              ?>

            </time>
          </div>
          <div class="title title_center" data-animation="fade-in-up"><span class="article-heading">
              <?php 
                $pageName = new Area("Page Name");
                $pageName->display();
              ?>
          </span></div>
          <div class="styling-dots styling-dots__center" data-animation="fade-in-up">
            <div class="styling-dots__dot"></div>
            <div class="styling-dots__dot"></div>
            <div class="styling-dots__dot"></div>
          </div>
        </div>
        <!-- block text-->
        <div class="cell large-6 large-offset-3 medium-8 medium-offset-2 indent-l" data-animation="section">
          <div class="editable" data-animation="fade-in-up_text">
            <p><?php 
                $pageDescription = new Area("Page Description");
                $pageDescription->display();
              ?></p>
          </div>
        </div>
         <?php 
            $eventBlockQuote = $p->getAttribute('blockquote');
            if($eventBlockQuote) { 
          ?>
        <!-- block blockquote-->
        <div class="cell large-4 large-offset-4 medium-6 medium-offset-3 indent-l" data-animation="section">
          <div class="editable" data-animation="fade-in-up_text">
            <blockquote><?=$eventBlockQuote?></blockquote>
          </div>
        </div>
        <?php } ?>
        <!-- block inner-image-->
        <div class="cell large-8 large-offset-2 small-10 small-offset-1 medium-offset-1 indent-l" data-animation="section">
          <div class="card card_9x16" data-animation="fade-in-up">
            <div class="card__image">
              <?php 
                $pageSingleImage = new Area("Page Single Image");
                $pageSingleImage->display();
              ?>
            </div>
          </div>
        </div>
        <!-- block social-->
        <div class="cell" data-animation="section">
          <div class="title title_center" data-animation="fade-in-up">
            <h6> 
              <?php 
                $share = new GlobalArea("Share");
                $share->display();
              ?>                        
            </h6>
          </div>
          <div class="flex-container align-center" data-animation="fade-in-up">
            <ul class="social">
              <?php 
                $sharePage = new Area("Share Page");
                $sharePage->display();
              ?> 
              <!-- <li><a class="no-barba" href="facebook.com" title="Our facebook" data-custom-hover="true"><i class="icon-facebook"></i></a></li>
              <li><a class="no-barba" href="twitter.com" title="Our twitter" data-custom-hover="true"><i class="icon-twitter"></i></a></li>
              <li><a class="no-barba" href="pinterest.com" title="Our pinterest" data-custom-hover="true"><i class="icon-pinterest"></i></a></li>
              <li><a class="no-barba" href="whatsapp.com" title="Our whatsapp" data-custom-hover="true"><i class="icon-whatsapp"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section indent-l" data-animation="section">
    <div class="overlay-image overlay-image_tr overlay-image_matterhorn-top">
      <!-- Attention: responsive image used. Require srcset -->
      <!-- image sizes: default - 682x407, retina @2x - 1364x814 -->
      <!-- please add size attributes to the image-->
      <?php 
        $matterhornImage = new GlobalArea("Matterhorn Image");
        $matterhornImage->display();
      ?>
    </div>
    <div class="grid-container">
      <div class="grid-x grid-margin-x indent-l">
        <div class="cell large-5 large-offset-1">
          <div class="eyebrow" data-animation="title"><span <?php if(!$c->isEditMode()) { ?> data-split-text="true"<?php } ?> >
              <?php 
                $otherEvent = new GlobalArea("Other Event");
                $otherEvent->display();
              ?>
          </span></div>
        </div>
      </div>
    </div>
    <div class="slider-diff-indents">
      <?php 
          $otherEventList = new GlobalArea("Other Events List");
          $otherEventList->display();
      ?> 
    </div>
  </section>
<div class="body-lines">
<div class="grid-container full">
<div class="grid-x">
<div class="body-lines__line cell small-2"></div>
<div class="body-lines__line cell small-2"></div>
<div class="body-lines__line cell small-2"></div>
<div class="body-lines__line cell small-2"></div>
<div class="body-lines__line cell small-2"></div>
</div>
</div>
</div>
</main>
<?php $this->inc('elements/footer.php'); ?>