<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
use Concrete\Core\Tree\Type\Topic as TopicTree;
use Concrete\Core\Block\View\BlockView as View;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$slug = end(explode("/", $c->getCollectionPath()));
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$handle = $p->getCollectionHandle();
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
$logoColor = $p->getAttribute('logo_color'); 
$filters = [];
?>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-logo-color="<?=$logoColor?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"></div>
            <!-- cover-->
            <!-- if cover_images.length-->
            <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
            <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
            <!--       +image(cover_image, 1440, 900)-->
            <!--     .cover-hover__images(data-role="cover-hover__images" )-->
            <!--       for image in cover_images-->
            <!--         -var {src} = image-->
            <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
            <!--       .cover-hover__stripes-->
            <!--         .grid-container.full-->
            <!--           .grid-x-->
            <!--             for image in cover_images-->
            <!--               .cell.small-2-->
            <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
            <section class="cover cover_full cover_overlay-bottom" data-title="Cover banner" data-bg="dark" data-animation="parallax-img section" data-use-native-height="true" data-trigger-hook="onLeave" data-offset="0" id="section-cover">
              <div class="cover__bg cover__bg_single-image" data-animation="parallax-item" data-role="main-image">
                <?php 
                  $expCoverImage = new Area('Experience Group Cover Image');
                  $expCoverImage->display();
                ?>
                </div>
              <div class="cover__overlay layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_light cover-title_left" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow">
                            <span <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                              <?php 
                                $expEyebrow = new GlobalArea('Experience Text');
                                $expEyebrow->display();
                              ?>
                            </span>
                          </div>
                        </div>
                        <div class="cover-title__title">
                          <h1 <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                              $expCoverTitle = new Area('Experience Group Cover Title');
                              $expCoverTitle->display();
                            ?>
                          </h1>
                        </div>
                        <div class="cover-title__description">
                          <p <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                              $expCoverDesc = new Area('Experience Group Cover Desc');
                              $expCoverDesc->display();
                            ?>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
            </section>
            <main class="content">
               <section class="section indent-inner-top-l" data-role="<?php if(!$c->isEditMode()) { ?>grid-with-filters<?php }?>" data-animation="section" data-offset="150">
              <div class="grid-container">
                  <div class="grid-x grid-margin-x indent-l">
                    <div class="cell large-10 large-offset-1">
                      <ul class="tab-links" data-role="animated-list-linefilters-container">
                        <li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide">
                          <?php if(!$c->isEditMode()) { ?><a href="#" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link filters-item" data-filter="*"><?php } ?>
                            <?php 
                         		  $allFilterText = new GlobalArea("All Filters Text");
                              $allFilterText->display();
                            ?>
                          <?php if(!$c->isEditMode()) { ?></a><?php } ?>
                        </li>
                        <?php 

                        $this->requireAsset('core/topics');  
                        $tt = new TopicTree();  
                        if($lang == 'en')
                          $tree = $tt->getByName('Experience Group Filters');
                        elseif($lang == 'fr')
                        {
                          $tree = $tt->getByName('Experience Group Filters FR');
                        }
                        else {
                          $capsLang = strtoupper($lang);
                          $tree = $tt->getByName('Experience Group Filters DE');
                        }
                            
                        $node = $tree->getRootTreeNodeObject();  
                        $node->populateChildren();  
                        if (is_object($node)) {  
                            foreach($node->getChildNodes() as $key => $filterCategory) {
                               if($node->getTreeNodeTypeHandle() == 'category') {
                                $treeParent = strtolower(str_replace(' ', '-', $filterCategory->getTreeNodeDisplayName()));
                               }
                                foreach($filterCategory->getChildNodes() as $key => $filterTopics) {
                                    if ($filterTopics instanceof \Concrete\Core\Tree\Node\Type\Topic) {  
                                        $filters[$treeParent][$filterTopics->getTreeNodeDisplayName()] = $filterTopics->getTreeNodeDisplayName();  
                                    }
                                }
                            } 
                        } 
                        foreach ($filters as $key => $results) {
                          if($key == $slug) {
                          foreach($results as $filter) { 
                            $filterAttr = strtolower($filter); ?>
                            <li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide">
                              <?php if(!$c->isEditMode()) { ?>
                                <a href="#" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link filters-item" data-filter="<?=$filterAttr ?>">
                              <?php } ?>
                                  <?=$filter ?>
                              <?php if(!$c->isEditMode()) { ?>    
                                </a>
                              <?php } ?>
                              </li>
                          <?php }  }
                        } ?><span class="tab-links__line" data-role="forward-line">
                          <div class="styling-dots">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div></span>
                      </ul>
                    </div>
                  </div>
                  <div <?php if(!$c->isEditMode()) { ?> class="grid-x grid-margin-x" <?php } ?>>
                    <?php 
                      $filterList = new Area("Experience Filter List");
                      $filterList->display();
                    ?>

                  </div>
                </div>
              </section>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
            <?php $this->inc('elements/footer.php'); ?>