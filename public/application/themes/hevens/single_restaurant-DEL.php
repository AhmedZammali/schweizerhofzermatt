<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$name = $p->getCollectionName();
$namespace = $p->getAttribute('page_namespace');
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
$lang = Localization::activeLanguage();
$ih = Loader::helper('image');  
?>
<?php $this->inc('elements/header.php'); ?>
<style type="text/css">
.application .department-logo{
    text-align: center !important;
  }
  .application .department-logo img{
    height:112px !important;
  }
</style>
<div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"></div>
            <!-- cover-->
            <!-- if cover_images.length-->
            <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
            <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
            <!--       +image(cover_image, 1440, 900)-->
            <!--     .cover-hover__images(data-role="cover-hover__images" )-->
            <!--       for image in cover_images-->
            <!--         -var {src} = image-->
            <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
            <!--       .cover-hover__stripes-->
            <!--         .grid-container.full-->
            <!--           .grid-x-->
            <!--             for image in cover_images-->
            <!--               .cell.small-2-->
            <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
             <section class="<?php if (!$c->isEditMode()) { ?>cover cover_full cover_overlay-bottom<?php } ?>" data-title="Cover banner" data-bg="dark" data-animation="section" data-role="cover-slider" data-mask-src="<?=$view->getThemePath()?>images/cover/mask.jpg" id="section-cover">
              <div class="<?php if (!$c->isEditMode()) { ?>cover__bg cover__bg_webgl-image<?php } ?>" data-role="main-image" data-cover-slider="scene">

                <?php 
                    $bannerImage = new Area('Banner Image');
                    $bannerImage->display();
                ?>
                
              </div>
              <?php if($c->isEditMode()) { 
                $cls = ""; 
              }
              else
              {
                $cls = "cover__overlay";
              }
            ?>

              <div class="<?php echo $cls; ?> layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_light cover-title_left" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow">
                            <?php if(!$c->isEditMode()) { ?>
                              <span><?php } ?>/ 
                            <?php 
                                $restaurant = new GlobalArea('Restaurant Text');
                                $restaurant->display();
                            ?>
                          </span></div>
                        </div>
                        <div class="cover-title__title">
                          <?php if(!$c->isEditMode()) { ?>
                          <h1 data-split-text="true">
                          <?php } ?>
                            <?php 
                                $pagename = new Area('Page Name');
                                $pagename->display();
                            ?>
                          </h1>
                        </div>
                        <div class="cover-title__description">
                          <?php if(!$c->isEditMode()) { ?>
                          <p data-split-text="true">
                          <?php } ?>
                            <?php 
                                $short_desc = new Area('Short Description');
                                $short_desc->display();
                            ?>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
            </section>
            <main class="content">
              <section class="section layers two-halves indent-xl indent-inner-top-l " data-animation="section">
                <div class="grid-container two-halves__bg">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-6 large-offset-6">
                      <div class="card card_two-halves">
                        <div class="card__image" data-role="simple-reveal">
                          <div data-role="reveal-target">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 640x880, retina @2x - 1280x1760 -->
                            <?php 
                              $restaurantImage = new Area('Restaurant Image');
                              $restaurantImage->display();
                            ?>                           
                          </div>                      
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="grid-container two-halves__overlay layers__center">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-4 large-offset-1">
                      <div class="two-halves__content">
                        <div class="department-logo" data-animation="fade-in-up">
                          <?php 
                              $restaurantLogo = new Area('Restaurant Logo');
                              $restaurantLogo->display();
                            ?>
                        </div>
                        <div class="title" data-animation="title">
                          <h2 class="visually-hidden"><?=$name ?></h2>
                          <span class="h4">
                            <?php 
                              $restaurantTitle = new Area('Restaurant Title');
                              $restaurantTitle->display();
                            ?>
                          </span>
                          <div class="styling-dots styling-dots_left indent-inner-top-m" data-animation="fade-in-up">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div>
                        </div>
                        <div class="editable editable_indent-l" data-animation="fade-in-up_text">
                          <p>
                            <!--  capitalize the first letter of the first word;-->
                            <?php 
                              $restaurantdesc = new Area('Restaurant Description');
                              $restaurantdesc->display();
                            ?> 
                          </p>
                        </div>
                        <div class="counter-list">
                          <?php $images= $p->getAttribute("image_attribute");                              
                              $attrData= explode("~",$images); 
                               $attrData = array_filter($attrData);

                               if(count($attrData) > 0) {
                                  foreach ($attrData as $key => $textIcon) {
                                    if(explode("!#",$textIcon)[0] != undefined) {
                                        
                                        $textValue = explode("!#",$textIcon)[0];
                                        $numText = explode("-#",$textValue);
                                        
                                        if(count($numText) > 0) {
                                          $textValue = $numText[0];
                                          /*$otherValue = explode("-#",$textValue)[1];
                                          print_r($otherValue);*/
                                        }                                        
                                        $textTitle = explode("|",explode("!#",$textIcon)[1])[0];
                                        $icon = explode("|",explode("!#",$textIcon)[1])[1];                                       
                                        ?>
                                        <div class="counter-list__item">
                                          <div class="counter-item counter-item_icon" data-animation="counter-item">                              
                                            <div class="counter-item__title">
                                              <p>
                                                <?=$textTitle?>
                                              </p>
                                            </div>
                                            <div class="counter-item__count">
                                              <p class="extra extra_icon"><i class="icon-<?=$icon?>"></i></p>
                                              <p data-animation="number"><?=$textValue?></p>
                                              <?php if($numText[1]) { ?>
                                                <p class="extra"><?=$numText[1]?></p>
                                              <?php } ?>
                                            </div>
                                          </div>
                                        </div>
                                        <?php
                                    }                                   
                                  }
                               }
                          ?>
                        </div>
                        <div class="buttons" data-animation="fade-in-up">
                          <?php 
                              $checkButton = new Area('Check Availability Attribute');
                              $checkButton->display();
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="section indent-xl">
                <div class="<?php if (!$c->isEditMode()) { ?>overlay-image<?php } ?> overlay-image_br overlay-image_birds">
                  <!-- Attention: responsive image used. Require srcset -->
                  <!-- image sizes: default - 322x224, retina @2x - 644x448 -->
                  <!-- please add size attributes to the image-->
                  <?php 
                        $birds = new GlobalArea('Birds Image');
                        $birds->display();
                    ?>
                </div>
                <div class="grid-container indent-m" data-animation="section">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-5 large-offset-1">
                      <div class="eyebrow" data-animation="title">
                        
                          <span>
                        <?php 
                            $explore = new GlobalArea('Explore');
                            $explore->display();
                        ?>
                              </span></div>
                      <div class="title" data-animation="title">
                        <h2>
                          <?php 
                              $gallery = new GlobalArea('Gallery');
                              $gallery->display();
                          ?>
                        </h2>
                      </div>
                    </div>
                  </div>
                </div>


                <?php 
                $images = $p->getAttribute('listing_slide');
                if($images != '') { ?>
                <div class="grid-container" data-role="slider-mini-gallery slider-container">
                  <div class="grid-x position-relative indent-l">
                    <div class="swiper-controls">
                      <div class="swiper-button swiper-button-next nav nav_next visible" data-custom-hover="true" data-hover-type="nav next"></div>
                      <div class="swiper-button swiper-button-prev nav nav_prev visible" data-custom-hover="true" data-hover-type="nav prev"></div>
                    </div>
                    <div class="cell small-8" data-role="mini-gallery-main card-overflow-text">
                      <div class="position-relative" data-role="slider-container">
                        <div class="grid-container">
                          <div class="grid-x grid-margin-x">
                            <div class="cell">
                              <div class="swiper-container" data-slider="gallery-mini_main">
                                <div class="swiper-wrapper">

                                  <?php 
                                  $images = $p->getAttribute('listing_slide');
                
				                if($images)
				                    $sliderImage = $images->getFileObjects();
				                  if(!empty($images)){
				                      foreach ($sliderImage as $file) {                     
				                        $ih = Loader::helper('image');                     
				                        $file = File::getByID($file->getFileID());
				                        $file = File::getByID($file->getFileID());
				                        $fileSrc = is_object($file) ? $ih->getThumbnail($file, 930, 680)->src : false;
                                $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 1860, 1360)->src : false;

				                        ?>

                                  <div class="swiper-slide">
                                    <div class="card card_680">
                                      <div class="card__image">
                                        <div class="image-inner" data-role="slide-inner">
                                          <!-- Attention: responsive image used. Require srcset -->
                                          <!-- image sizes: default - 930x680, retina @2x - 1860x1360 --><img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
 <?php
                      } 
                  } ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="cell small-4" data-role="mini-gallery-sub">
                      <div class="position-relative" data-role="slider-container">
                        <div class="grid-container">
                          <div class="grid-x grid-margin-x">
                            <div class="cell">
                              <div class="swiper-container" data-slider="gallery-mini_sub">
                                <div class="swiper-wrapper">
                                 
                                  <?php 
                      $galleryimages = $p->getAttribute('small_gallery_slider');
                      
                      if($galleryimages)
                          $gallerysliderImage = $galleryimages->getFileObjects();
                        if(!empty($galleryimages)){
                            foreach ($gallerysliderImage as $file) {                     
                              $ih = Loader::helper('image');                     
                              $file = File::getByID($file->getFileID());
                              $fileSrc = is_object($file) ? $ih->getThumbnail($file, 450, 450)->src : false;
                               $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 900, 900)->src : false;
                              ?>

                                  <div class="swiper-slide">
                                    <div class="card card_square">
                                      <div class="card__image">
                                        <div class="image-inner" data-role="slide-inner">
                                          <!-- Attention: responsive image used. Require srcset -->
                                          <!-- image sizes: default - 450x450, retina @2x - 900x900 --><img class="swiper-lazy" data-src="<?=$fileSrc?>" data-srcset="<?=$fileSrcSet?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <?php } } ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="grid-x grid-margin-x" data-animation="section" data-trigger-offset="-200">
                    <div class="cell large-4 large-offset-4 medium-6 medium-offset-3 small-10 small-offset-1">
                      <div class="mini-gallery__info" data-animation="fade-in-up">
                        <div class="slide-info">
                          <div class="swiper-pagination" data-role="slider-pagination"></div>
                          <div class="swiper-progress" data-role="progress"><span class="line"></span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              </section>
              <section class="section layers two-halves indent-xl two-halves_reverse" data-animation="section">
                <div class="grid-container two-halves__bg">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-6">
                      <div class="card card_two-halves">
                        <div class="card__image" data-role="simple-reveal">
                          <div data-role="reveal-target">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 640x880, retina @2x - 1280x1760 -->

                             <?php 
                              $menuImage = new Area('Menu Image');
                              $menuImage->display();
                            ?>

              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="grid-container two-halves__overlay layers__center">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-4 large-offset-7">
                      <div class="two-halves__content">
                        <div class="eyebrow" data-animation="title">

                          <span>/ 
                          <?php 
                              $the = new GlobalArea('the');
                              $the->display();
                          ?>
                        </span></div>
                        <div class="title" data-animation="title">
                          <h2>
                            <?php 
                              $menuTitle = new GlobalArea('Menu title');
                              $menuTitle->display();
                          ?>
                          </h2>
                          <div class="styling-dots styling-dots_left indent-inner-top-m" data-animation="fade-in-up">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div>
                        </div>
                        <div class="editable editable_indent-l" data-animation="fade-in-up_text">
                          <p>
                            <!--  capitalize the first letter of the first word;-->
                             <?php 
                              $menudesc = new Area('Menu Description');
                              $menudesc->display();
                          ?>
                          </p>
                        </div>
                        <div class="buttons" data-animation="fade-in-up">

                        	<?php 
                              $downloadmenu = new Area('Download Menu');
                              $downloadmenu->display();
                          	?>

                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>


<?php $this->inc('elements/footer.php'); ?>