<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$ih = Loader::helper('image'); 
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}


$news_date = $c->getAttribute('news_date');
if($news_date){
$ndate  = $news_date->format('d F Y');
$ntime = $news_date->format('h:i A');
}


?>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" header_light" data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
                }">
            </div> 

              <?php if($c->isEditMode()) { 
                $cls = ""; 
                $split= '';
              }
              else
              {
                $cls = "cover__overlay";
                $split ='data-split-text="true"';
              }
            ?>
            <main class="content">
              <section class="section indent-inner-top-l indent-xl">
                <h1 class="visually-hidden">
                  <?php 
                    $page_title = new Area('Page Title Name');
                    $page_title->display();
                  ?>
                </h1>
                <div class="grid-container">
                  <div class="grid-x grid-margin-x">
                    <!-- block main image-->
                    <div class="cell large-10 large-offset-1 indent-l" data-animation="section">
                      <div class="card card_9x16" data-animation="fade-in-up">
                        <div class="card__image">
                          <?php 
                            $bannerImage = new Area("News Banner Image");
                            $bannerImage->display();
                          ?>
                         
                        </div>
                      </div>
                    </div>
                    <!-- block title-->
                    <div class="cell large-8 large-offset-2 medium-10 medium-offset-1 indent-l" data-animation="section">
                      <div class="eyebrow eyebrow_center eyebrow_article" data-animation="fade-in-up">
                        <time datetime="<?=$ndate?>">
                          <?=$ndate?>
                        </time>
                      </div>
                      <div class="title title_center" data-animation="fade-in-up"><span class="article-heading">
                        <?php 
                          $page_title = new Area('Page Title Name');
                          $page_title->display();
                        ?>
                      </span></div>
                      <div class="styling-dots styling-dots__center" data-animation="fade-in-up">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                      </div>
                    </div>
                    <!-- block text-->
                    <div class="cell large-6 large-offset-3 medium-8 medium-offset-2 indent-l" data-animation="section">
                      <div class="editable" data-animation="fade-in-up_text">
                        <p>
                          <?php 
                            $news_desc = new Area('News Description');
                            $news_desc->display();
                          ?>
                        </p>
                      </div>
                    </div>
                    <!-- block blockquote-->
                    <div class="cell large-4 large-offset-4 medium-6 medium-offset-3 indent-l" data-animation="section">
                      <div class="editable" data-animation="fade-in-up_text">
                        <blockquote>
                          <?php 
                            $news_quote = new Area('News Quote');
                            $news_quote->display();
                          ?>
                        </blockquote>
                      </div>
                    </div>
                    <!-- block inner-image-->
                    <div class="cell large-8 large-offset-2 small-10 small-offset-1 medium-offset-1 indent-l" data-animation="section">
                      <div class="card card_9x16" data-animation="fade-in-up">
                        <div class="card__image">
                          <?php 
                            $news_image = new Area('News Image');
                            $news_image->display();
                          ?>
                        </div>
                      </div>
                    </div>
                    <!-- block inner-title-->
                    <div class="cell large-4 large-offset-3 medium-6 medium-offset-2 small-8 small-offset-1 indent-l" data-animation="section">
                      <div class="title" data-animation="fade-in-up">
                        <h4>
                          <?php 
                            $exp_heading = new Area('Experience Heading');
                            $exp_heading->display();
                          ?>
                        </h4>
                      </div>
                      <div class="styling-dots styling-dots_left" data-animation="fade-in-up">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                      </div>
                    </div>
                    <!-- block text-->
                    <div class="cell large-6 large-offset-3 medium-8 medium-offset-2 indent-l" data-animation="section">
                      <div class="editable" data-animation="fade-in-up">
                        <p>
                          <?php 
                            $exp_desc = new Area('Experience Description');
                            $exp_desc->display();
                          ?>
                        </p>
                      </div>
                    </div>
                    <!-- download-link block-->
                    <div class="cell large-6 large-offset-3 medium-8 medium-offset-2 indent-l" data-animation="section">
                      <div class="editable editable_text-center">
                        <?php 
                            $download_pdf = new GlobalArea('Download PDF');
                            $download_pdf->display();
                          ?>
                      </div>
                    </div>
                    <!-- block social-->

                    <div class="cell" data-animation="section">
                      <div class="title title_center" data-animation="fade-in-up">
                        <h6>
                          <?php 
                            $share_text = new GlobalArea('Share Text');
                            $share_text->display();
                          ?>
                        </h6>
                      </div>
                      <div class="flex-container align-center" data-animation="fade-in-up">
                        <ul class="social">
                          <?php 
                            $social_share = new GlobalArea('News Social Share');
                            $social_share->display();
                          ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <?php if($c->isEditMode()) { 
                $cls = ""; 
                $split= '';
                }
                else
                {
                  $cls = "cover__overlay";
                  $split ='data-split-text="true"';
                }
              ?>
              <section class="section indent-l" data-animation="section">
                <div class="grid-container">
                  <div class="grid-x grid-margin-x indent-l">
                    <div class="cell large-5 large-offset-1">
                      <div class="eyebrow"><span <?=$split?>>
                          <?php 
                            $read_next = new GlobalArea('Read Next Text');
                            $read_next->display();
                          ?></span></div>
                    </div>
                  </div>
                </div>
                <div class="slider-diff-indents">
                  <div class="position-relative" data-role="slider-container">
                    <div class="swiper-controls">
                      <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                      <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                    </div>
                    <div class="grid-container">
                      <div class="grid-x grid-margin-x">
                        <div class="cell  small-10 small-offset-1 medium-offset-1">
                          <div class="swiper-container" data-slider="another-items">
                            <div class="swiper-wrapper">
                              <div class="swiper-pagination"></div>
                              <?php 
                                $news_MoreList = new Area('News More List');
                                $news_MoreList->display();
                              ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
<?php $this->inc('elements/footer.php'); ?>