<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$parentPageID = $p->getCollectionParentID();
$parentPage = Page::getByID($parentPageID); 
$lang = Localization::activeLanguage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
?>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"></div>
            <!-- cover-->
            <!-- if cover_images.length-->
            <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
            <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
            <!--       +image(cover_image, 1440, 900)-->
            <!--     .cover-hover__images(data-role="cover-hover__images" )-->
            <!--       for image in cover_images-->
            <!--         -var {src} = image-->
            <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
            <!--       .cover-hover__stripes-->
            <!--         .grid-container.full-->
            <!--           .grid-x-->
            <!--             for image in cover_images-->
            <!--               .cell.small-2-->
            <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
            <section class="<?php if (!$c->isEditMode()) { ?>cover cover_full<?php } ?>" data-title="Cover banner" data-bg="dark" data-animation="section" id="section-cover">
              <div class="<?php if (!$c->isEditMode()) { ?> cover__overlay<?php } ?> layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_light cover-title_left" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow">
                            <span <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                              <?php 
                                $coverExplore = new GlobalArea('Explore');
                                $coverExplore->display();
                              ?>
                            </span>
                          </div>
                        </div>
                        <div class="cover-title__title">
                          <h1 <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                              $singleRoomTitle = new Area('Single Room Title');
                              $singleRoomTitle->display();
                            ?>
                          </h1>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
              <div class="<?php if (!$c->isEditMode()) { ?> cover__bg<?php } ?>" data-role="main-image">
                <div class="simple-slider <?php if (!$c->isEditMode()) { ?>simple-slider_cover<?php } ?>" data-role="simple-slider-container" data-fract-pagination="true">
                  <div class="position-relative" data-role="slider-container">
                    <div class="swiper-controls">
                      <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                      <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                    </div>
                    <div class="grid-container  full">
                      <div class="grid-x grid-margin-x">
                        <div class="cell">
                          <div class="swiper-container" data-slider="simple-slider">
                            <div class="slide-info">
                              <div class="swiper-pagination" data-role="slider-pagination"></div>
                              <div class="swiper-progress" data-role="progress"><span class="line"></span></div>
                            </div>
                            <?php 
                              $singleRoomCoverSlider = new Area('Single Room Cover Slider');
                              $singleRoomCoverSlider->display();
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <main class="content">
              <section class="section layers two-halves indent-l indent-inner-top-l two-halves_reverse" data-animation="section">
                <div class="grid-container two-halves__bg">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-6">
                      <div class="card card_two-halves">
                        <div class="card__image" data-role="simple-reveal">
                          <div data-role="reveal-target">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 640x880, retina @2x - 1280x1760 -->
                            <?php 
                              $singleRoomImage = new Area('Single Room Image');
                              $singleRoomImage->display();
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="grid-container two-halves__overlay layers__center">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-4 large-offset-7">
                      <div class="two-halves__content">
                        <div class="eyebrow" data-animation="title">
                          <span <?php if (!$c->isEditMode()) { ?> data-split-text="true" <?php } ?>>
                            <?php 
                              $exploreOurText = new GlobalArea('Explore Our Text');
                              $exploreOurText->display();
                            ?>
                          </span>
                        </div>
                        <div class="title" data-animation="title">
                          <h2 <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                              $singleRoomTitle = new Area('Single Room Title');
                              $singleRoomTitle->display();
                            ?>
                          </h2>
                        </div>
                        <div class="editable editable_indent-l" data-animation="fade-in-up_text">
                          <p>
                            <?php 
                              $singleRoomDesc = new Area('Single Room Description');
                              $singleRoomDesc->display();
                            ?>
                          </p>
                          <p><i class="icon-matterhorn_right"></i></p>
                        </div>
                        <div class="counter-list">
                          <?php $images= $p->getAttribute("image_attribute");                              
                              $attrData= explode("~",$images); 
                               $attrData = array_filter($attrData);

                               if(count($attrData) > 0) {
                                  foreach ($attrData as $key => $textIcon) {
                                    if(explode("!#",$textIcon)[0] != undefined) {
                                        
                                        $textValue = explode("!#",$textIcon)[0];
                                        $numText = explode("-#",$textValue);
                                        
                                        if(count($numText) > 0) {
                                          $textValue = $numText[0];
                                          /*$otherValue = explode("-#",$textValue)[1];
                                          print_r($otherValue);*/
                                        }                                        
                                        $textTitle = explode("|",explode("!#",$textIcon)[1])[0];
                                        $icon = explode("|",explode("!#",$textIcon)[1])[1];                                       
                                        ?>
                                        <div class="counter-list__item">
                                          <div class="counter-item counter-item_icon" data-animation="counter-item">                              
                                            <div class="counter-item__title">
                                              <p>
                                                <?=$textTitle?>
                                              </p>
                                            </div>
                                            <div class="counter-item__count">
                                              <p class="extra extra_icon"><i class="icon-<?=$icon?>"></i></p>
                                              <p data-animation="number"><?=$textValue?></p>
                                              <?php if($numText[1]) { ?>
                                                <p class="extra"><?=$numText[1]?></p>
                                              <?php } ?>
                                            </div>
                                          </div>
                                        </div>
                                        <?php
                                    }                                   
                                  }
                               }
                          ?>                          
                        </div>
                        <?php $parentExternalUrl = $parentPage->getAttribute('external_url'); ?>
                        <div class="buttons" data-animation="fade-in-up">
                          <?php if (!$c->isEditMode()) { ?>
                          <a class="button button_dark button_full-width button_uppercase" target="_blank" href="<?= ($parentExternalUrl) ? $parentExternalUrl : '#'?>">
                          <?php } ?>
                            <?php 
                              $checkAvailabilityButton = new GlobalArea('Check Availability Button Text');
                              $checkAvailabilityButton->display();
                            ?>
                            <?php if (!$c->isEditMode()) { ?>
                          </a>                          
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="section indent-l" data-animation="section">
                <div class="overlay-image overlay-image_tr overlay-image_matterhorn-right">
                  <!-- Attention: responsive image used. Require srcset -->
                  <!-- image sizes: default - 682x407, retina @2x - 1364x814 -->
                  <!-- please add size attributes to the image-->
                  <?php 
                      $birds = new GlobalArea('Matterhorn Image');
                      $birds->display();
                    ?>
                </div>
                <div class="grid-container">
                  <div class="grid-x grid-margin-x indent-l">
                    <div class="cell large-5 large-offset-1">
                      <div class="eyebrow" data-animation="title">
                        <span <?php if (!$c->isEditMode()) { ?> data-split-text="true" <?php } ?>>
                          <?php 
                            $exploreText = new GlobalArea('Explore');
                            $exploreText->display();
                          ?>
                        </span>
                      </div>
                      <div class="title" data-animation="title">
                        <h2 <?php if (!$c->isEditMode()) { ?> data-split-text="true" <?php } ?>>
                          <?php 
                            $otherRoomsText = new GlobalArea('Other Rooms Text');
                            $otherRoomsText->display();
                          ?>
                        </h2>
                      </div>
                    </div>
                  </div>
                </div>
                  <?php 
                    $otherRoomsList = new GlobalArea('Other Rooms List');
                    $otherRoomsList->display();
                  ?>
              </section>
              
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
            <?php $this->inc('elements/footer.php'); ?>