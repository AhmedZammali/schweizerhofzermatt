<?php
namespace Application\Theme\Hevens;

use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Page\Theme\Theme;
use Concrete\Core\Area\Area;
use Concrete\Core\Page\Page;
use Concrete\Core\Page\PageList;
use Concrete\Core\Tree\Node\Type\Topic;
use Concrete\Core\Express\EntryList;
use Concrete\Core\Entity\File\File;
use Concrete\Core\File\Image\Thumbnail\Type\Type;
use Config;

class PageTheme extends Theme implements ThemeProviderInterface
{
    /**
     * @var string
     */
    protected $languageCode;

    /**
     * @var string
     */
    protected $pageListPagination;

    /**
     * @var array
     */
    protected $blockTypes = [];

    public function getThemeAreaLayoutPresets()
    {
        //TODO
    }

    public function getThemeName()
    {
        return 'Hevens';
    }

    public function getThemeHandle()
    {
        return 'hevens';
    }

    /**
     * @return array
     */
    public function getThemeEditorClasses()
    {
        return array(
            array('title' => t('Cover Sub Title'), 'menuClass' => '', 'spanClass' => 'cover-sub-title', 'forceBlock' => '1'),
            array('title' => t('Cover Main Title'), 'menuClass' => '', 'spanClass' => 'cover-main-title', 'forceBlock' => '1'),
            array('title' => t('Cover Medium Title'), 'menuClass' => '', 'spanClass' => 'cover-medium-title', 'forceBlock' => '1'),
            array('title' => t('Title Line'), 'menuClass' => '', 'spanClass' => 'h2-line', 'forceBlock' => '1'),
            array('title' => t('Title Line Center'), 'menuClass' => '', 'spanClass' => 'h2-line h2-line_center', 'forceBlock' => '1'),
            array('title' => t('Title Line Center mb-60'), 'menuClass' => '', 'spanClass' => 'h2-line h2-line_center h2-line_m60-bottom', 'forceBlock' => '1'),
            array('title' => t('Main Quote'), 'menuClass' => '', 'spanClass' => 'main-quote', 'forceBlock' => '1'),
        );
    }

    public function registerAssets()
    {
        $u = new \User();
        $adminGroup = \Concrete\Core\User\Group\Group::getByName('Administrators');
        $isAdministrator = ($u->inGroup($adminGroup) || $u->isSuperUser());

        if (!$isAdministrator) {
            $this->providesAsset('javascript', 'jquery');
            $this->providesAsset('css', 'core/frontend/errors');
        }

        $this->providesAsset('css', 'blocks/form');
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        if ($this->languageCode == null) {
            $this->languageCode = Section::getCurrentSection()->getLanguage();
        }

        return $this->languageCode;
    }

    /**
     * @param integer $pageID
     *
     * @return integer
     */
    public function getPageIDInBaseLocale($pageID)
    {
        $locale = \Config::get('concrete.base_locale');
        $relatedPageID = Section::getRelatedCollectionIDForLocale($pageID, $locale);

        if (!is_null($relatedPageID)) {
            return $relatedPageID;
        }

        return $pageID;
    }

    /**
     * @return string
     */
    public function getSiteTreeHomeLink()
    {
        $c = \Page::getCurrentPage();

        return $c->getSiteTreeObject()->getSiteHomePageObject()->getCollectionLink();
    }

    /**
     * @param string $link
     * @param array $attributes
     *
     * @return void
     */
    public function renderEditableAreaWrapperStart($link, $attributes = array()) {
        $c = \Page::getCurrentPage();

        if ($c->isEditMode()) {
            $html = '<div ';
            foreach ($attributes as $attribute => $value) {
                $html .= $attribute . '="' . $value . '" ';
            }

        } else {
            $html = '<a href="' . $link . '" ';
            foreach ($attributes as $attribute => $value) {
                $html .= $attribute . '="' . $value . '" ';
            }
        }

        $html .= '">';

        echo $html;
    }

    /**
     * @return void
     */
    public function renderEditableAreaWrapperEnd() {
        $c = \Page::getCurrentPage();

        if ($c->isEditMode()) {
            echo '</div>';
        } else {
            echo '</a>';
        }
    }

    /**
     * @param Area       $area
     * @param Page       $page
     * @param array      $blockArgs
     *
     * @return void
     */
    public function addBlockToArea(Area $area, Page $page, $blockArgs = array())
    {
        $areaHandle = $area->getAreaHandle();

        $blockType = $this->getBlockTypeByHandle($blockArgs['block_type_handle']);
        $blockArgs = array_slice($blockArgs, 1);

        if ($area->isGlobalArea()) {

            if (is_null(\Stack::getByName($areaHandle))) {
                \Stack::addGlobalArea($areaHandle);
            }

            $stack = \Stack::getByName($areaHandle);
            $stack->addBlock($blockType, 'Main', $blockArgs);

        } else {

            $newBlock = $page->addBlock($blockType, $area, $blockArgs);
            $newBlockID = $newBlock->getBlockID();
            $areaBlockIDs = $page->getBlockIDs($areaHandle);
            $newBlockData = [
                'bID'      => $newBlockID,
                'arHandle' => $areaHandle,
            ];

            if (!is_array($areaBlockIDs)) {
                $areaBlockIDs = array();
            }

            $areaBlockIDs[$areaHandle][] = $newBlockData;

        }

    }

    protected function getBlockTypeByHandle($btHandle)
    {
        if (!isset($blockTypes[$btHandle])) {
            $this->blockTypes[$btHandle] = \BlockType::getByHandle($btHandle);
        }

        return $this->blockTypes[$btHandle];
    }

    /**
     * @param \Page   $pageObj
     * @param array   $filterData
     * @param boolean $count
     *
     * @return array|\Traversable
     *
     * @throws \Exception
     */
    public function getFilteredPageList(\Page $pageObj, $filterData = array(), $count = false)
    {
        $list = new PageList();
        $list->disableAutomaticSorting();

        if ($filterData['filter_by_page_tree_parent']) {
            $parentCID = $pageObj->getSiteTreeObject()->getSiteHomePageID();
        } else {
            $parentCID = $pageObj->cID;
        }

        if ($filterData['find_in_children']) {
            $list->filterByPath(\Page::getByID($parentCID)->getCollectionPath());
        } else {
            $list->filterByParentID($parentCID);
        }

        if ($filterData['query']) {
            $list->filterByKeywords($filterData['query']);
        }

        if ($filterData['tree_node']) {
            foreach ($filterData['tree_node'] as $topic) {
                if (is_object($topic)) {
                    $topicObj = $topic;
                } else {
                    $topicObj = Topic::getByID(intval($topic));
                }

                if (is_object($topicObj) && $topicObj instanceof Topic) {
                    $list->filterByTopic(intval($topicObj->getTreeNodeID()));
                }
            }
        }

        if ($filterData['attributes']) {
            foreach ($filterData['attributes'] as $handle => $value) {
                if (is_bool($value) && $value) {
                    $list->filterByAttribute($handle, '', '!=');
                } else {
                    if (is_array($value)) {
                        
                        $qb = $list->getQueryObject();
                        $expr = $qb->expr();
                        $exprOrXConditions = [];

                        $i = 1;
                        foreach ($value as $valueOR) {
                            $exprOrXConditions[] = $expr->eq("ak_" . $handle, ":" . $handle . $i);
                            $qb->setParameter($handle . $i, $valueOR);
                            $i++;
                        }

                        $exprOrX = call_user_func_array([$expr, 'orX'], $exprOrXConditions);
                        $qb->andWhere($exprOrX);

                    } else {
                        $list->filterByAttribute($handle, $value);
                    }
                }
            }
        }

        if ($filterData['pageType']) {
            $list->filterByPageTypeHandle($filterData['pageType']);
        }

        if ($filterData['isFeatured']) {
            $list->filterByIsFeatured(1);
        }

        if ($filterData['excludeCIDs']) {
            $list->filter("p.cID", $filterData['excludeCIDs'], '!=');
        }

        if ($count) {
            return $list->getTotalResults();
        }

        if ($filterData['sort_by'] && $count) {

            foreach ($filterData['sort_by'] as $field => $direction) {
                $list->sortBy($field, $direction);
            }

        } else {
            $list->sortByDisplayOrder();
        }


        if ($filterData['item_per_page'] && $filterData['paginate']) {
            $list->setItemsPerPage($filterData['item_per_page']);
            $pagination = $list->getPagination();
            $currentPageResults = $pagination->getCurrentPageResults();

            if ($filterData['paginate'] && $pagination->haveToPaginate()) {
                $arguments = array(
                    'prev_message'        => '',
                    'next_message'        => '',
                    'dots_message'        => '',
                    'active_suffix'       => '',
                    'css_container_class' => 'pagination',
                    'css_prev_class'      => 'prev',
                    'css_next_class'      => 'next',
                    'css_disabled_class'  => 'disabled',
                    'css_dots_class'      => 'disabled',
                    'css_active_class'    => 'active',
                );
                $pagination = $pagination->renderDefaultView($arguments);

                $this->setPageListPagination($pagination);
            }

            return $currentPageResults;
        }

        return $list->getResults();
    }

    /**
     * @param string $pagination
     *
     * @return void
     */
    public function setPageListPagination(string $pagination)
    {
        $this->pageListPagination = $pagination;
    }

    /**
     * @return void
     */
    public function renderPageListPagination()
    {
        if (!is_null($this->pageListPagination)) {
            echo $this->pageListPagination;
        }
    }

    /**
     * @param string $handle
     *
     * @return array
     */
    public function getEntryListByHandle($handle)
    {
        $entity = \Express::getObjectByHandle($handle);
        $list = new EntryList($entity);
        $result = $list->getResults();

        return $result;
    }

    public function getThumbnailData(File $image, $thumb_handle)
    {
        $thumbType = Type::getByHandle($thumb_handle);
        $thumbSrc = $image->getThumbnailURL($thumbType->getBaseVersion());

        $data = [];
        $data['src'] = file_exists(substr($thumbSrc, 1)) ? $thumbSrc : $image->getRelativePath();
        $data['altText'] = $this->getImageAltAttribute($image);
        $data['width'] = $thumbType->getWidth();
        $data['height'] = $thumbType->getHeight();

        return $data;
    }

    /**
     * Returns alt (set by attribute) according to active language
     * @param $file
     * @return string
     */
    public function getImageAltAttribute($file)
    {
        $activeLanguage = $this->getLanguageCode();

        if (is_object($file) && $file->getAttribute('image_alt_attr_' . $activeLanguage)) {
            $alt = $file->getAttribute('image_alt_attr_' . $activeLanguage);
        } else {
            $alt = Config::get('site.sites.default.name');
        }

        return $alt;
    }

    public function getGlobalArea($attrName){
        $db =  \Database::connection();
        $query = $db->query("SELECT btCachedBlockOutput FROM CollectionVersionBlocksOutputCache where arHandle LIKE '$attrName' "); 
         $dataSector[] = $query->fetchRow();
        return $dataSector[0]['btCachedBlockOutput']; 
    }
}