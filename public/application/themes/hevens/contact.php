<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
?>
<?php $this->inc('elements/header.php'); ?>
  <div id="barba-wrapper">
    <div class="barba-container">
<div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"></div>
      <!-- cover-->
      <!-- if cover_images.length-->
      <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
      <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
      <!--       +image(cover_image, 1440, 900)-->
      <!--     .cover-hover__images(data-role="cover-hover__images" )-->
      <!--       for image in cover_images-->
      <!--         -var {src} = image-->
      <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
      <!--       .cover-hover__stripes-->
      <!--         .grid-container.full-->
      <!--           .grid-x-->
      <!--             for image in cover_images-->
      <!--               .cell.small-2-->
      <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
      <section class="cover cover_full cover_overlay-bottom" data-title="Cover banner" data-bg="dark" data-animation="parallax-img section" data-use-native-height="true" data-trigger-hook="onLeave" data-offset="0" id="section-cover">
        <div class="cover__bg cover__bg_single-image" data-animation="parallax-item" data-role="main-image">
         <?php
          $bannerImage = new GlobalArea('Contact Banner Image');
          $bannerImage->display();
        ?>
        </div>
        <div class="<?= !($c->isEditMode()) ? 'cover__overlay' : '' ?> layers__center">
          <div class="grid-container full">
            <div class="grid-x grid-margin-x">
              <div class="cell small-10 small-offset-1 medium-offset-1">
                <div class="cover-title cover-title_light cover-title_left" data-role="main-title">
                  <div class="cover-title__eyebrow">
                    <div class="eyebrow"><span <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                      <?php 
                        $bannerHeader = new GlobalArea('Contact Header');
                        $bannerHeader->display();
                      ?>
                    </span></div>
                  </div>
                  <div class="cover-title__title">
                    <h1 <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                      <?php 
                        $pageName = new Area('Page Name');
                        $pageName->display();
                      ?>
                    </h1>
                  </div>
                  <div class="cover-title__description">
                    <p <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                      <?php 
                        $getTouch = new GlobalArea('Get In Touch');
                        $getTouch->display();
                      ?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="scroll-action"></div>
      </section>
      <main class="content">
        <section class="section layers two-halves indent-xl indent-inner-top-l two-halves_reverse" data-animation="section">
          <div class="grid-container two-halves__bg">
            <div class="grid-x grid-margin-x">
              <div class="cell large-6">
                <div class="card card_two-halves">
                   <div class="card__map" data-role="simple-reveal">
                    <div data-role="reveal-target">
<div class="map" data-role="google-maps-container" data-key="AIzaSyBE2H8ZKFvq456fSnSi0n9Zb1hwueti0LM" data-coords="{&quot;lat&quot;: 46.022969, &quot;lng&quot;: 7.747939}" data-options="{&quot;zoom&quot;: 18, &quot;mapTypeId&quot;: &quot;roadmap&quot;}"></div>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
          <div class="grid-container two-halves__overlay layers__center">
            <div class="grid-x grid-margin-x">
              <div class="cell large-4 large-offset-7">
                <div class="two-halves__content">
                  <div class="title" data-animation="title">
                    <h2 class="visually-hidden">                    
                      <?php 
                        $location = new Area('Location');
                        $location->display();
                      ?>
                    </h2><span class="h4" <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                     <?php 
                        $location = new Area('Location');
                        $location->display();
                      ?>
                      </span>
                    <div class="styling-dots styling-dots_left indent-inner-top-m" data-animation="fade-in-up">
                      <div class="styling-dots__dot"></div>
                      <div class="styling-dots__dot"></div>
                      <div class="styling-dots__dot"></div>
                    </div>
                  </div>
                  <div class="editable editable_indent-l" data-animation="fade-in-up_text">
                    <p>
                      <!--  capitalize the first letter of the first word;-->
                      <?php 
                        $description = new Area('Description');
                        $description->display();
                      ?>                     
                    </p>
                  </div>
                  <ul class="contacts contacts_column">
                    <li class="contacts__item contacts-item">
                      <div class="contacts-item__title" data-animation="fade-in-up"><span>
                        <?php 
                            $address = new GlobalArea('Address Text');
                            $address->display();
                          ?>
                      </span></div>
                      <div class="contacts-item__content" data-animation="fade-in-up">
                        <address>
                          <?php 
                            $address = new GlobalArea('Address');
                            $address->display();
                          ?>                          
                        </address>
                      </div>
                    </li>
                    <li class="contacts__item contacts-item">
                      <div class="contacts-item__title" data-animation="fade-in-up"><span>
                      <?php 
                        $phone = new GlobalArea('Phone Text');
                        $phone->display();
                      ?>                      
                      </span></div>
                      <div class="contacts-item__content" data-animation="fade-in-up">
                      <?php 
                        $phone = new GlobalArea('Phone Number');
                        $phone->display();
                      ?>                        
                      </div>
                    </li>
                    <li class="contacts__item contacts-item">
                      <div class="contacts-item__title" data-animation="fade-in-up"><span>
                      <?php 
                        $email = new GlobalArea('Email Text');
                        $email->display();
                      ?>
                      </span></div>
                      <div class="contacts-item__content" data-animation="fade-in-up">
                        <?php 
                          $email = new GlobalArea('Email Address');
                          $email->display();
                        ?>                       
                      </div>
                    </li>
                    <li class="contacts__item contacts-item">
                      <div class="contacts-item__title" data-animation="fade-in-up"><span>Social</span></div>
                      <div class="contacts-item__content" data-animation="fade-in-up">
                         <ul class="social">
                        <?php 
                          $social = new GlobalArea('Social');
                          $social->display();
                        ?>
                       
                          <!-- <li><a href="facebook.com" title="Our facebook" data-custom-hover="true"><i class="icon-facebook"></i></a></li>
                          <li><a href="pinterest.com" title="Our pinterest" data-custom-hover="true"><i class="icon-pinterest"></i></a></li>
                          <li><a href="twitter.com" title="Our twitter" data-custom-hover="true"><i class="icon-twitter"></i></a></li>
                          <li><a href="instagram.com" title="Our instagram" data-custom-hover="true"><i class="icon-instagram"></i></a></li> -->
                        </ul>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section indent-xl">
          <div class="overlay-image overlay-image_br overlay-image_birds">
            <!-- Attention: responsive image used. Require srcset -->
            <!-- image sizes: default - 322x224, retina @2x - 644x448 -->
            <!-- please add size attributes to the image--><!-- <img data-src="images/overlays/birds_322x224.png" data-srcset="images/overlays/birds_322x224.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="322" height="224"> -->
            <?php
                $contact = new GlobalArea('Our Room Image Br');
                $contact->display();
            ?>
          </div>
          <div class="grid-container" data-animation="section">
            <div class="grid-x grid-margin-x position-relative">
              <div class="cell large-10 large-offset-1 bg-white indent-inner-top-l indent-inner-bot-xl">
                <div class="title title_center" data-animation="title">
                  <h2 class="visually-hidden">                 
                  <?php 
                    $contactHeading = new Area('Contact Heading');
                    $contactHeading->display();
                  ?>
                  </h2><span class="h4" <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                  <?php 
                    $contactUs = new Area('Contact Us');
                    $contactUs->display();
                  ?>
                  </span>
                  <div class="styling-dots indent-inner-top-m" data-animation="fade-in-up">
                    <div class="styling-dots__dot"></div>
                    <div class="styling-dots__dot"></div>
                    <div class="styling-dots__dot"></div>
                  </div>
                </div>
                 <div class="contact-form">
                  <?php 
                    $contactForm = new GlobalArea('Contact Us Form');
                    $contactForm->display(); 
                  ?>
                </div> 
              </div>
            </div>
          </div>
        </section>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
        <?php $this->inc('elements/footer.php'); ?>