<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$ih = Loader::helper('image'); 
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
?>
<style type="text/css">
  .pagination>li>a, .pagination>li>span
{
  border : 0px solid #ddd !important;
  color : currentColor !important;
}
.application .pagination li.is-active a, .application .pagination li.is-active span
{
  color:#fff !important;
}

</style>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" header_light" data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
                }">
            </div> 

              <?php if($c->isEditMode()) { 
                $cls = ""; 
                $split= '';
              }
              else
              {
                $cls = "cover__overlay";
                $split ='data-split-text="true"';
              }
            ?>
            <section class="cover cover_small cover_overlay-bottom cover_light" data-title="Cover banner" data-bg="dark" data-animation="section" id="section-cover">
              <div class="cover__overlay layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_left cover-title_subpage" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow"><span <?=$split?>>
                            <?php 
                              $stay_Updated = new GlobalArea('Stay Updated Text');
                              $stay_Updated->display();
                          ?></span></div>
                        </div>
                        <div class="cover-title__title">
                          <?php if (!$c->isEditMode()) { ?>
                          <h1 data-split-text="true">
                          <?php } ?>
                            <?php 
                              $page_title = new Area('Page Title Name');
                              $page_title->display();
                          ?>
                          <?php if (!$c->isEditMode()) { ?>
                          </h1>
                          <?php } ?>
                        </div>
                        <div class="cover-title__description">
                          <?php if (!$c->isEditMode()) { ?>
                          <p data-split-text="true">
                          <?php } ?>
                            <?php 
                              $pageCaption = new Area('Page Caption');
                              $pageCaption->display();
                            ?>
                          <?php if (!$c->isEditMode()) { ?>  
                          </p>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
              <div class="overlay-image overlay-image_br overlay-image_zermatt-bottom">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 610x414, retina @2x - 1220x828 -->
                <!-- please add size attributes to the image-->
                <?php 
                    $zermatt = new GlobalArea('zermatt Image');
                    $zermatt->display();
                ?>
              </div>
            </section>

            <?php if($c->isEditMode()) { 
                $cls = ""; 
                $role= '';
              }
              else
              {
                $cls = "cover__overlay";
                $role ='data-role="grid-with-filters"';
              }
            ?>

            <main class="content">
              <section class="section indent-inner-top-l indent-xl" <?=$role?> data-animation="section" data-offset="150">
                <div class="grid-container">
                  <div class="grid-x grid-margin-x visually-hidden indent-l">
                    <div class="cell large-10 large-offset-1">
                      <ul class="tab-links" data-role="animated-list-linefilters-container">
                        <li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide">
                          <?php if (!$c->isEditMode()) { ?>
                          <a href="#" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link filters-item" data-filter="*">
                          <?php } ?>
                          All
                          <?php if (!$c->isEditMode()) { ?>
                        </a>
                      <?php } ?>
                      </li><span class="tab-links__line" data-role="forward-line">
                          <div class="styling-dots">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div></span>
                      </ul>
                    </div>
                  </div>
                    <?php 
                      $news_List = new Area('News List');
                      $news_List->display();
                    ?> 
                </div>
              </section>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
<?php $this->inc('elements/footer.php'); ?>