<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$handle = $p->getCollectionHandle();
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
$logoColor = $p->getAttribute('logo_color'); 
?>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes="  header_light" data-html-classes=" " data-namespace="<?=$namespace?>" data-logo-color="<?=$logoColor?>"
              data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"
              ></div>
            <!-- cover-->
            <!-- if cover_images.length-->
            <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
            <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
            <!--       +image(cover_image, 1440, 900)-->
            <!--     .cover-hover__images(data-role="cover-hover__images" )-->
            <!--       for image in cover_images-->
            <!--         -var {src} = image-->
            <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
            <!--       .cover-hover__stripes-->
            <!--         .grid-container.full-->
            <!--           .grid-x-->
            <!--             for image in cover_images-->
            <!--               .cell.small-2-->
            <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
            <section class="cover cover_light" data-title="Cover banner" data-bg="dark" data-animation="section" id="section-cover">
              <div class="<?php if (!$c->isEditMode()) { ?>cover__overlay<?php } ?> layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <h1 class="visually-hidden"><?=$name ?></h1>
                    <div class="cover-title cover-title_dark cover-title_left <?php if (!$c->isEditMode()) { ?>cover-title_subpage<?php } ?> cell small-10 small-offset-1 medium-offset-1" data-role="multi-title">
                      <div class="cover-title__eyebrow" data-role="multi-title__eyebrow">
                        <div class="eyebrow">
                          <span <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
	                            $explore = new GlobalArea('Explore');
	                            $explore->display();
                        	  ?>
                          </span>
                        </div>
                      </div>
                      <div class="cover-title__title layers" data-role="multi-title__titles">
                        <?php if (!$c->isEditMode()) { ?><span class="h1" aria-hidden="true" data-role="multi-title__main-title"><?php } ?>
                          <?php 
                            $coverTitle = new Area('Cover Title');
                            $coverTitle->display();
                          ?>
                        <?php if (!$c->isEditMode()) { ?></span><?php } ?>
                      </div>
                      <div class="cover-title__description" data-role="multi-title__descriptions">
                        <?php if (!$c->isEditMode()) { ?><p data-role="multi-title__main-description"><?php } ?>
                          <?php 
                            $coverDesc = new Area('Cover Short Desc');
                            $coverDesc->display();
                          ?>
                        <?php if (!$c->isEditMode()) { ?></p><?php } ?>
                      </div>
                      <div class="cover-title__link" data-role="multi-title__link">
                        <div class="buttons">
                          <?php if(!$c->isEditMode()) { ?><a href="#" data-role="dynamic-hover-arrow" data-custom-hover="true"><?php } else { ?><div><?php } ?>
                            <?php 
	                            $exploreMore = new GlobalArea('Explore More');
	                            $exploreMore->display();
                        	  ?>
                          <?php if(!$c->isEditMode()) { ?></a><?php } else { ?></div><?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
              <div class="overlay-image overlay-image_br overlay-image_winter-asset">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 742x532, retina @2x - 1484x1064 -->
                <!-- please add size attributes to the image-->
                <!-- <img data-src="images/overlays/winter_742x532.png" data-srcset="images/overlays/winter_742x532.png 2x" draggable="false" alt="8 Ways Media SA" data-lazy-loading="true" width="742" height="532"> -->
                <?php 
                    $winterOverlay = new GlobalArea('Winter Overlay Image');
                    $winterOverlay->display();
                ?>
              </div>
              <!-- <div class="<?php if (!$c->isEditMode()) { ?>layers__end<?php } ?>" data-role="multi-title__direct-links">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-10 large-offset-1">
                      <?php 
                        $expMenu = new Area('Experience Group Menu');
                        $expMenu->display();
                      ?>
                    </div>
                  </div>
                </div>
              </div> -->
            </section>
            <main class="content">
              <div class="overlay-image overlay-image_cl overlay-image_matterhorn-center">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 682x407, retina @2x - 1364x814 -->
                <!-- please add size attributes to the image-->
                  <?php 
		                $matterhornImage = new GlobalArea('Matterhorn Image');
		                $matterhornImage->display();
	                ?>
              </div>
              <div class="grid-container full indent-inner-top-m" data-role="multi-title__items">
                <?php 
                  $expList = new Area('Experiences List');
                  $expList->display();
                ?>
              </div>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
            <?php $this->inc('elements/footer.php'); ?>