
<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = implode("/", $pageSlug);
}
$namespace = $p->getAttribute('page_namespace');
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}
?>

<?php $this->inc('elements/header.php'); ?>
<div id="barba-wrapper">
      <div class="barba-container">
        <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=$namespace?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
            }">
        </div>
        <!-- cover-->
        <!-- if cover_images.length-->
        <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
        <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
        <!--       +image(cover_image, 1440, 900)-->
        <!--     .cover-hover__images(data-role="cover-hover__images" )-->
        <!--       for image in cover_images-->
        <!--         -var {src} = image-->
        <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
        <!--       .cover-hover__stripes-->
        <!--         .grid-container.full-->
        <!--           .grid-x-->
        <!--             for image in cover_images-->
        <!--               .cell.small-2-->
        <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
    <section class="cover cover_full" data-title="Cover banner" data-bg="dark" data-animation="section" data-role="cover-slider" data-mask-src="<?=$view->getThemePath()?>/images/cover/mask.jpg" id="section-cover">
      <div class="cover__bg cover__bg_webgl-image" data-role="main-image" data-cover-slider="scene">
      	<?php
     		$bannerImage = new GlobalArea('Banner Image');
      		$bannerImage->display();
      	?>
      <!--   <div class="visually-hidden" data-src="images/cover/1440x900/image-2_1440x900.jpg" data-type="img" data-cover-slider="slide"></div> -->
      </div>
      <div class="<?= !($c->isEditMode()) ? 'cover__overlay' : '' ?> layers__center">
        <div class="grid-container full">
          <div class="grid-x grid-margin-x">
            <div class="cell large-10 large-offset-1">
              <h1 class="visually-hidden">
                <?php
                    $bannerTitle = new Area('Banner Title');
                    $bannerTitle->display();
                ?> 
                <?php
                    $bannerHeading = new Area('Banner Heading');
                    $bannerHeading->display();
                ?>
                  
                </h1>
              <div class="cover-title cover-title_light" data-role="main-title">
                <div class="cover-title__title"><span <?= !($c->isEditMode()) ? 'class="h1 cover-hover__title cover-hover__title_main" aria-hidden="true" data-split-text="true"' : '' ?>  data-role="cover-hover__main-title">
                <?php
                    $bannerTitle = new Area('Banner Title');
                    $bannerTitle->display();
                ?>
                </span>
                  <!-- for image in cover_images-->
                  <!--   span.h1.cover-hover__title(aria-hidden="true" data-split-text="true" data-role="cover-hover__title")= image.title-->
                </div>
                <div class="cover-title__description"><span <?= !($c->isEditMode()) ? 'class="cover-hover__description cover-hover__description_main" data-split-text="true"' : '' ?>  aria-hidden="true" data-role="cover-hover__main-description">
                <?php
                    $bannerHeading = new Area('Banner Heading');
                    $bannerHeading->display();
                ?>
                </span>
                  <!-- for image in cover_images-->
                  <!--   span(data-split-text="true" aria-hidden="true" data-role="cover-hover__description").cover-hover__description= image.description-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
       <div class="<?= !($c->isEditMode()) ? 'cover__social' : '' ?>">
        <div class="grid-container full">
          <div class="grid-x grid-margin-x">
            <div class="cell small-1 flex-container flex-dir-column align-right">
              <div data-role="hash-nav-container"></div>
              <!-- <ul class="contacts">
                <?php
                    $socialLink = new GlobalArea('Social Link');
                    $socialLink->display();
                ?>
              </ul> -->
            </div>
          </div>
        </div>
      </div>
      <div class="scroll-action"></div>
    </section>
        <main class="content">
          <section class="section indent-inner-top-xl indent-inner-bot-xl sm-indent-inner-top-xl" id="section-about" data-title="Explore the hotel" data-bg="light">
            <div class="<?= !$c->isEditMode() ? 'overlay-image' : ''?> overlay-image_bl overlay-image_deer">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 199x381, retina @2x - 398x762 -->
              <!-- please add size attributes to the image-->
                <?php
                    $bannerImage = new GlobalArea('About Image');
                    $bannerImage->display();
                ?>
<!--               <img data-src="images/overlays/deer_199x381.png" data-srcset="images/overlays/deer_199x381.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="199" height="381"> -->
            </div>
            <div class="grid-container">
              <div class="grid-x grid-margin-x">
                <div class="cell small-10 small-offset-2 medium-offset-2" data-role="card-overflow-text">
                  <div class="simple-slider" data-role="simple-slider-container">
                    <div class="position-relative" data-role="slider-container">
                      <div class="swiper-controls">
                        <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                        <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                      </div>
                      <div class="grid-container">
                        <div class="grid-x grid-margin-x">
                          <div class="cell">
                            <?php
                                $imageSlider = new GlobalArea('About Image Slider');
                                $imageSlider->display();
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-role="overflow-content">
                    <p data-role="overflow-text">
                        <?php
                            $imageSliderText = new Area('Image Slider Text');
                            $imageSliderText->display();
                        ?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="grid-x grid-margin-x indent-inner-top-l" data-animation="section" data-trigger-offset="-200">
                <div class="cell large-8 large-offset-3 medium-10 medium-offset-2">
                      <div class="styling-dots indent-m" data-animation="fade-in-up">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                      </div>
                      <div class="editable editable_two-columns" data-animation="fade-in-up_text">
                      <?php
                        $aboutText = new Area('About Text');
                        $aboutText->display();
                      ?>
                      </div>
                    <div class="buttons" data-animation="fade-in-up">
                      <?php
                        $aboutText = new Area('About Button Text');
                        $aboutText->display();
                      ?>
                    </div>
                  </div>
              </div>
            </div>
          </section>
          <section class="section indent-inner-bot-xl" id="section-rooms" data-title="Our rooms" data-bg="light">
            <div class="<?= !$c->isEditMode() ? 'overlay-image' : ''?> overlay-image_tr overlay-image_matterhorn-682x407">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 682x407, retina @2x - 1364x814 -->
              <!-- please add size attributes to the image-->
                <?php
                    $ourRoomImage = new GlobalArea('Our Room Image Tr');
                    $ourRoomImage->display();
                ?>
           
<!--               <img data-src="images/overlays/matterhorn_682x407.png" data-srcset="images/overlays/matterhorn_682x407.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="682" height="407"> -->
            </div>
            <div class="<?= !$c->isEditMode() ? 'overlay-image' : ''?> overlay-image_bl overlay-image_matterhorn">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 509x614, retina @2x - 1018x1228 -->
              <!-- please add size attributes to the image-->
                <?php
                    $ourRoomImage = new GlobalArea('Our Room Image Bl');
                    $ourRoomImage->display();
                ?>
<!--               <img data-src="images/overlays/zermatt-2_509x614.png" data-srcset="images/overlays/zermatt-2_509x614.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="509" height="614"> -->
            </div>
            <div class="<?= !$c->isEditMode() ? 'overlay-image' : ''?> overlay-image_br overlay-image_birds">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 322x224, retina @2x - 644x448 -->
              <!-- please add size attributes to the image-->
                <?php
                    $ourRoomImage = new GlobalArea('Our Room Image Br');
                    $ourRoomImage->display();
                ?>
<!--               <img data-src="images/overlays/birds_322x224.png" data-srcset="images/overlays/birds_322x224.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width=" " height="224"> -->
            </div>
            <div class="grid-container indent-l" data-animation="section">
              <div class="grid-x grid-margin-x">
                <div class="cell large-5 large-offset-1">
                  <div class="eyebrow" data-animation="title"><span <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>> 
                    <?php
                        $explore = new GlobalArea('Explore');
                        $explore->display();
                    ?>
                  </span></div>
                  <div class="title" data-animation="title">
                    <h2 <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                        <?php
                            $ourRooms = new Area('Our Rooms');
                            $ourRooms->display();
                        ?>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="grid-container" data-role="slider-mini-gallery slider-container">
                <?php
                    $ourRoomsList = new Area('Our Rooms List');
                    $ourRoomsList->display();
                ?>

            </div>
          </section>            
          <section class="section indent-inner-bot-xl" id="section-offers" data-title="Our Offers" data-bg="light" data-animation="section">
            <div class="<?= !$c->isEditMode() ? 'overlay-image' : ''?> overlay-image_br overlay-image_zermatt-610x414">
              <!-- Attention: responsive image used. Require srcset -->
              <!-- image sizes: default - 610x414, retina @2x - 1220x828 -->
              <!-- please add size attributes to the image-->
              <?php
                    $ourOfferList = new GlobalArea('Our Offer List Br');
                    $ourOfferList->display();
                ?>
<!--               <img data-src="images/overlays/zermatt_610x414.png" data-srcset="images/overlays/zermatt_610x414.png 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" width="610" height="414"> -->
            </div>
            <div class="grid-container indent-l">
              <div class="grid-x grid-margin-x">
                <div class="cell large-5 large-offset-1">
                  <div class="eyebrow" data-animation="title"><span <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                    <?php
                        $explore = new GlobalArea('Explore');
                        $explore->display();
                    ?>                        
                    </span></div>
                  <div class="title" data-animation="title">
                    <h2 <?= !($c->isEditMode()) ? 'data-split-text="true"' : '' ?>>
                        <?php
                            $ourRooms = new Area('Offers');
                            $ourRooms->display();
                        ?>
                    </h2>
                  </div>
                </div>
<!--                 <div class="cell large-6 flex-container align-bottom" data-animation="fade-in-right">
                    <div class="tab-extra-link tab-extra-link_bottom">
                        <?php
                            $allOffer = new Area('All Offers');
                            $allOffer->display();
                        ?>
                    </div>
                </div> -->
              </div>
            </div>
            <?php
                $ourOffers = new Area('Our Offers');
                $ourOffers->display();
            ?>
          </section>
          <section class="section indent-l bg-gray" id="section-services" data-title="Our services" data-bg="light" data-animation="section">
                <div class="home-services layers" data-role="home-services">
                  
                        <?php
                            $ourServices = new Area('Our Services List');
                            $ourServices->display();
                        ?>                    
                </div>
              </section>

<div class="body-lines">
<div class="grid-container full">
<div class="grid-x">
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
  <div class="body-lines__line cell small-2"></div>
</div>
</div>
</div>
        </main>
        <?php $this->inc('elements/footer.php'); ?>