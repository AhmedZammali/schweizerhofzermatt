<?php  defined('C5_EXECUTE') or die("Access Denied."); 
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$this->inc('elements/header.php');
?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes="  header_light" data-html-classes=" " data-lang-links="{
                &quot;en&quot;: &quot;/page-404.html?en&quot;,
                &quot;fr&quot;: &quot;/page-404.html?fr&quot;,
                &quot;ge&quot;: &quot;/page-404.html?ge&quot;
              }"></div>
            <main class="content">
              <section class="section indent-inner-top-l indent-xl">
                <div class="grid-container">
                  <div class="grid-x grid-margin-x">
                    <div class="cell large-8 large-offset-2">
                      <div class="title">
                        <h1 class="visually-hidden">Page not Found</h1>
                        <span class="h2">
                          <?php
                            $errorTitle = new GlobalArea('404 Error Title ');
                            $errorTitle->display();
                          ?>
                        </span>
                      </div>
                      <div class="editable">
                        <p>
                          <?php
                            $errorMessage = new GlobalArea('404 Error Message ');
                            $errorMessage->display();
                          ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <script>
                document.addEventListener('DOMContentLoaded', function () {
                  setTimeout(function () {
                    window.location = '<?php echo View::url('/'); ?>'; // insert current language from PHP here, for example '/fr'
                  }, 5000);
                });
              </script>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
            <?php $this->inc('elements/footer.php'); ?>