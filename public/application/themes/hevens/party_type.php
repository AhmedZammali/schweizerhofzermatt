<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$lang = Localization::activeLanguage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
  $pageUrlSlug = implode("/", $pageSlug);
} else {
  $pageUrlSlug = $pageSlug;
}
$pastEvents = basename($_SERVER['QUERY_STRING']);
$curren_url = strtok($_SERVER["REQUEST_URI"],'?');

$namespace = $p->getAttribute('page_namespace');
$handle = $p->getCollectionHandle();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
    $locale = $al->getLanguage();
}
if (!$locale) {
    $locale = \Localization::activeLocale();
    $al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
    $pc = new Checker(\Page::getByID($m->getCollectionID()));
    if ($pc->canRead()) {
        $mlAccessible[] = $m;
        $languages[] = $m->getCollectionID();
    }
}

$this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">     
            <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"></div>

            <section class="cover cover_full cover_overlay-bottom" data-title="Cover banner" data-bg="dark" data-animation="parallax-img section" data-use-native-height="true" data-trigger-hook="onLeave" data-offset="0" id="section-cover">
              <div class="cover__bg cover__bg_single-image" data-animation="parallax-item" data-role="main-image">
                <?php
                  $bannerImage = new Area('Party Type Banner Image (1440X900) ');
                  $bannerImage->display();
              ?>
              </div>
              <?php if($c->isEditMode()) { 
                $cls = ""; 
                $split= '';
              }
              else
              {
                $cls = "cover__overlay";
                $split ='data-split-text="true"';
              }
            ?>
              <div class="<?=$cls?> layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_light cover-title_left" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow"><span <?=$split?>> 
                            <?php $text = new GlobalArea("Explore");
                                $text->display();
                            ?></span></div>
                        </div>
                        <div class="cover-title__title">
                          <h1 <?=$split?>> 
                            <?php $title = new Area("Party Title");
                                $title->display();
                            ?></h1>
                        </div>
                        <div class="cover-title__description">
                          <p <?=$split?>><?php $headerText = new Area("Party Sub Heading");
                                $headerText->display();
                            ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<!--               <div class="cover__social">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-1 flex-container flex-dir-column align-right">
                      <ul class="contacts">
                        <?php 
                            $socialshare = new GlobalArea('Social Link');
                            $socialshare->display();
                        ?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="scroll-action"></div>
            </section>
            <main class="content">
              <section class="section indent-inner-top-l" data-role="<?php if(!$c->isEditMode()) { ?>grid-with-filters<?php }?>" data-animation="section" data-offset="150">
                <div class="grid-container">
                  <div class="grid-x grid-margin-x indent-l">
                    <div class="cell medium-8 medium-offset-1">
                      <ul class="tab-links" data-role="animated-list-linefilters-container">
                        <li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide">                         	
                        	<?php if(!$c->isEditMode()) { ?>
                         		<a href="#" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link filters-item" data-filter="*">
                         	<?php } ?>
                         	<?php 
                         		$allFilterText = new GlobalArea("All Filters Text");
                                $allFilterText->display();
                            ?>
                            <?php if(!$c->isEditMode()) { ?>
                        		</a>
                        	<?php } ?></li>
                        <?php                            
                            switch($lang) {
                                case 'fr':
                                        $filter = new Area("Party Filters FR");
                                		$filter->display();
                                    break;
                                case 'de':
                                        $filter = new Area("Party Filters DE");
                                		$filter->display();
                                    break;
                                default:
                                        $filter = new Area("Party Filters EN");
                                		$filter->display();
                            }
                        ?>
                        <span class="tab-links__line" data-role="forward-line">
                        <span class="tab-links__line" data-role="forward-line">
                          <div class="styling-dots">
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                            <div class="styling-dots__dot"></div>
                          </div></span>
                      </ul>
                    </div>
                    <div class="cell medium-2">
                      <div class="tab-extra-link">
                        <?php if($pastEvents == "pastEvents") {?>
                        <?php if(!$c->isEditMode()) { ?>
                        <a data-custom-hover="true" href="<?=$curren_url?>">
                        	<?php } ?>
	                        <?php 
	                          $allPresentEvents = new GlobalArea("All Present Events");
	                          $allPresentEvents->display();
	                        ?>
                        <?php if(!$c->isEditMode()) { ?>
                      	</a>
                  		<?php } ?>
                      <?php } else { ?>
                      	<?php if(!$c->isEditMode()) { ?>
                        <a data-custom-hover="true" href="?pastEvents">
                        <?php } ?>
                        <?php 
                          $allPastEvents = new GlobalArea("All Past Events");
                          $allPastEvents->display();
                        ?>
                        <?php if(!$c->isEditMode()) { ?>
                        </a>
                    	<?php } ?>
                      <?php } ?>

                      </div>
                    </div>
                  </div>
                  <div <?php if(!$c->isEditMode()) { ?> class="grid-x grid-margin-x" <?php } ?>>
							<?php $filter = new Area("Offer Filters");
                                $filter->display();
                            ?>
                  </div>
                </div>
              </section>
                <div class="body-lines">
                  <div class="grid-container full">
                    <div class="grid-x">
                      <div class="body-lines__line cell small-2"></div>
                      <div class="body-lines__line cell small-2"></div>
                      <div class="body-lines__line cell small-2"></div>
                      <div class="body-lines__line cell small-2"></div>
                      <div class="body-lines__line cell small-2"></div>
                    </div>
                  </div>
                </div>
            </main>
<?php $this->inc('elements/footer.php'); ?>