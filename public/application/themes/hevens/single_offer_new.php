<?php  defined('C5_EXECUTE') or die("Access Denied."); 
use Concrete\Core\Multilingual\Page\Section\Section;
use Concrete\Core\Permission\Checker;
$p = Page::getCurrentPage();
$pageSlug = array_filter(explode("/", $c->getCollectionPath()));
if(count($pageSlug)) {
$pageUrlSlug = implode("/", $pageSlug);
} else {
$pageUrlSlug = $pageSlug;
}
$namespace = $p->getAttribute('page_namespace');
$handle = $p->getCollectionHandle();
$name = $p->getCollectionName();
$ml = Section::getList();
$c = \Page::getCurrentPage();
$al = Section::getBySectionOfSite($c);
$languages = [];
$locale = null;
if ($al !== null) {
$locale = $al->getLanguage();
}
if (!$locale) {
$locale = \Localization::activeLocale();
$al = Section::getByLocale($locale);
}
$mlAccessible = [];
foreach ($ml as $m) {
$pc = new Checker(\Page::getByID($m->getCollectionID()));
if ($pc->canRead()) {
$mlAccessible[] = $m;
$languages[] = $m->getCollectionID();
}
}
$lang = Localization::activeLanguage();
$ih = Loader::helper('image');  
?>
<?php $this->inc('elements/header.php'); ?>
        <div id="barba-wrapper">
          <div class="barba-container">
            <div data-role="dynamic-classes" data-header-classes=" " data-html-classes=" " data-namespace="<?=strtolower($namespace)?>" data-lang-links="{
              &quot;en&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[0]?>&quot;,
              &quot;fr&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[1]?>&quot;,
              &quot;de&quot;: &quot;<?= View::url('/');?>/<?=$pageUrlSlug?>/switch_language/<?= $p->getCollectionID()?>/<?=$languages[2]?>&quot;
              }"></div>
            <!-- cover-->
            <!-- if cover_images.length-->
            <!--   .cover__bg.cover-hover(data-animation="parallax-item" data-role="cover-hover")-->
            <!--     .cover-hover__main-image(data-role="cover-hover__main-image")-->
            <!--       +image(cover_image, 1440, 900)-->
            <!--     .cover-hover__images(data-role="cover-hover__images" )-->
            <!--       for image in cover_images-->
            <!--         -var {src} = image-->
            <!--         +image_lazy(src, 1440, 900)('data-manual-loading' = 'true')-->
            <!--       .cover-hover__stripes-->
            <!--         .grid-container.full-->
            <!--           .grid-x-->
            <!--             for image in cover_images-->
            <!--               .cell.small-2-->
            <!--                 a(href= image.link title= image.title data-custom-hover="true" data-hover-text="View" data-role="cover-hover__stripe").cover-hover__stripe-->
            <section class="<?php if (!$c->isEditMode()) { ?>cover cover_full cover_overlay-bottom<?php } ?>" data-title="Cover banner" data-bg="dark" <?php if (!$c->isEditMode()) { ?>data-animation="parallax-img section"<?php } ?> data-use-native-height="true" data-trigger-hook="onLeave" data-offset="0" id="section-cover">
              <div class="<?php if (!$c->isEditMode()) { ?>cover__bg cover__bg_single-image<?php } ?>" data-animation="parallax-item" data-role="main-image">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 1440x900, retina @2x - 2880x1800 --><!-- <img src="images/cover/1440x900/image-3_1440x900.jpg" srcset="images/cover/1440x900/image-3_1440x900.jpg 2x" draggable="false" alt="8 Ways Media SA"> -->
                <?php 
                  $coverImage = new Area('Offers Cover Image');
                  $coverImage->display();
                ?>
              </div>
              <div class="<?php if (!$c->isEditMode()) { ?>cover__overlay<?php } ?> layers__center">
                <div class="grid-container full">
                  <div class="grid-x grid-margin-x">
                    <div class="cell small-10 small-offset-1 medium-offset-1">
                      <div class="cover-title cover-title_light cover-title_left" data-role="main-title">
                        <div class="cover-title__eyebrow">
                          <div class="eyebrow">
                            <span <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                              <?php 
                                $explore = new GlobalArea('Explore');
                                $explore->display();
                              ?>
                            </span>
                          </div>
                        </div>
                        <div class="cover-title__title">
                          <h1 <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                              $coverTitle = new Area('Offers Cover Title');
                              $coverTitle->display();
                            ?>
                          </h1>
                        </div>
                        <div class="cover-title__description">
                          <p <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                              $coverDesc = new Area('Offers Cover Description');
                              $coverDesc->display();
                            ?>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="scroll-action"></div>
            </section>
            <main class="content">
              <?php 
                $offersList = new Area('Offers List');
                $offersList->display();
              ?>
<div class="body-lines">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
      <div class="body-lines__line cell small-2"></div>
    </div>
  </div>
</div>
            </main>
            <?php $this->inc('elements/footer.php'); ?>