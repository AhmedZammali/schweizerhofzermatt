<?php
namespace Application\Attribute\MenuTreeSelector;

use Concrete\Core\Attribute\Controller as AttributeTypeController;
use Concrete\Core\Attribute\FontAwesomeIconFormatter;
use Concrete\Core\Entity\Attribute\Value\Value\NumberValue;
use Concrete\Core\Page\Page;

class Controller extends AttributeTypeController
{
    public function getIconFormatter()
    {
        return new FontAwesomeIconFormatter('sitemap');
    }

    public function getAttributeValueClass()
    {
        return NumberValue::class;
    }

    public function form()
    {
        $value = null;
        $pages = array();
        $filters = array();
        if (is_object($this->attributeValue)) { 
            $value = $this->getAttributeValue()->getValue();
            //print_r($value);
        }
        if (!$value) {
            if ($this->request->query->has($this->attributeKey->getAttributeKeyHandle())) {
                $value = $this->createAttributeValue((int) $this->request->query->get($this->attributeKey->getAttributeKeyHandle()));
            }
        }
        $page = Page::getCurrentPage();
        $cID = $page->getCollectionID();
        //echo $id;
        $parentPageID = $page->getCollectionParentID();
        
        $parent = Page::getByID($parentPageID); 
        $pages = $parent->getCollectionChildrenArray();
        $filters[0] = $cID;

        $this->set('value', $value);
        $this->set('pages', $pages);
        $this->set('filters', $filters);
        $this->set('startingPoint', $parentPageID);
        $this->set('page_selector', $this->app->make('helper/form/page_selector'));
    }

    public function getDisplayValue()
    {
        $cID = $this->getAttributeValue()->getValue();
        $page = Page::getByID($cID, 'ACTIVE');
        if (is_object($page) && !$page->isError()) {
            return t('<a href="%s">%s</a>', $page->getCollectionLink(), $page->getCollectionName());
        }
    }

    public function getPlainTextValue()
    {
        $cID = $this->getAttributeValue()->getValue();
        $page = Page::getByID($cID, 'ACTIVE');
        if (is_object($page) && !$page->isError()) {
            return $page->getCollectionLink();
        }
    }

    public function createAttributeValue($value)
    {
        $av = new NumberValue();
        if ($value instanceof Page) {
            $value = $value->getCollectionID();
        }
        $av->setValue($value);

        return $av;
    }

    public function createAttributeValueFromRequest()
    {
        $data = $this->post();
        if (isset($data['value'])) {
            return $this->createAttributeValue((int) $data['value']);
        }
    }

    public function importValue(\SimpleXMLElement $akv)
    {
        if (isset($akv->value)) {
            $c = Page::getByPath((string) $akv->value);
            if (is_object($c) && !$c->isError()) {
                return $c->getCollectionID();
            }
        }
    }

    public function exportValue(\SimpleXMLElement $akn)
    {
        if (is_object($this->attributeValue)) {
            $cID = $this->getAttributeValue()->getValue();
            $page = Page::getByID($cID, 'ACTIVE');
            $avn = $akn->addChild('value', $page->getCollectionPath());
        }
    }
}
