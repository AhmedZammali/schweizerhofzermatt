<?php defined('C5_EXECUTE') or die("Access Denied.");

$navItems = $controller->getNavItems();
$c = Page::getCurrentPage();
foreach ($navItems as $ni) {
$classes = array();

if ($ni->isCurrent) {
//class for the page currently being viewed
$classes[] = 'nav-selected';
}

if ($ni->inPath) {
//class for parent items of the page currently being viewed
$classes[] = 'nav-path-selected';
}


$ni->classes = implode(" ", $classes);
}

//*** Step 2 of 2: Output menu HTML ***/

if (count($navItems) > 0) {
echo '<ul>'; //opens the top-level menu
  foreach ($navItems as $ni) {
      $namespace = $ni->cObj->getAttribute('page_namespace');
      $hoverImage = $ni->cObj->getAttribute('menu_hover_image');
      if($hoverImage) {
        $hoverImage = $ni->cObj->getAttribute('menu_hover_image')->getVersion()->getRelativePath();
      }
      //$hoverImageThumb = Core::make('helper/image')->getThumbnail($hoverImage,512, 512, true)->src;
      if(!$ni->cObj->getAttribute('hide_in_vertical_menu')) { ?>
    <li class="menu__top-item" data-namespace="<?=$namespace ?>">
      <div class="card card_menu" data-role="menu-card" data-asset="<?=$hoverImage?>" ><a class="hidden-link" href="<?=$ni->url?>" title="<?php echo h($ni->name)?>" data-custom-hover="true"><?= h($ni->name)?></a>
        <div class="card__image">
          <?php  $img = $ni->cObj->getAttribute('thumbnail');
              $image_thumbnail_thumb = Core::make('helper/image')->getThumbnail($img,330, 170, true);
              $image_thumbnail_thumb_set = Core::make('helper/image')->getThumbnail($img,660, 340, true);
              if($image_thumbnail_thumb->src==''){
                  $img = $ni->cObj->getAttribute('banner_image');
              $image_thumbnail_thumb = Core::make('helper/image')->getThumbnail($img,330, 170, true);
              $image_thumbnail_thumb_set = Core::make('helper/image')->getThumbnail($img,660, 340, true);
              }
              ?>
          <!-- Attention: responsive image used. Require srcset -->
          <!-- image sizes: default - 330x170, retina @2x - 660x340 --><img data-src="<?=$image_thumbnail_thumb->src?>" data-srcset="<?=$image_thumbnail_thumb_set->src?> 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true" data-manual-loading="true">
        </div>
        <div class="card__label">
          <h5 class="double-text"><?= h($ni->name)?></h5>
        </div>
      </div>
    </li>
  <?php } ?>
<?php } ?> 
</ul> 
<?php }?>
   
