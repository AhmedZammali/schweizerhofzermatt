<?php defined('C5_EXECUTE') or die("Access Denied.");

$navItems = $controller->getNavItems();
$c = Page::getCurrentPage();
foreach ($navItems as $ni) {
    $classes = array();

    if ($ni->isCurrent) {
        //class for the page currently being viewed
        $classes[] = 'nav-selected';
    }

    if ($ni->inPath) {
        //class for parent items of the page currently being viewed
        $classes[] = 'nav-path-selected';
    }

   
    $ni->classes = implode(" ", $classes);
}

//*** Step 2 of 2: Output menu HTML ***/

if (count($navItems) > 0) {
    echo '<ul class="nav">'; //opens the top-level menu

    foreach ($navItems as $ni) {
    	if($ni->cObj->getAttribute('horizontal_menu_name')!=''){
            $name= $ni->cObj->getAttribute('horizontal_menu_name');
        }else{
            $name = $ni->name;
        }
        if($ni->cObj->getAttribute('is_featured')){
	        echo '<li class="' . $ni->classes . '">'; //opens a nav item
	        echo '<a title="'.h($ni->name).'" href="' . $ni->url . '" target="' . $ni->target . '" data-custom-hover="true" class="' . $ni->classes . '">' . h( $name ) . '</a>';

	        if ($ni->hasSubmenu) {
	            echo '<ul>'; //opens a dropdown sub-menu
	        } else {
	            echo '</li>'; //closes a nav item" onfocus="alert('Stored XSS in SEO Name field')"  autofocus="true"

	            echo str_repeat('</ul></li>', $ni->subDepth); //closes dropdown sub-menu(s) and their top-level nav item(s)
	        }
    	}
	}
    echo '</ul>'; //closes the top-level menu
} elseif (is_object($c) && $c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?=t('Empty Auto-Nav Block.')?></div>
<?php

}
