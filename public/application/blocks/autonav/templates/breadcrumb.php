<?php defined('C5_EXECUTE') or die("Access Denied.");

$navItems = $controller->getNavItems(true); // Ignore exclude from nav
$c = Page::getCurrentPage();

if (count($navItems) > 0) {
    echo '<ul>'; //opens the top-level menu
    foreach ($navItems as $ni) {
        if($ni->cObj->getAttribute('new_page_name')) {
            $ni->name = $ni->cObj->getAttribute('new_page_name');
        }
        if ($ni->isCurrent) {
            echo '<li><span>' . $ni->name . '</span></li>';
        } else {
            echo '<li><a href="' . $ni->url . '" target="' . $ni->target . '">' . $ni->name . '</a></li>';
        }
    }

    echo '</ul>';//closes the top-level menu
} elseif (is_object($c) && $c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?=t('Empty Auto-Nav Block.')?></div>
<?php 
}
