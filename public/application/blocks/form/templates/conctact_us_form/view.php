<?php 

defined('C5_EXECUTE') or die("Access Denied.");
use \Concrete\Block\Form\MiniSurvey;
$survey = $controller;
$miniSurvey = new MiniSurvey($b);
$miniSurvey->frontEndMode = true;
$bID = intval($bID);
$qsID = intval($survey->questionSetId);
$formAction = $view->action('submit_form').'#formblock'.$bID;

$questionsRS = $miniSurvey->loadQuestions($qsID, $bID);
$questions = [];

while ($questionRow = $questionsRS->fetchRow()) {
    $question = $questionRow;
    $question['input'] = $miniSurvey->loadInputType($questionRow, false);
    if ($questionRow['inputType'] == 'text') {
        $question['type'] = 'textarea';
    } elseif ($questionRow['inputType'] == 'field') {
        $question['type'] = 'text';
    } else {
        $question['type'] = $questionRow['inputType'];
    }

    $question['labelFor'] = 'for="Question' . $questionRow['msqID'] . '"';

    //Remove hardcoded style on textareas
    if ($question['type'] == 'textarea') {
        $question['input'] = str_replace('style="width:95%"', '', $question['input']);
    }

    $questions[] = $question;
}
$success = (\Request::request('surveySuccess') && \Request::request('qsid') == intval($qsID));
$thanksMsg = $survey->thankyouMsg;
$errorHeader = isset($formResponse) ? $formResponse : null;
$errors = isset($errors) && is_array($errors) ? $errors : [];
if (isset($invalidIP) && $invalidIP) {
    $errors[] = $invalidIP;
}
$errorDivs = '';
foreach ($errors as $error) {
    $errorDivs .= '<div class="error">'.$error."</div>\n"; 
}
$surveyBlockInfo = $miniSurvey->getMiniSurveyBlockInfoByQuestionId($qsID, $bID);
$captcha = $surveyBlockInfo['displayCaptcha'] ? Loader::helper('validation/captcha') : false;
$i=0;
$formName = new GlobalArea('Name');
$formEmail = new GlobalArea('Email');
$formMessage = new GlobalArea('Message');
$nameError = new GlobalArea('Name Required Error Meassage');
$emailError = new GlobalArea('Email Required Error Meassage');
$emailInvaildError = new GlobalArea('Invalid Email Error Meassage');
$gdprError = new GlobalArea('GDPR Required Error Meassage');
?>
<form class="form form_column"action="<?php  echo $formAction ?>" method="POST"  id="miniSurveyView<?php  echo $bID; ?>"  method="POST" data-role="contact-form" data-sitekey="6Lf7634UAAAAAKZyQc0Uk83cSMOXIWfGfpG6QneC">
   <?=Core::make('token')->output('form_block_submit_qs_'.$qsID);?>
  <fieldset class="fieldset form__content-block" data-role="content-block">
  <?php foreach ($questions as $question) { ?>
      <?php  if($question['inputType']=='select') { $data=explode("%%",$question['options']); }
       ?>
    <?php if($question['inputType']=='field'){
       ?>
    <div class="input-block input-block_null input-block_input" data-animation="fade-in-up" data-role="input-block" data-type="input" data-required="<?php echo ($question['required'] == 1) ? 'true' : '' ?>" data-required-message="<?=$nameError->display()?>" data-custom-hover="true" data-hover-text="Chose">
      <div class="input-block__label">
        <label class="label" data-role="label" for="input-block-0"><?=$formName->display()?></label>
      </div>
      <div class="input-block__input">
        <input class="input" id="input-block-0" name="Question<?=$question['msqID']?>" data-role="input" autocomplete="off"></input>
        <!--type= validation || type || 'text'--><span class="input-decoration"><?php  echo $question['question']; ?></span>
      </div>
      <div class="input-block__error" data-role="error"></div>
    </div>
  <?php } else if($question['inputType']=='email'){ ?>
    <div class="input-block input-block_email input-block_input" data-animation="fade-in-up" data-role="input-block" data-type="input" data-required="<?php echo ($question['required'] == 1) ? 'true' : '' ?>" data-required-message="<?=$emailError->display()?>" data-validation="email" data-validation-message="<?=$emailInvaildError->display()?>" data-custom-hover="true" data-hover-text="Chose">
      <div class="input-block__label">
        <label class="label" data-role="label" for="input-block-1"><?=$formEmail->display()?></label>
      </div>
      <div class="input-block__input">
        <input class="input" id="input-block-1" name="Question<?=$question['msqID']?>" data-role="input" autocomplete="off"></input>
        <!--type= validation || type || 'text'--><span class="input-decoration"><?php  echo $question['question']; ?></span>
      </div>
      <div class="input-block__error" data-role="error"></div>
    </div>
  <?php }else if($question['inputType']=='text') {?>
    <div class="input-block input-block_null input-block_textarea" data-animation="fade-in-up" data-role="input-block" data-type="textarea" data-custom-hover="true" data-hover-text="Chose">
      <div class="input-block__label">
        <label class="label" data-role="label" for="input-block-2"><?=$formMessage->display()?></label>
      </div>
      <div class="input-block__input">
        <textarea class="input" id="input-block-2" name="Question<?=$question['msqID']?>" data-role="input" autocomplete="off"></textarea>
        <!--type= validation || type || 'text'--><span class="input-decoration"><?php  echo $question['question']; ?></span>
      </div>
      <div class="input-block__error" data-role="error"></div>
    </div>
  <?php }
      }
     ?>
      <div class="fieldset__bottom">
        <div class="inputs">
      <?php foreach ($questions as $question) { 
        if($question['inputType']=='checkboxlist') { ?>
      <?php if($question['required']==true) { $check="";?>         
        <div class="input-block input-block_null input-block_checkbox" data-animation="fade-in-up" data-role="input-block" data-type="checkbox" data-required="<?php echo ($question['required'] == 1) ? 'true' : '' ?>" data-required-message="<?=$gdprError->display();?>" data-custom-hover="true" data-hover-text="Chose">
        <?php  } else {  $check="checked=1"; ?>
          <div class="input-block input-block_null input-block_checkbox" data-animation="fade-in-up" data-role="input-block" data-type="checkbox" data-custom-hover="true" data-hover-text="Chose">
         <?php } ?>
          <div class="input-block__input input-block__input_checkbox input-block__input_checkbox-align-column">
            <div class="custom-checkbox custom-checkbox_type-input-checkbox">
              <label class="custom-checkbox__label">
                <input class="visually-hidden" type="checkbox" <?php echo $check;?> name="Question<?=$question['msqID']?>_0" value="<?php  echo $question['question']; ?>" data-role="input"><span class="decorator"><i class="icon-checkbox"></i></span><span class="text"><?php  echo $question['question']; ?></span>
              </label>
            </div>
          </div>
          <div class="input-block__error" data-role="error"></div>
        </div> 
      <?php }      
      }
      ?>
      </div>
      <div class="buttons form__content-block" data-animation="fade-in-up" data-role="content-block">
        <button class="button button_dark button_uppercase" type="submit" title="Submit form" data-custom-hover="true"><span><?php  echo h(t($survey->submitText)); ?></span></button>
      </div>
    <input name="qsID" type="hidden" value="<?php  echo $qsID; ?>" />
    <input name="pURI" type="hidden" value="<?php  echo isset($pURI) ? $pURI : ''; ?>" />
    <input name="from-type" type="hidden" value="newsletter" />
    </div>
  </fieldset>
  <div class="input-block input-block_captcha visually-hidden" data-role="captcha-block"></div>
  <div class="form__message" data-role="message-block">
    <div class="form-loader">
      <div class="form-loader__item form-loader__item_circular" data-role="message-loader"><svg viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="21" cy="21" r="20" stroke="#000000" stroke-width="2" fill="none" fill-rule="evenodd"></circle></svg></div>
      <div class="form-loader__item form-loader__item_success" data-role="success-mark"><svg viewBox="0 0 19 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M.7 7.4L6 12.7 18.4.2" stroke="#000000" stroke-width="1" fill="none" fill-rule="evenodd"></path></svg></div>
      <div class="form-loader__item form-loader__item_error" data-role="error-mark"><svg viewBox="0 0 15 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g stroke="#000000" stroke-width="1" fill="none" fill-rule="evenodd"><path d="M13.7 13.4L1.3.9"></path><path d="M1 13.4L13.4.9"></path></g></svg></div>
    </div>
  </div>
</form>
