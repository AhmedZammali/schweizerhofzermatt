<?php defined('C5_EXECUTE') or die('Access Denied.');
$c = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$imageHelper = Core::make('helper/image');
$lang = Localization::activeLanguage();
//$attrLang = ($lang == 'en')?'':'_'.$lang;
$attrLang ='';
if ($tableName) { ?>
    <h2><?=$tableName?></h2>
    <?php if ($tableDescription) { ?>
        <p><?=$tableDescription?></p>
    <?php } ?>
<?php }
    $results = $list->getResults();
?>
   <?php foreach ($results as $key => $result): 
        $order = ($key%2 == 0) ? 0 : 1;
        //$sectionClass = ( $order == 0 ) ? "" : "without-indent";
        $thumbnail = $result->getAttribute('offer_image'.$attrLang);
        $fileSrc = is_object($thumbnail) ? $imageHelper->getThumbnail($thumbnail, 640, 1080, true)->src : false;
        $fileSrcSet = is_object($thumbnail) ? $imageHelper->getThumbnail($thumbnail, 1280, 2160, true)->src : false;
        $title = $result->getAttribute('offer_title'.$attrLang);
        $description = $result->getAttribute('offer_description'.$attrLang); 
        $link = $result->getAttribute('offer_link'.$attrLang);
    ?>
    <?php if(!$order) { ?>
        <section class="section layers two-halves indent-inner-top-l indent-inner-bot-l " data-animation="section">
            <div class="grid-container two-halves__bg">
                <div class="grid-x grid-margin-x">
                <div class="cell large-6 large-offset-6">
                    <div class="card card_two-halves">
                    <div class="card__image" data-role="simple-reveal">
                        <div data-role="reveal-target" data-animation="parallax-img">
                        <div data-animation="parallax-item">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 640x1080, retina @2x - 1280x2160 --><img data-src="<?=$fileSrc ?>" data-srcset="<?=$fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="grid-container two-halves__overlay layers__center">
                <div class="grid-x grid-margin-x">
                <div class="cell large-4 large-offset-1">
                    <div class="two-halves__content">
                    <div class="eyebrow" data-animation="title">
                        <span <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                                $amazingText = new GlobalArea('Amazing Text');
                                $amazingText->display();
                            ?>
                        </span>
                    </div>
                    <div class="title" data-animation="title">
                        <h2 <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>><?=$title ?></h2>
                        <div class="styling-dots styling-dots_left indent-inner-top-m" data-animation="fade-in-up">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        </div>
                    </div>
                    <div class="editable editable_indent-l" data-animation="fade-in-up_text">
                        <?=$description ?>
                    </div>
                    <div class="buttons" data-animation="fade-in-up">
                    <?php if (!$c->isEditMode()) { ?><a class="button button_accent button_full-width button_uppercase" href="<?=$link ?>" data-custom-hover="true"><?php } ?>
                        <span>
                            <?php 
                                $checkOffer = new GlobalArea('Check The Offer button text');
                                $checkOffer->display();
                            ?>
                        </span>
                        <?php if (!$c->isEditMode()) { ?></a><?php } ?>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    <?php } else { ?>
        <section class="section layers two-halves indent-inner-top-l indent-inner-bot-l two-halves_reverse" data-animation="section">
            <div class="overlay-image overlay-image_br overlay-image_matterhorn-682x407 overlay-image_matterhorn">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 682x407, retina @2x - 1364x814 -->
                <!-- please add size attributes to the image-->
                <?php 
                    $matterhornBG = new GlobalArea('Matterhorn Image');
                    $matterhornBG->display();
                ?>
            </div>
            <div class="grid-container two-halves__bg">
                <div class="grid-x grid-margin-x">
                <div class="cell large-6">
                    <div class="card card_two-halves">
                    <div class="card__image" data-role="simple-reveal">
                        <div data-role="reveal-target" data-animation="parallax-img">
                        <div data-animation="parallax-item">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 640x1080, retina @2x - 1280x2160 --><img data-src="<?=$fileSrc ?>" data-srcset="<?=$fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="grid-container two-halves__overlay layers__center">
                <div class="grid-x grid-margin-x">
                <div class="cell large-4 large-offset-7">
                    <div class="two-halves__content">
                    <div class="eyebrow" data-animation="title">
                        <span <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>>
                            <?php 
                                $amazingText = new GlobalArea('Amazing Text');
                                $amazingText->display();
                            ?>
                        </span>
                    </div>
                    <div class="title" data-animation="title">
                        <h2 <?php if (!$c->isEditMode()) { ?>data-split-text="true"<?php } ?>><?=$title ?></h2>
                        <div class="styling-dots styling-dots_left indent-inner-top-m" data-animation="fade-in-up">
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        <div class="styling-dots__dot"></div>
                        </div>
                    </div>
                    <div class="editable editable_indent-l" data-animation="fade-in-up_text">
                        <?=$description ?>
                    </div>
                    <div class="buttons" data-animation="fade-in-up">
                        <?php if (!$c->isEditMode()) { ?><a class="button button_accent button_full-width button_uppercase" href="<?=$link ?>" data-custom-hover="true"><?php } ?>
                            <span>
                                <?php 
                                    if($key == 1) {
                                        $checkOffer = new GlobalArea('Contact Offer button text');
                                    } else {
                                        $checkOffer = new GlobalArea('Check The Offer button text');
                                    }
                                    $checkOffer->display();
                                ?>
                            </span>
                        <?php if (!$c->isEditMode()) { ?></a><?php } ?>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php endforeach; ?>
    