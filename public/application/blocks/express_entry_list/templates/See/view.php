<?php defined('C5_EXECUTE') or die('Access Denied.');

$c = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$imageHelper = Core::make('helper/image');
$entity = Express::getObjectByHandle('gallery_image');
$list = new Concrete\Core\Express\EntryList($entity);
$teams = $list->getResults(); $i=1; 
$lang =  Localization::activeLanguage();
?>

                    <div class="cell large-10 large-offset-1">
                      <ul class="cards-list cards-list_2-items-isotope" data-role="filtered-content"> <?php
                        foreach ($teams as $team) { ?>
                                <?php if($i%3==2){

                                    $fileSrc = $imageHelper->getThumbnail($team->getAttribute('image'),570,650,true)->src;
                                    $fileSrcSet = $imageHelper->getThumbnail($team->getAttribute('image'),1140,1300,true)->src;                              
                                }else{
                                    $fileSrc = $imageHelper->getThumbnail($team->getAttribute('image'),570,380,true)->src;
                                    $fileSrcSet = $imageHelper->getThumbnail($team->getAttribute('image'),1140,760,true)->src;
                                }
                                $galleryLanguage =strtolower($team->getAttribute('language'));
                                $alangs =explode("\n",$galleryLanguage);
                            for($k=0;$k<count($alangs);$k++) {
                                 if($alangs[$k]==$lang) { ?>
                                    <li class="cards-list__item" data-role="filtered-item" data-filter-tags=""><a class="card card_simple card_free-height card_centered-text" href="#" title="<?=$team->getAttribute('title')?>" data-tile="<?=$team->getAttribute('title')?>" data-role="gallery-modal-trigger" data-params="{&quot;url&quot;: &quot;<?= View::url('/');?>/<?=$lang?>/slider_gallery&quot;, &quot;type&quot;: &quot;GET&quot;, &quot;data&quot;:{&quot;ref&quot;: &quot;<?=$team->getID();?>&quot;}}" data-animation="fade-in-up" data-custom-hover="true">
                                        <div class="card__image">
                                          <!-- Attention: responsive image used. Require srcset -->
                                          <!-- image sizes: default - 570x380, retina @2x - 1140x760 --><img data-src="<?=$fileSrc?>" data-srcset="<?=$fileSrcSet?> 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
                                        </div>
                                        <div class="card__content">
                                          <div class="card__label">
                                            <h5><?=$team->getAttribute('title')?></h5>
                                          </div>
                                        </div></a>
                                    </li>
                      <?php  $i++; } }
                            }?> 
                      </ul>
                    </div>
                  