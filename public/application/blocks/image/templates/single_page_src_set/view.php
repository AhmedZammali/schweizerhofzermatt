<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$fileSrc = false;
$fileSrcSet = false;
 $ih = Loader::helper('image');
if (is_object($f) && $f->getFileID()) {
    $maxWidth = 640;
    $maxHeight = 880;
    if ($maxWidth > 0 || $maxHeight > 0) {        
        $file = File::getByID($f->getFileID());
        $fileWidth = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight, true)->width : false;
        $fileHeight = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight, true)->height : false;
        $fileSrc = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight, true)->src : false;
        $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, ($maxWidth * 2), ($maxHeight * 2), true)->src : false; 

    } else {
        $file = File::getByID($f->getFileID());
        $fileSrc = is_object($file) ? $file->getVersion()->getRelativePath() : false; 
        $fileSrcSet = is_object($file) ? $file->getVersion()->getRelativePath() : false;  
    }

    if($maxWidth!=0){
        $width="width='".$fileWidth."'";
    }else{
        $width="";
    }if($maxHeight!=0){
        $height="height='".$fileHeight."'";
    }else{
         $height="";
    }
    ?>
    <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 640x880, retina @2x - 1280x1760 -->
    <img data-src="<?php echo $fileSrc; ?>" data-srcset="<?php echo $fileSrcSet; ?> 2x" draggable="false" alt="<?php echo $altText; ?>" data-lazy-loading="true">
    <?php 
} ?>