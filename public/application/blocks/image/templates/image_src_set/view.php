<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$fileSrc = false;
$fileSrcSet = false;
 $ih = Loader::helper('image');
if (is_object($f) && $f->getFileID()) {
    if ($maxWidth > 0 || $maxHeight > 0) {        
        $file = File::getByID($f->getFileID());
        $fileWidth = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight, true)->width : false;
        $fileHeight = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight, true)->height : false;
        $fileSrc = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight, true)->src : false;
        $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, ($maxWidth * 2), ($maxHeight * 2), true)->src : false; 

    } else {
        $file = File::getByID($f->getFileID());
        $fileSrc = is_object($file) ? $file->getVersion()->getRelativePath() : false;
        $fileSrcSet = is_object($file) ? $file->getVersion()->getRelativePath() : false;
    }
    //if($cropImage) {
    if($maxWidth!=0){
        $width="width='".$maxWidth."'";
    }else{
        $width="";
    }if($maxHeight!=0){
        $height="height='".$maxHeight."'";
    }else{
         $height="";
    }
    //}

    ?>

     <img data-src="<?php echo $fileSrc; ?>" data-srcset="<?php echo $fileSrcSet; ?> 2x"   draggable="false" alt="<?php echo $altText; ?>" data-lazy-loading="true" <?=$width?> <?=$height?>>
    <?php
} ?>