<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$fileSrc = false;
$fileSrcSet = false;
 $ih = Loader::helper('image');
if (is_object($f) && $f->getFileID()) {

    $file = File::getByID($f->getFileID());       
    $fileSrc = is_object($file) ? $ih->getThumbnail($file, 1440, 900, true)->src : false;
    $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 2880, 1800, true)->src : false;

    ?>
    <img src="<?=$fileSrc ?>" srcset="<?=$fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
<?php
}