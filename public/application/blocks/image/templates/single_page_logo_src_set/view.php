<?php
  defined('C5_EXECUTE') or die(_("Access Denied."));
 
  $ih = Loader::helper('image'); // load the c5 image helper
  $img = File::getByID($fID); // get the image
  $imagePath = $img->getURL(); // get the image path
  $imageTitle = $img->getTitle(); // get the image title
  $imgaeThumb = $ih->getThumbnail($img, $maxWidth, $maxHeight,true); // render thumb using the size entered in the block edit screen
  $imgaeThumb_srcset = $ih->getThumbnail($img, $maxWidth*2, $maxHeight*2,true); 
  $linkURL = $controller->getLinkURL(); // get the link url
  $linkTitle = $controller->getaltText(); // get the link text
?> 
 
<?php if($maxWidth==0 || $maxHeight==0) { ?>
<img data-src="<?php echo $imagePath ?>" data-srcset="<?php echo $imagePath ?> 2x" draggable="false" alt="<?=$linkTitle?>" data-lazy-loading="true" class="<?php echo $check?>">
<?php } else { ?>
   <img data-src="<?php echo $imgaeThumb->src ?>" data-srcset="<?php echo $imgaeThumb_srcset->src ?> 2x" draggable="false" alt="<?=$linkTitle?>" data-lazy-loading="true" class="<?php echo $check?>" width="<?php print_r($maxWidth); ?>" height="<?=$maxHeight?>">
 <?php } ?>
