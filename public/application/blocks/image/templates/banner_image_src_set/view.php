<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$fileSrc = false;
$fileSrcSet = false;
 $ih = Loader::helper('image');
if (is_object($f) && $f->getFileID()) {
    if ($maxWidth > 0 || $maxHeight > 0) { 
        $file = File::getByID($f->getFileID());       
        $fileSrc = is_object($file) ? $ih->getThumbnail($file, $maxWidth, $maxHeight,true)->src : false;
        if(!$cropImage) {
            $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, ($maxWidth * 2) , ($maxHeight * 2),true)->src : false;            
        }

    } else {
        $file = File::getByID($f->getFileID());
        //$fileSrc = is_object($file) ? $file->getVersion()->getRelativePath() : false;   
        $fileSrc = is_object($file) ? $ih->getThumbnail($file, 1440, 900, true)->src : false;        
    }
    ?>

    <img  src="<?= $fileSrc ?>" data-src="<?= $fileSrc ?>"  data-type="img" data-cover-slider="slide">

<?php
}