<?php 
    foreach($languages as $key=> $lang) { 
   
    if($activeLanguage == $key) { ?> 
    	<li class="is-active" data-role="list-item">
            <a class="no-barba" rel="alternate" hreflang="<?php echo strtolower(substr($lang, 0, 2)); ?>" href="<?= $view->action('switch_language', $cID, $key) ?>" title="<?=$lang?>" data-role="roll-hover-btn" data-custom-hover="true">
                	<?php  echo strtoupper(substr($lang, 0, 2)); ?>
            </a>
        </li>
        
    <?php } else { ?>
        
      <li data-role="list-item">
            <a class="no-barba" rel="alternate" hreflang="<?php echo strtolower(substr($lang, 0, 2)); ?>" href="<?= $view->action('switch_language', $cID, $key) ?>" title="<?=$lang?>" data-role="roll-hover-btn" data-custom-hover="true">
                	<?php  echo strtoupper(substr($lang, 0, 2)); ?>
            </a>
        </li>     
    <?php 
        }
    
}?>
