<?php
defined('C5_EXECUTE') or die('Access Denied.');
echo $controller->getOpenTag();

if ($controller->getTitle()) {
    echo '<span class="ccm-block-page-attribute-display-title">' . $controller->getTitle() . '</span>';
}
$page = Page::getCurrentPage();
$image = $page->getAttribute($controller->attributeHandle);
$ih = Loader::helper('image');
if (!$page->isEditMode()) {
    if (is_object($image)) {
        $file = File::getByID($image->getFileID());     
        $src = is_object($file) ? $ih->getThumbnail($file, 1440, 900, true)->src : false;
        $srcSet = is_object($file) ? $ih->getThumbnail($file, 2880, 1800, true)->src : false;
    } ?> 
    <!-- Attention: responsive image used. Require srcset -->
    <!-- image sizes: default - 1440x900, retina @2x - 2880x1800 -->
    <img src="<?=$src ?>" srcset="<?=$srcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
<?php } else  { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Image not viewable in editmode.'); ?></div>
<?php } ?>

<?php echo $controller->getCloseTag(); ?>

  