<?php
defined('C5_EXECUTE') or die('Access Denied.');
echo $controller->getOpenTag();
$c = Page::getCurrentPage();
$page=Page::getByID($c->getCollectionParentID());
$spaAttr = $page->getAttribute($controller->attributeHandle);
$buttonText = ($controller->getTitle()) ? $controller->getTitle() : '';

if(!empty($spaAttr)) { ?>
    <healcode-widget class="button button_dark button_full-width button_uppercase" data-custom-hover="true" <?=$spaAttr ?> data-inner-html="&lt;span&gt;<?=$buttonText ?>&lt;/span&gt;"></healcode-widget>
<?php } else { ?>
    <a class="button button_dark button_full-width button_uppercase" href="#" data-custom-hover="true"><span><?=$buttonText ?></span></a>
<?php } 

echo $controller->getCloseTag(); ?>