<?php
defined('C5_EXECUTE') or die('Access Denied.');
echo $controller->getOpenTag();
$c = Page::getCurrentPage();
$page=Page::getByID($c->getCollectionParentID());
$extUrl = $page->getAttribute($controller->attributeHandle);
$href = !empty($extUrl) ? $extUrl : '#';
$buttonText = ($controller->getTitle()) ? $controller->getTitle() : '';
?>

    <a class="button button_dark button_full-width button_uppercase" href="<?=$href ?>" data-custom-hover="true"><span><?=$buttonText ?></span></a>

<?php
echo $controller->getCloseTag(); ?>

