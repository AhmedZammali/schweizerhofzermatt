<?php
defined('C5_EXECUTE') or die('Access Denied.');
echo $controller->getOpenTag();

if ($controller->getTitle()) {
    echo '<span class="ccm-block-page-attribute-display-title">' . $controller->getTitle() . '</span>';
}
$page = Page::getCurrentPage();
$data = [];
$mImages = $page->getAttribute($controller->attributeHandle);
if(!empty($mImages)) {
    $ih = Loader::helper('image');  
    $multipleImage = $mImages->getFileObjects();  
    foreach ($multipleImage as $key => $file) {                                   
        $file = File::getByID($file->getFileID());
        $data[$key]['src'] = is_object($file) ? $ih->getThumbnail($file, 1440, 900, true)->src : false;
        $data[$key]['srcSet'] = is_object($file) ? $ih->getThumbnail($file, 2880, 1800, true)->src : false;
    }
} ?> 
<div class="swiper-wrapper">
    <?php foreach($data as $key => $image) { ?>
        <div class="swiper-slide">
            <div class="card card_full">
                <div class="card__image">
                    <div class="image-inner" data-role="slide-inner">
                        <!-- Attention: responsive image used. Require srcset -->
                        <!-- image sizes: default - 1440x900, retina @2x - 2880x1800 --><img src="<?=$image['src'] ?>" srcset="<?=$image['srcSet'] ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php    
    echo $controller->getCloseTag();
?>

  