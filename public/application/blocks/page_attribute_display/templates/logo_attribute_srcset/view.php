<?php
defined('C5_EXECUTE') or die('Access Denied.');
echo $controller->getOpenTag();

if ($controller->getTitle()) {
    echo '<span class="ccm-block-page-attribute-display-title">' . $controller->getTitle() . '</span>';
}
$page = Page::getCurrentPage();
$image = $page->getAttribute($controller->attributeHandle);
$ih = Loader::helper('image');
if (!$page->isEditMode()) {
    if (is_object($image)) {
        $file = File::getByID($image->getFileID());     
        $src = is_object($file) ? $ih->getThumbnail($file, 640, 880)->src : false;
        $srcSet = is_object($file) ? $ih->getThumbnail($file, 1280, 1760)->src : false;
        //$src = is_object($file) ? $file->getVersion()->getRelativePath() : false; 
    } ?> 
    <!-- Attention: responsive image used. Require srcset -->
    <!-- image sizes: default - 640x880, retina @2x - 1280x1760 -->
    <img data-src="<?=$src ?>" data-srcset="<?=$srcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt" data-lazy-loading="true">
<?php } else  { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Image not viewable in editmode.'); ?></div>
<?php } ?>

<?php echo $controller->getCloseTag(); ?>

  