<?php
defined('C5_EXECUTE') or die('Access Denied.');
echo $controller->getOpenTag();
$c = Page::getCurrentPage(); 
$href = !($c->getAttribute($controller->attributeHandle)) ? '#' : $c->getAttribute($controller->attributeHandle); 
//$target = !($c->getAttribute($controller->attributeHandle)) ? ' ' : ' target="_blank "';
$target = ($c->getAttribute($controller->attributeHandle) && $c->getAttribute('book_pop_up')) ? ' target="_blank "' : ' ';

?>

    <?php if (!$c->isEditMode()) { ?><a class="button button_dark button_full-width button_uppercase" href="<?=$href ?>"<?=$target ?>data-custom-hover="true"><?php } else { ?><div class="button button_dark button_full-width button_uppercase"><?php } ?>
        <span>
        <?php 
            $checkAvailText = new GlobalArea('Check Availability Button Text');
            $checkAvailText->display();
        ?>
        </span>
    <?php if (!$c->isEditMode()) { ?></a><?php } else { ?></div><?php } ?>
<?php    
    echo $controller->getCloseTag();
?>

  