<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<?php foreach ($selected as $service) { ?>
  <li><a href="<?php echo h($service->getServiceLink()) ?>" title="Our <?=$service->getDisplayName()?>" target="_blank" data-custom-hover="true">
  <i class="icon-<?php echo strtolower($service->getDisplayName()) ?>"></i></a></li>
<?php }?>
