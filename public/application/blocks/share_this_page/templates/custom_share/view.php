<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>
<?php foreach ($selected as $service) { ?>

<?php
	        	$name = ucfirst($service->getDisplayName());?>
    <li class="contacts__item contacts-item">
    	<div class="contacts-item__content">
	    	<a href="<?php echo h($service->getServiceLink()) ?>" data-custom-hover="true" title="<?php print_r($name); ?>">
	    		
	        	<?php print_r($name); ?>
	        </a>
    	</div>
    </li>
<?php } ?>