<?php defined('C5_EXECUTE') or die("Access Denied.");
$p = Page::getCurrentPage();
?>

<div class="slider-diff-indents">
  <div class="position-relative" data-role="slider-container">
    <div class="swiper-controls">
      <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
      <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
    </div>
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="cell  small-10 small-offset-1 medium-offset-1">
          <div class="swiper-container" data-slider="another-items">
            <div class="swiper-wrapper">
              <div class="swiper-pagination"></div>
              <?php foreach($rows as $key => $row) { 
                $file = File::getByID($row['fID']);
                $ih = Loader::helper('image');
                $fileSrc = is_object($file) ? $ih->getThumbnail($file, 770, 650, true)->src : false;
                $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 1540, 1300, true)->src : false; ?>

                <div class="swiper-slide">
                  <div class="card card_simple card_380" data-animation="fade-in-up" lazy>
                    <div class="card__image">
                      <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                        <!-- Attention: responsive image used. Require srcset -->
                        <!-- image sizes: default - 570x650, retina @2x - 1140x1300 -->
                        <!-- FINAL Image Size after width scaled by: default by 200 - 770x650 , retina @2x by 400 - 1540x1300 -->
                        <img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                      </div>
                      <div class="swiper-lazy-preloader"></div>
                    </div>
                    <div class="card__content">
                      <div class="card__top"></div>
                      <div class="card__label">
                        <h5><?=$row['title'] ?></h5>
                      </div>
                      <div class="card__description">
                        <div class="editable editable_text-sm">
                          <p>
                            <?=$row['description'] ?>
                          </p>
                        </div>
                      </div>
                      <!-- <div class="card__buttons">
                        <div class="buttons"><a class="button button_simple button_with-icon-left" href="single-offer.html" title="Amazing Ski" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><span>Discover</span></a></div>
                      </div> -->
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
