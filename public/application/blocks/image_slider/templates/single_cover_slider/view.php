<?php defined('C5_EXECUTE') or die("Access Denied.");
    $navigationTypeText = (0 == $navigationType) ? 'arrows' : 'pages';
    $c = Page::getCurrentPage();
?>
<?php if (count($rows) > 0) { ?>
    <div class="swiper-wrapper">
        <?php foreach ($rows as $row) { 
            $file = File::getByID($row['fID']);
            $ih = Loader::helper('image');
            $fileSrc = is_object($file) ? $ih->getThumbnail($file, 1440, 640,true)->src : false; 
            $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 2880, 1280,true)->src : false; ?>
            <div class="swiper-slide">
                <div class="card card_full">
                    <div class="card__image">
                        <div class="image-inner" data-role="slide-inner">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 1440x640, retina @2x - 2880x1280 --><img src="<?=$fileSrc ?>" srcset="<?=$fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermattss">
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
        <div class="ccm-image-slider-placeholder">
            <p><?php echo t('No Slides Entered.'); ?></p>
        </div>
    <?php } ?>