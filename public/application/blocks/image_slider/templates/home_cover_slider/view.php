<?php defined('C5_EXECUTE') or die("Access Denied.");
$p = Page::getCurrentPage();
?>
<div class="swiper-container" data-slider="simple-slider">
    <div class="swiper-wrapper">
    <?php foreach ($rows as $k => $row) {
        $file = File::getByID($row['fID']);
        $ih = Loader::helper('image');
        $fileSrc = is_object($file) ? $ih->getThumbnail($file, 1170, 800, true)->src : false;
        $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 2340, 1600, true)->src : false;       
        ?>              
        <div class="swiper-slide">
          <div class="card card_800">
            <div class="card__image">
              <div class="image-inner" data-role="slide-inner">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 1170x800, retina @2x - 2340x1600 --><img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false">
              </div>
            </div>
          </div>
        </div>
    <?php } ?>
    </div>
</div>
