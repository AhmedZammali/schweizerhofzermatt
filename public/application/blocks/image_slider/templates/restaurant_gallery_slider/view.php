<?php defined('C5_EXECUTE') or die("Access Denied.");
$p = Page::getCurrentPage();
?>
<div class="swiper-container" data-slider="gallery-mini_sub">
    <div class="swiper-wrapper">
    <?php foreach ($rows as $k => $row) {
        $file = File::getByID($row['fID']);
        $ih = Loader::helper('image');
        $fileSrc = is_object($file) ? $ih->getThumbnail($file, 450, 450,true)->src : false;
        $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 900, 900,true)->src : false;

        ?>              
        <div class="swiper-slide">
           <div class="card card_square">
                <div class="card__image">
                <div class="image-inner" data-role="slide-inner">
                 <!-- Attention: responsive image used. Require srcset -->
                 <!-- image sizes: default - 450x450, retina @2x - 900x900 -->
                <img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false">
              </div>
            </div>
          </div>
        </div>
    <?php } ?>
    </div>
</div>
