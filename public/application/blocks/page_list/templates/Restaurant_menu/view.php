<?php
defined('C5_EXECUTE') or die("Access Denied.");
$current_page = Page::getCurrentPage();
$page_name = $current_page->getCollectionName();
$current_url = $current_page->getCollectionLink();;
$c = Page::getCurrentPage();

if ($c->isEditMode() && $controller->isBlockEmpty())
{
?>
<div><?php echo t('No Restaurants List found') ?></div>
<?php
}
else
{ ?>
        <?php foreach ($pages as $key => $page) {  
                $name = $page->getCollectionName();
                $url = $page->getCollectionPointerExternalLink();
                        if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                                $target = '_blank';
                        }
                else {
                $url = $page->getCollectionLink();
                $target = $page->getAttribute('nav_target');
                }
                $video =$page->getAttribute("video_url"); 
                $thumbnail =$page->getAttribute("thumbnail");
                    if (is_object($thumbnail))
                    {
                        if ($image_thumbnail_thumb = Core::make('helper/image')->getThumbnail($thumbnail,1440, 800, true));
                        {
                            $thumb_url = $image_thumbnail_thumb->src;
                        }
                    }
        ?>  

<li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide">
        <a href="<?=$url?>" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link "><?=$name?></a>
</li>

<?php } } ?>