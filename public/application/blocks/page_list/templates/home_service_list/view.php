<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');  
$services = []; 
if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {  

    ?>
  <div class="layers__center">
    <div class="layers">
      <div class="position-relative" data-role="slider-container">
        <div class="swiper-controls">
          <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
          <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
        </div>
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell small-6 large-offset-3 small-offset-1 medium-offset-1">
              <div class="swiper-container" data-slider="home-services-titles">
                <div class="swiper-wrapper">

                    <?php 
                    foreach ($pages as $pl) {
                      if($pl->getAttribute('is_services') == '1') {
                        $order = $pl->getAttribute('service_order') ? $pl->getAttribute('service_order') : 0;
                        $services[$order] = $pl;
                      }
                      ksort($services);
                    }
                               
                    foreach ($services as $page) {
  
                        //if( $page->getAttribute('is_services') == '1') {
                        // Prepare data for each page being listed...
                        $title = $page->getCollectionName();
                        $shortDesc = $page->getAttribute('short_description');
                        
                        $isFeatured = $page->getAttribute('is_featured');
                        if ($page->getCollectionPointerExternalLink() != '') {
                        $url = $page->getCollectionPointerExternalLink();
                        if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                            $target = '_blank';
                        }
                        } else {
                            $url = $page->getCollectionLink();
                            $target = $page->getAttribute('nav_target');
                        }
                        $target = empty($target) ? '_self' : $target;
                        ?>
                          <div class="swiper-slide" data-role="services-title" aria-hidden="true" data-custom-hover="true" data-hover-text="View">
                            <div class="home-services__close"><i class="icon-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></i></div>
                            <div class="home-services__title" data-role="services-animated-title">
                              <div class="eyebrow"><span>
                                <?php
                                    $explore = new GlobalArea('Explore');
                                    $explore->display();
                                ?>                        
                                </span></div>
                              <div class="title" data-swiper-parallax="-35%">
                                <h2><?= $title ?></h2>
                              </div>
                            </div>
                            <div class="home-services__content">
                              <div class="editable editable_text-left" data-limit-rows="4">
                                <p>
                                  <?= $shortDesc ?>
                                </p>
                              </div>
                             
                               <div class="buttons">
                                <?php if(!$c->isEditMode()) { ?>
                                <a class="is-accent" href="<?= $url ?>" target="<?= $target ?>" title="<?=$title?>" data-role="dynamic-hover-arrow" data-custom-hover="true">
                                <?php } ?>
                                   <?php 
                                        $exploreMore = new GlobalArea("Explore More");
                                        $exploreMore->display();
                                    ?>
                                <?php if(!$c->isEditMode()) { ?>
                                 </a>
                                <?php } ?>
                            </div>
                            
                            </div>
                          </div>
                    <?php  }?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="position-relative" data-role="slider-container">
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell small-6 large-offset-3 small-offset-1 medium-offset-1">
              <div class="swiper-container" data-slider="home-services-titles_colored">
                <div class="swiper-wrapper">
                    <?php                       
                    foreach ($services as $page) {
                        //if( $page->getAttribute('is_services') == '1') {
                        // Prepare data for each page being listed...
                        $title = $page->getCollectionName();
                        $shortDesc = $page->getAttribute('short_description');

                    ?>
                  <div class="swiper-slide">
                    <div class="home-services__title" data-role="services-animated-title">
                      <div class="eyebrow"><span>
                        <?php
                            $explore = new GlobalArea('Explore');
                            $explore->display();
                        ?>
                      </span></div> 
                      <div class="title" data-swiper-parallax="-35%">
                        <h2><?= $title ?></h2>
                      </div>
                    </div>
                  </div>
                    <?php  } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell medium-8 medium-offset-4">
        <div class="home-services__images layers">
            <?php 
             
            foreach ($services as $page) {
                 if( $page->getAttribute('is_services') == '1') {
                // Prepare data for each page being listed...
                $title = $page->getCollectionName();
                $shortDesc = $page->getAttribute('short_description');
                
                $thumbnail = false;
                $fileSrc = false;
                $fileSrcSet = false;
                $thumbnail = $page->getAttribute('banner_image');
                if($thumbnail) {          
                    $file = File::getByID($thumbnail);        
                    $fileSrc = is_object($file) ? $ih->getThumbnail($file, 930, 870, true)->src : false;
                     $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 1860, 1740, true)->src : false;
 
                } 
                if ($page->getCollectionPointerExternalLink() != '') {
                $url = $page->getCollectionPointerExternalLink();
                if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                    $target = '_blank';
                }
                } else {
                    $url = $page->getCollectionLink();
                    $target = $page->getAttribute('nav_target');
                }
                $target = empty($target) ? '_self' : $target;
            ?>
              <div class="home-services__image" data-role="home-services-image">
                <div class="mask-in">
                  <div class="mask-out" data-custom-hover="true" data-hover-text="View">
                    <div class="overlay-close" data-role="close" data-custom-hover="true" data-hover-text="Close"></div>
                    <div class="card card_870">
                      <div class="card__image">
                        <!-- Attention: responsive image used. Require srcset -->
                        <!-- image sizes: default - 930x870, retina @2x - 1860x1740 --><img data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet?> 2x" draggable="false" data-lazy-loading="true">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } } ?>
        </div>
      </div>
    </div>
</div>
<?php  
} ?>
