<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');   

if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {  ?>
<div class="grid-x align-right">
  <?php foreach ($pages as $page) { 

    $name = $page->getCollectionName(); 
    if($page->getAttribute('page_title_delimiter')) {
      $splitName = $page->getAttribute('page_title_delimiter');  
    } else {
      $splitName = $name;  
    }
    $shortDescription = $page->getAttribute('page_small_desc');
    $mImages = $page->getAttribute('listing_slide');
    if(!empty($mImages)) {
      $imgArray = array();
      $ih = Loader::helper('image');  
      $multipleImage = $mImages->getFileObjects();  
      foreach ($multipleImage as $key => $file) {                                   
          $file = File::getByID($file->getFileID());
          $imgArray[$key]['src'] = is_object($file) ? $ih->getThumbnail($file, 1440, 640, true)->src : false;
          $imgArray[$key]['srcSet'] = is_object($file) ? $ih->getThumbnail($file, 2880, 1280, true)->src : false;
          //$imgArray[$key]['src'] = is_object($file) ? $file->getVersion()->getRelativePath() : false;
          //$imgArray[$key]['srcSet'] = is_object($file) ? $file->getVersion()->getRelativePath() : false;
          //$imageUrl = is_object($file) ? $ih->getThumbnail($file, 2880, 1280, true)->src : false;
          //array_push($imgArray, $imageUrl);
      }
    }
    $url = $nh->getLinkToCollection($page);
    $target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
    $target = empty($target) ? '_self' : $target;
  
  ?>
  <div class="cell small-10">
    <div data-role="multi-title__item" data-title="<?=$splitName ?>" data-description="<?=$shortDescription ?>" data-link="<?=$url ?>">
      <div class="simple-slider" data-role="simple-slider-container">
        <div class="position-relative" data-role="slider-container">
          <div class="swiper-controls">
            <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
            <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
          </div>
          <div class="grid-container">
            <div class="grid-x grid-margin-x">
              <div class="cell">
                <div class="swiper-container" data-slider="simple-slider"><a href="<?=$url ?>" title="<?=$name ?>" data-custom-hover="true"></a>
                  <div class="swiper-wrapper">
                    <?php if(!empty($imgArray)) {
                      foreach ($imgArray as $key => $src) { ?>
                      <div class="swiper-slide">
                        <div class="card card_640">
                          <div class="card__image">
                            <div class="image-inner" data-role="slide-inner">
                              <!-- Attention: responsive image used. Require srcset -->
                              <!-- image sizes: default - 1440x640, retina @2x - 2880x1280 --><img class="swiper-lazy" data-src="<?=$src['src'] ?>" data-srcset="<?=$src['srcSet'] ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php } 
                    } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php } ?>
