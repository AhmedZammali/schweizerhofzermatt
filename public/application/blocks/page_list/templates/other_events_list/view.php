<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');   

if ($c->isEditMode() && $controller->isBlockEmpty()) {
  ?>
  <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
  <?php
} else {  ?>
<div class="position-relative" data-role="slider-container">
  <div class="swiper-controls">
    <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
    <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
  </div>
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell  small-10 small-offset-1 medium-offset-1">
        <div class="swiper-container" data-slider="another-items">
          <div class="swiper-wrapper">
            <div class="swiper-pagination"></div>  
              <?php 
                $list->sortBy('ak_start_date', 'desc');
                $pages = $list->getPagination();    
              ?>    
                <?php $j = 1; foreach ($pages as $page) { 

                  if($curentPageID != $page->getCollectionId()) {
                      $j++;   
                      $thumbnail =$page->getAttribute("thumbnail");
                      if($j%2==0){                
                        $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 570, 650, true)->src : false;
                        $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1140, 1300, true)->src : false;
                      }else{                   
                          $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 570, 380, true)->src : false;
                          $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1140, 760, true)->src : false;
                      }     
                    $title = $page->getCollectionName(); 
                    $url = $nh->getLinkToCollection($page);
                    $target = empty($target) ? '_self' : $target;
                    $eventStartDate = $page->getAttribute('start_date');
                    $eventEndDate = $page->getAttribute('end_date');
                    if($eventStartDate) {
                      $startDate  = $eventStartDate->format('Y-m-d');
                      $eventStartDate = $eventStartDate->format('d F Y');
                    }
                    if($eventEndDate) {
                      $endDate  = $eventEndDate->format('Y-m-d');
                      $eventEndDate = $eventEndDate->format('d F Y');
                    }

                    $currentDate = date("d F Y");
                    ?>
                    <?php if(!$pastEvents && (strtotime($eventEndDate) >= strtotime($currentDate))) { ?>
                    <div class="swiper-slide">
                      <div class="card card_simple <?php echo ($j%2==0) ? 'card_650' : 'card_380' ?> " data-animation="fade-in-up" lazy>
                        <div class="card__image">
                          <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                            <!-- Attention: responsive image used. Require srcset -->
                            <!-- image sizes: default - 570x650, retina @2x - 1140x1300 --><img class="swiper-lazy" src="<?=$fileSrc?>" srcset="<?=$fileSrcSet?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                          </div>
                          <div class="swiper-lazy-preloader"></div>
                        </div>
                        <div class="card__content">
                          <div class="card__top"><div class="eyebrow eyebrow_close eyebrow_large"><time datetime="<?=$startDate?>"><?=$eventStartDate?></time> <?php if($eventEndDate){ ?>- <time datetime="<?=$endDate?>"><?=$eventEndDate?></time><?php } ?></div></div>
                          <div class="card__label">
                            <h5><?=$title ?></h5>
                          </div>
                          <div class="card__buttons">
                            <div class="buttons">
                              <?php if(!$c->isEditMode()) { ?><a class="button button_simple button_with-icon-left" href="<?= $url ?>" target="<?= $target ?>" title="<?=$name?>" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i>
                              <?php } ?>
                              <span>                                  
                              <?php 
                                  $discover = new GlobalArea('Discover');
                                  $discover->display();
                              ?>
                            </span>
                            <?php if(!$c->isEditMode()) { ?></a>
                            <?php } ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>                
<?php } } } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php } ?>
