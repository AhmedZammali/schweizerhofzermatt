<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');   

if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {  ?>

  <div class="grid-x position-relative indent-l">
    <div class="swiper-controls">
      <div class="swiper-button swiper-button-next nav nav_next visible" data-custom-hover="true" data-hover-type="nav next"></div>
      <div class="swiper-button swiper-button-prev nav nav_prev visible" data-custom-hover="true" data-hover-type="nav prev"></div>
    </div>
    <div class="cell small-8" data-role="mini-gallery-main">
      <div class="position-relative" data-role="slider-container">
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell">
              <div class="swiper-container" data-slider="gallery-mini_main">
                <div class="swiper-wrapper">
                <?php 

                foreach ($pages as $page) {
                    // Prepare data for each page being listed...
                    $title = $page->getCollectionName();
                    $shortDesc = $page->getAttribute('page_small_desc');
                    
                    $thumbnail = false;
                    $fileSrc = false;
                    $fileSrcSet = false;
                    $thumbnail = $page->getAttribute('single_room_image');
                    if($thumbnail) {          
                        $file = File::getByID($thumbnail);
                        $fileSrc = is_object($file) ? $ih->getThumbnail($file, 930, 680, true)->src : false;        
                        $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 1860, 1360, true)->src : false;
     
                    }                     
                ?>
                  <div class="swiper-slide">
                    <div class="card card_680">
                      <div class="card__image">
                        <div class="image-inner" data-role="slide-inner">
                          <!-- Attention: responsive image used. Require srcset -->
                          <!-- image sizes: default - 930x680, retina @2x - 1860x1360 --><img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false">
                        </div>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="cell small-4" data-role="mini-gallery-sub">
      <div class="position-relative" data-role="slider-container">
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell">
              <div class="swiper-container" data-slider="gallery-mini_sub">
                <div class="swiper-wrapper">
                    <?php 
                    foreach ($pages as $page) {
                        // Prepare data for each page being listed...
                        $title = $page->getCollectionName();
                        $shortDesc = $page->getAttribute('page_small_desc');
                        
                        $thumbnail = false;
                        $fileSrc = false;
                        $fileSrcSet = false;
                        $thumbnail = $page->getAttribute('single_room_image');
                        if($thumbnail) {          
                            $file = File::getByID($thumbnail);        
                            $fileSrc = is_object($file) ? $ih->getThumbnail($file, 450, 450, true)->src : false;
                            $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 900, 900, true)->src : false;
                        }                     
                        ?>
                      <div class="swiper-slide">
                        <div class="card card_square">
                          <div class="card__image">
                            <div class="image-inner" data-role="slide-inner">
                              <!-- Attention: responsive image used. Require srcset -->
                              <!-- image sizes: default - 450x450, retina @2x - 900x900 --><img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false">
                            </div>
                          </div>
                        </div>
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="grid-x grid-margin-x">
    <!--.cell.large-4.large-offset-2(data-animation="fade-in-up")-->
    <div class="cell large-4 large-offset-8">
      <div class="mini-gallery__titles layers">
            <?php 
              foreach ($pages as $page) {
                  $title = $page->getCollectionName();
                  $shortDesc = $page->getAttribute('page_small_desc');
                   if ($page->getCollectionPointerExternalLink() != '') {
                  $url = $page->getCollectionPointerExternalLink();
                  if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                      $target = '_blank';
                  }
                  } else {
                      $url = $page->getCollectionLink();
                      $target = $page->getAttribute('nav_target');
                  }
                  $target = empty($target) ? '_self' : $target;
              ?>    
              <div class="mini-gallery__title" data-role="slide-title">
                <div class="title">
                  <h4><?= $title ?></h4>
                </div>
                <div class="editable">
                  <p>
                    <!--  capitalize the first letter of the first word;-->
                    <?= $shortDesc ?>
                  </p>
                </div>
                <div class="buttons">
                  <?php if(!$c->isEditMode()) { ?>
                  <a href="<?= $url ?>" target="<?= $target ?>" data-role="dynamic-hover-arrow" data-custom-hover="true">
                  <?php } ?>
                    <?php 
                      $exploreMore = new GlobalArea("Explore More");
                      $exploreMore->display();
                    ?>
                  <?php if(!$c->isEditMode()) { ?>
                  </a>
                  <?php } ?>
                  </div>
              </div>
            <?php } ?>
      </div>
    </div>
    <div class="cell large-8 large-offset-2">
      <div class="mini-gallery__info" data-animation="fade-in-up">
        <div class="slide-info">
          <div class="swiper-pagination" data-role="slider-pagination"></div>
          <div class="swiper-progress" data-role="progress"><span class="line"></span></div>
        </div>
      </div>
    </div>
  </div>
<?php 
} ?>
