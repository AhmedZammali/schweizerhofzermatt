<?php
defined('C5_EXECUTE') or die("Access Denied.");
$current_page = Page::getCurrentPage();
$ih = Loader::helper('image'); 
$lang = Localization::activeLanguage();
$page_name = $current_page->getCollectionName();
$current_url = $current_page->getCollectionLink();;
$c = Page::getCurrentPage();

if ($c->isEditMode() && $controller->isBlockEmpty()) { ?>
 <div><?php echo t('No Experience List found') ?></div>
<?php } else { 
  $j = 0;
  if($lang=='en'){
    $langs ='';
  }
  else {
    $langs ="_".$lang;
  }
?>
<div class="cell">
  <ul class="cards-list cards-list_2-items-isotope" data-role="filtered-content">
  <?php foreach ($pages as $key => $page) { 
    $j++; 
    $name = $page->getCollectionName();
    $thumbnail =$page->getAttribute("experience_group_image");
    if($j == 2) {                
      $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 690, 850, true)->src : false;
      $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1380, 1700, true)->src : false;
    } else if($j == 3) {                   
      $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 690, 650, true)->src : false;
      $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1380, 1300, true)->src : false;
    } else if($j == 4) {
      $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 690, 460, true)->src : false;
      $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1380, 920, true)->src : false;
    } else {
      $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 690, 380, true)->src : false;
      $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1380, 760, true)->src : false;
    }          
    $url = $page->getCollectionPointerExternalLink();
    if ($page->openCollectionPointerExternalLinkInNewWindow()) {
      $target = '_blank';
    } else {
      $url = $page->getCollectionLink();
      $target = $page->getAttribute('nav_target');
    }
    $description = $page->getCollectionDescription();

    $expType = $page->getAttribute("experience_group_filters".$langs, 'display'); 
    $typestr= explode(",",$expType);
    $strTypes= '';
    for($i=0; $i<count($typestr); $i++) {
        $strTypes .= " ".$typestr[$i]; 
    }
    //$thumbnail =$page->getAttribute("thumbnail");
  ?> 
    <li class="cards-list__item" data-role="filtered-item" data-filter-tags="<?= strtolower($strTypes);?>">
      <div class="card card_simple card_free-height" data-animation="fade-in-up" lazy>
        <div class="card__image">
          <!-- Attention: responsive image used. Require srcset -->
          <!-- image sizes: default - 690x380, retina @2x - 1380x760 -->
          <!-- image sizes: 2 - 690x850, retina @2x - 1380x1700 -->
          <!-- image sizes: 3 - 690x650, retina @2x - 1380x1300 -->
          <!-- image sizes: 4 - 690x460, retina @2x - 1380x920 -->
          <img src="<?=$fileSrc?>" srcset="<?=$fileSrcSet?> 2x" draggable="false" alt="Schweizerhof Zermatt">
        </div>
        <div class="card__content">
          <div class="card__top"></div>
          <div class="card__label">
            <h4><?=$name?></h4>
          </div>
          <div class="card__description">
            <div class="editable editable_text-sm">
              <p>
              <?=$description?>
              </p>
            </div>
          </div>
          <div class="card__buttons">
            <div class="buttons">
              <?php if(!$c->isEditMode()) { ?><a class="button button_simple button_with-icon-left" href="<?= $url ?>" title="<?=$name?>" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i><?php } ?>
                <span>
                <?php 
                    $discover = new GlobalArea('Discover');
                    $discover->display();
                ?>
                </span>
              <?php if(!$c->isEditMode()) { ?></a><?php } ?>
            </div>
          </div>
        </div>
      </div>
    </li>
  <?php } ?>  
    <li class="cards-list__item cards-list__item_not-found" data-role="filtered-item" data-filter-tags="not-found">
      <div class="card card_not-found">
        <div class="card__content">
          <div class="title">
            <h3>
              <?php 
                  $contentNotFoundHeading = new GlobalArea('Content not found');
                  $contentNotFoundHeading->display();
              ?>
            </h3>
          </div>
          <div class="editable">
            <p><?=$noResultsMessage?></p>
          </div>
        </div>
      </div>
    </li>      
  </ul>
</div>
<?php } ?>