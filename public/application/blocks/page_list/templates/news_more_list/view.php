<?php
defined('C5_EXECUTE') or die("Access Denied.");
$current_page = Page::getCurrentPage();
$ih = Loader::helper('image'); 
$lang = Localization::activeLanguage();
$page_name = $current_page->getCollectionName();
$current_url = $current_page->getCollectionLink();;
$c = Page::getCurrentPage();

$list->filterByParentID($current_page->getCollectionParentID());
$pages = $list->getResults();

if ($c->isEditMode() && $controller->isBlockEmpty())
{
?>
<div><?php echo t('No Offers List found') ?></div>
<?php
}
else
{ $j=0;
if($lang=='en'){
      $langs ='';
    }
    else {
      $langs ="_".$lang;
    }
     ?>
  <?php 
  $list->sortBy('ak_news_date', 'desc');
$pages = $list->getPagination();
if(count($pages) > 1) {
  foreach ($pages as $key => $page) { $j++; 
                $name = $page->getCollectionName();
                $pageID = $c->getCollectionID(); //Getting PageID
                $childID = $page->getCollectionID(); 
                $thumbnail =$page->getAttribute("thumbnail");
                if($j%3==2){                
                  $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 570, 380, true)->src : false;
                  $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1140, 760, true)->src : false;
                }else{                   
                    $fileSrc = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 570, 380, true)->src : false;
                    $fileSrcSet = is_object($thumbnail) ? $ih->getThumbnail($thumbnail, 1140, 760, true)->src : false;
                }  

                if ($childID != $pageID){

                if ($page->getCollectionPointerExternalLink() != '') {
                    $url = $page->getCollectionPointerExternalLink();
                    if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                        $target = '_blank';
                    }
                } else {
                    $url = $page->getCollectionLink();
                    $target = $page->getAttribute('nav_target');
                }

                $description =$page->getAttribute("short_description"); 
                $news_date = $page->getAttribute('news_date');
                $ndate  = $news_date->format('d F Y');
                $ntime = $news_date->format('h:i A');
                $offerType =$page->getAttribute("offer_type".$langs); 
                $typestr= explode("\n",$offerType);
                $strTypes='';
                for($i=0;$i<count($typestr);$i++){
                    $strTypes .= " ".$typestr[$i]; 
                }
                $thumbnail =$page->getAttribute("thumbnail");
        ?> 
                        <div class="swiper-slide">
                                <div class="card card_simple card_380" data-animation="fade-in-up" lazy>
                                  <div class="card__image">
                                    <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                                      <!-- Attention: responsive image used. Require srcset -->
                                      <!-- image sizes: default - 570x380, retina @2x - 1140x760 --><img class="swiper-lazy" data-src="<?=$fileSrc?>" data-srcset="<?=$fileSrcSet?> 2x" draggable="false" alt="8 Ways Media SA">
                                    </div>
                                    <div class="swiper-lazy-preloader"></div>
                                  </div>
                                  <div class="card__content">
                                    <div class="card__top"><div class="eyebrow eyebrow_close eyebrow_large"><time datetime="<?=$ndate?>"><?=$ndate?></time></div></div>
                                    <div class="card__label">
                                      <h5><?=$name?></h5>
                                    </div>
                                    <div class="card__buttons">
                                <div class="buttons">
                                  <?php if(!$c->isEditMode()) { ?><a class="button button_simple button_with-icon-left" href="<?= $url ?>" target="<?= $target ?>" title="<?=$name?>" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i>
                                  <?php } ?>
                                  <span>                                  
                                  <?php 
                                      $discover = new GlobalArea('Discover');
                                      $discover->display();
                                  ?>
                                </span>
                                <?php if(!$c->isEditMode()) { ?></a>
                                <?php } ?>
                                </div>
                              </div>
                                  </div>
                                </div>
                              </div>
                        <?php } ?>        
<?php } }
else { ?>
  <div class="ccm-block-page-list-no-pages"><?php echo h($noResultsMessage) ?></div>
  <?php } } ?>