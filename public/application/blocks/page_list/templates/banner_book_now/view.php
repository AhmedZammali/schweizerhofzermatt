<?php
defined('C5_EXECUTE') or die("Access Denied.");
$current_page = Page::getCurrentPage();
$page_name = $current_page->getCollectionName();
$current_url = $current_page->getCollectionLink();;
$c = Page::getCurrentPage();

if ($c->isEditMode() && $controller->isBlockEmpty()) { ?>
<div><?php echo t('No Restaurants List found') ?></div>
<?php } else { ?>
<div class="application application_cta">
  <div class="cta" data-role="cta-block">
    <div class="cta__block cta__block_light"><span class="cta__main-text">
          <?php    
            $bookNow= new GlobalArea("Book Now Footer Text");
            $bookNow->display();
          ?>
    </span>
      <ul>
      <?php foreach ($pages as $key => $page) {  
      if($page->getAttribute('is_featured')) {
        $name = $page->getAttribute('book_now_name');
        $bookPopUp = $page->getAttribute('book_pop_up');        
        $images= $page->getAttribute("image_attribute");  
        $externalUrl= $page->getAttribute("external_url");  
        $widgetAttr = $page->getAttribute("spa_widget_attributes");  
        
        $attrData= explode("~",$images); 
         $attrData = array_filter($attrData);

         if(count($attrData) > 0) {
            foreach ($attrData as $key => $textIcon) {
              if(explode("!#",$textIcon)[0] != undefined) {
                  
                  $textValue = explode("!#",$textIcon)[0];
                  $numText = explode("-#",$textValue);
                  
                  if(count($numText) > 0) {
                    $textValue = $numText[0];
                  }                                        
                  $textTitle = explode("|",explode("!#",$textIcon)[1])[0];
                  $icon = explode("|",explode("!#",$textIcon)[1])[1];                                       
                  ?>
                  <li>
                    <?php if($widgetAttr) { ?>
                      <healcode-widget class="cta__link cta-link" data-custom-hover="true" <?=$widgetAttr ?> data-inner-html="<span class='cta-link__icon'><i class='icon-spa'></i></span><span class='cta-link__description'><?=$textValue ?></span>"></healcode-widget>
                    <?php } else { ?>
                      <a class="cta__link cta-link" href="<?php if(!$bookPopUp) echo $externalUrl; else echo '#';?>" title="<?=$textTitle?>" data-custom-hover="true" <?=$bookPopUp ? 'data-role="cta-modal-open"' : ''?> <?= ($externalUrl) ? 'target="_blank"': '' ?>><span class="cta-link__icon"><i class="icon-<?=$icon?>"></i></span><span class="cta-link__description"><?=$textValue ?></span></a>
                    <?php } ?>
                  </li>
                  <?php
              }                                   
            }
         }
       }
      } ?>
      </ul>
    </div>
  </div>
  <div class="cta-modal" data-role="cta-modal">
    <div class="cta-modal__top">
      <div class="cta-modal__title">
        <h4>
          <?php    
            $bookNowHeader= new GlobalArea("Book Now Sidebar Header");
            $bookNowHeader->display();
          ?>
        </h4>
      </div>
      <div class="cta-modal__button">
        <button data-role="cta-modal-close" title="Close" aria-label="Close modal" data-custom-hover="true"><i class="icon-close"></i></button>
      </div>
    </div>
    <div class="cta-modal__content">
      <ul>
        <?php foreach ($pages as $key => $page) {  
      if($page->getAttribute('book_pop_up') && $page->getAttribute('external_url')) {
        $name = $page->getCollectionName();
        $url = $page->getAttribute('external_url')
        ?>
        <li><a href="<?=$url?>" title="<?=strtolower($name)?>" <?php if($url != '#') echo 'target="_blank"'; ?> data-custom-hover="true"><span><?=$name?></span></a></li>
      <?php
      } 
      }?>        
      </ul>
    </div>
  </div>
</div>
<?php } ?>