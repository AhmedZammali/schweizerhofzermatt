<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');   

if ($c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
<?php } else {  ?>
        <ul class="tab-links" data-role="animated-list-line">
                <?php foreach ($pages as $page) { 
                        $title = $page->getCollectionName(); 
                        $url = $page->getCollectionPointerExternalLink();
                        if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                                $target = '_blank';
                        } else {
                                $url = $page->getCollectionLink();
                                $target = $page->getAttribute('nav_target');
                        } ?>

                        <li class="tab-links__item" data-role="list-item" data-animation="fade-in-slide">
                                <a href="<?=$url ?>" data-custom-hover="true" title="title" data-role="roll-hover-btn tab-list-link "><?=$title ?></a>
                        </li>
                <?php } ?>
                <span class="tab-links__line" data-role="forward-line">
                        <div class="styling-dots">
                                <div class="styling-dots__dot"></div>
                                <div class="styling-dots__dot"></div>
                                <div class="styling-dots__dot"></div>
                        </div>
                </span>
        </ul>

<?php } ?>