<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');   

if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {  ?>
<div class="slider-diff-indents">
  <div class="position-relative" data-role="slider-container">
    <div class="swiper-controls">
      <div class="swiper-button swiper-button-next nav nav_next visible" data-custom-hover="true" data-hover-type="nav next"></div>
      <div class="swiper-button swiper-button-prev nav nav_prev visible" data-custom-hover="true" data-hover-type="nav prev"></div>
    </div>
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="cell  large-12 large-offset-0 small-10 small-offset-1 medium-offset-1">
          <div class="swiper-container" data-slider="another-items">
            <div class="swiper-wrapper">
              <div class="swiper-pagination"></div>
              <?php foreach ($pages as $page) { 
                if($curentPageID != $page->getCollectionId()) {
                $title = $page->getCollectionName(); 
                $thumbnail = $page->getAttribute('single_room_image');
                if($thumbnail) {          
                  $file = File::getByID($thumbnail);        
                  $src = is_object($file) ? $ih->getThumbnail($file, 690, 380, true)->src : false;
                  $srcSet = is_object($file) ? $ih->getThumbnail($file, 1380, 760, true)->src : false;
                }
                $url = $nh->getLinkToCollection($page);
                $target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
                $target = empty($target) ? '_self' : $target;
                $description = $page->getCollectionDescription();
                $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
                $description = $th->entities($description);
                ?>
                <div class="swiper-slide">
                  <div class="card card_simple card_380" data-animation="fade-in-up" lazy>
                    <div class="card__image">
                      <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                        <!-- Attention: responsive image used. Require srcset -->
                        <!-- image sizes: default - 690x380, retina @2x - 1380x760 --><img class="swiper-lazy" data-src="<?=$src ?>" data-srcset="<?=$srcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                      </div>
                      <div class="swiper-lazy-preloader"></div>
                    </div>
                    <div class="card__content">
                      <div class="card__top"></div>
                      <div class="card__label">
                        <h4><?=$title ?></h4>
                      </div>
                      <div class="card__description">
                        <div class="editable editable_text-sm">
                          <p>
                            <?=$description ?>
                          </p>
                        </div>
                      </div>
                      <div class="card__buttons">
                        <div class="buttons">
                          <?php if (!$c->isEditMode()) { ?>
                          <a class="button button_simple button_with-icon-left" href="<?=$url ?>" title="<?=$title ?>" data-split-text="button-letters" data-custom-hover="true"><i class="icon-arrow-small_right"></i>
                          <?php } ?>
                        <span>
                          <?php 
                            $discoverText = new GlobalArea('Discover');
                            $discoverText->display();
                          ?>
                        </span>
                         <?php if (!$c->isEditMode()) { ?>
                        </a>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
