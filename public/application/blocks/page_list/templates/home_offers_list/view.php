<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$curentPageID = $c->getCollectionId();
/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$ih = Loader::helper('image');   

if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {  ?>
<div class="slider-diff-indents">
  <div class="position-relative" data-role="slider-container">
    <div class="swiper-controls">
      <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
      <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
    </div>
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class="cell  small-10 small-offset-1 medium-offset-1">
          <div class="swiper-container" data-slider="another-items">
            <div class="swiper-wrapper">
              <div class="swiper-pagination"></div>
              <?php 
                $i = 0;
                foreach ($pages as $page) {
                    // Prepare data for each page being listed...
                    $title = $page->getCollectionName();
                    $shortDesc = $page->getAttribute('short_description');
                    
                    $thumbnail = false;
                    $fileSrc = false;
                    $fileSrcSet = false;
                    $i++;
                    $thumbnail = $page->getAttribute('thumbnail');
                    if($thumbnail) {          
                        $file = File::getByID($thumbnail);    
                        if($i % 2 == 0) {
                          $fileSrc = is_object($file) ? $ih->getThumbnail($file, 570, 380, true)->src : false;
                          $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 1140, 760, true)->src : false;
                        } else {
                          $fileSrc = is_object($file) ? $ih->getThumbnail($file, 570, 650 , true)->src : false;
                          $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 1140, 1300 , true)->src : false;
                        } 
                    } 
                    if ($page->getCollectionPointerExternalLink() != '') {
                    $url = $page->getCollectionPointerExternalLink();
                    if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                        $target = '_blank';
                    }
                    } else {
                        $url = $page->getCollectionLink();
                        $target = $page->getAttribute('nav_target');
                    }
                    $target = empty($target) ? '_self' : $target;
                    ?>
                    <div class="swiper-slide">
                        <div class="card card_simple <?= ($i % 2 == 0) ? 'card_380' : 'card_650'?>" data-animation="fade-in-up" lazy>
                          <div class="card__image">
                            <div class="image-inner" data-swiper-parallax="-100" data-swiper-parallax-duration="1000">
                              <!-- Attention: responsive image used. Require srcset -->
                              <!-- image sizes: default - 570x650, retina @2x - 1140x1300 --><img class="swiper-lazy" data-src="<?=$fileSrc?>" data-srcset="<?=$fileSrcSet?> 2x" draggable="false">
                            </div>
                            <div class="swiper-lazy-preloader"></div>
                          </div>
                          <div class="card__content">
                            <div class="card__top"></div>
                            <div class="card__label">
                              <h5><?=$title?></h5>
                            </div>
                            <div class="card__description">
                              <div class="editable editable_text-sm">
                                <p>
                                  <!--  capitalize the first letter of the first word;-->
                                  <?=$shortDesc?>
                                </p>
                              </div>
                            </div>
<!--                             <div class="card__buttons">
                              <div class="buttons">
                                <?php if(!$c->isEditMode()) { ?>
                                <a class="button button_simple button_with-icon-left" href="<?= $url ?>" target="<?= $target ?>" title="Week-End Suite Deals" data-split-text="button-letters" data-custom-hover="true">
                                <?php } ?>
                                  <i class="icon-arrow-small_right"></i><span>
                                <?php 
                                    $discover = new GlobalArea("Discover");
                                    $discover->display();
                                ?>
                                </span>
                                <?php if(!$c->isEditMode()) { ?>
                                </a>
                                <?php } ?>
                                </div>
                            </div> -->
                          </div>
                        </div>
                    </div>
                   <?php 
                   } 
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php  
} ?>
