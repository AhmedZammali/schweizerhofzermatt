<?php
defined('C5_EXECUTE') or die("Access Denied.");
$current_page = Page::getCurrentPage();
$page_name = $current_page->getCollectionName();
$current_url = $current_page->getCollectionLink();;
$c = Page::getCurrentPage();

if ($c->isEditMode() && $controller->isBlockEmpty())
{
?>
<div><?php echo t('No Restaurants List found') ?></div>
<?php
}
else
{ ?>

  <?php foreach ($pages as $key => $page) {  
                $name = $page->getCollectionName();
                
                if($page->getAttribute('page_title_delimiter')) {
                  $splitName = $page->getAttribute('page_title_delimiter');  
                } else {
                  $splitName = $name;  
                }
                $shortdesc = $page->getAttribute("restaurant_short_desc");
                $url = $page->getCollectionPointerExternalLink();
                        if ($page->openCollectionPointerExternalLinkInNewWindow()) {
                                $target = '_blank';
                        }
                else {
                $url = $page->getCollectionLink();
                $target = $page->getAttribute('nav_target');
                }
                $video =$page->getAttribute("video_url"); 
                $thumbnail =$page->getAttribute("thumbnail");
                    if (is_object($thumbnail))
                    {
                        if ($image_thumbnail_thumb = Core::make('helper/image')->getThumbnail($thumbnail,1440, 800, true));
                        {
                            $thumb_url = $image_thumbnail_thumb->src;
                        }
                    }
        ?> 

<div class="cell small-10">
                    <div data-role="multi-title__item" data-title="<?=$splitName?>" data-description="<?=$shortdesc?>" data-link="<?=$url?>">
                      <div class="simple-slider" data-role="simple-slider-container">
                        <div class="position-relative" data-role="slider-container">
                          <div class="swiper-controls">
                            <div class="swiper-button swiper-button-next nav nav_next" data-custom-hover="true" data-hover-type="nav next"></div>
                            <div class="swiper-button swiper-button-prev nav nav_prev" data-custom-hover="true" data-hover-type="nav prev"></div>
                          </div>
                          <div class="grid-container">
                            <div class="grid-x grid-margin-x">
                              <div class="cell">
                                <div class="swiper-container" data-slider="simple-slider"><a href="<?=$url?>" title="<?=$name?> Restaurant" data-custom-hover="true"></a>
                                  <div class="swiper-wrapper">

                                    <?php 
                $images = $page->getAttribute('listing_slide');
                
                if($images)
                    $sliderImage = $images->getFileObjects();
                  if(!empty($images)){
                      foreach ($sliderImage as $file) {                     
                        $ih = Loader::helper('image');                     
                        $file = File::getByID($file->getFileID());
                        $fileSrc = is_object($file) ? $ih->getThumbnail($file, 1440, 640, true)->src : false;
                        $fileSrcSet = is_object($file) ? $ih->getThumbnail($file, 2880, 1280, true)->src : false;
                        ?>
                        <div class="swiper-slide">
                                      <div class="card card_640">
                                        <div class="card__image">
                                          <div class="image-inner" data-role="slide-inner">
                                            <!-- Attention: responsive image used. Require srcset -->
                                            <!-- image sizes: default - 1440x640, retina @2x - 2880x1280 -->
                                            <img class="swiper-lazy" data-src="<?= $fileSrc ?>" data-srcset="<?= $fileSrcSet ?> 2x" draggable="false" alt="Schweizerhof Zermatt">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                      <?php
                      } 
                  } else { ?>                   
                          <p>No Images Found</p>  
                 <?php } ?>



                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
<?php } }?>                  