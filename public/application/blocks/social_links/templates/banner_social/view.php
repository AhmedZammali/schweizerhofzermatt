 <?php   foreach ($links as $link) { 
	$service = $link->getServiceObject();
	$name = str_replace(' ', '-', $service->getName());
 ?>
<li class="contacts__item contacts-item">
  <div class="contacts-item__content"><a href="<?php echo h($link->getURL());?>" data-custom-hover="true" title="<?= $name ?>"><?= $name ?></a></div>
</li>
<?php } ?>