<ul class="social"> 
 <?php   foreach ($links as $link) { 
	$service = $link->getServiceObject();
	$name = str_replace(' ', '-', $service->getName());
 ?>
<li>
 <a href="<?php echo h($link->getURL());?>" target="_blank" title="<?=$name?>" class="no-barba" data-custom-hover="true" title="<?= $name ?>"><i class="icon-<?=strtolower($name)?>"></i></a>
</li>
<?php } ?>
</ul>