<?php 
/* Cachekey: cache/stash_default/zend/zend_feed_reader_32cf6f1b9ec7fdf83da2b96ed1f32a3c/ */
/* Type: array */
/* Expiration: 2019-01-20T05:21:17+05:30 */



$loaded = true;
$expiration = 1547941877;

$data = array();

/* Child Type: string */
$data['return'] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<rss version=\"2.0\" xmlns:slash=\"http://purl.org/rss/1.0/modules/slash/\">
  <channel>
    <title>concrete5.org Blog</title>
    <description>concrete5.org Blog</description>
    <generator>Zend_Feed_Writer 2 (http://framework.zend.com)</generator>
    <link>https://www.concrete5.org/about/blog</link>
    <item>
      <title>concrete5 8.4.4 Now Available!</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Thu, 10 Jan 2019 04:53:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/concrete5-844-now-available</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/concrete5-844-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Town Hall Meeting Jan 8th</title>
      <description><![CDATA[Chat with the core team this tuesday]]></description>
      <pubDate>Tue, 08 Jan 2019 00:05:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/town-hall-meeting-jan-8th</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/town-hall-meeting-jan-8th</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>First Townhall Meeting of 2019</title>
      <description><![CDATA[Join us for the first townhall meeting of 2019]]></description>
      <pubDate>Mon, 31 Dec 2018 19:26:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/first-townhall-meeting-2019</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/first-townhall-meeting-2019</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Townhall meeting this Tuesday, December 4th</title>
      <description><![CDATA[Meet with the core team on Tuesday]]></description>
      <pubDate>Sat, 01 Dec 2018 00:23:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/townhall-meeting-tuesday</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/townhall-meeting-tuesday</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Town Hall Summary</title>
      <description><![CDATA[Recap of what we discussed on 11/6]]></description>
      <pubDate>Sat, 10 Nov 2018 19:28:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/town-hall-summary</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/town-hall-summary</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Trick or treat my sweets! Totally Random is back next Tuesday...</title>
      <description><![CDATA[Monthly video Town Hall hangout]]></description>
      <pubDate>Wed, 31 Oct 2018 21:33:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/trick-or-treat-my-sweets-totally-random-back-next-tuesday</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/trick-or-treat-my-sweets-totally-random-back-next-tuesday</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Thank you CascadiaPHP!</title>
      <description><![CDATA[Wow that was fun!]]></description>
      <pubDate>Wed, 26 Sep 2018 01:11:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/thank-you-cascadiaphp</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/thank-you-cascadiaphp</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.3 Now Available!</title>
      <description><![CDATA[Version 8.4.3 of concrete5 is now available. This is a minor maintenance release that fixes some bugs found in 8.4.2.]]></description>
      <pubDate>Mon, 24 Sep 2018 21:56:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/concrete5-843-now-available</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/concrete5-843-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.3 is Now Available!</title>
      <description><![CDATA[8.4.3 brings some bug fixes, improves localization and UI experience, and improvements to Express.]]></description>
      <pubDate>Mon, 24 Sep 2018 21:07:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-843-now-available</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-843-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Latest concrete5 / CascadiaPHP details.</title>
      <description><![CDATA[Plans for the conference in September 2018]]></description>
      <pubDate>Sun, 26 Aug 2018 16:09:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/latest-concrete5-cascadiaphp-details</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/latest-concrete5-cascadiaphp-details</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Official End of Life on concrete5 version 6.x</title>
      <description><![CDATA[It's been way longer than we promised, it's time to move on. ]]></description>
      <pubDate>Fri, 24 Aug 2018 17:45:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/official-end-life-concrete5-version-6x</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/official-end-life-concrete5-version-6x</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.2 is Now Available!</title>
      <description><![CDATA[8.4.2 brings some bug fixes, fixes broken marketplace integration, adds some new developer features and workflow performance and experience improvements.]]></description>
      <pubDate>Wed, 08 Aug 2018 21:27:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-842-now-available</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-842-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Serious concrete5 leaders are planning for CascadiaPHP, are you?</title>
      <description><![CDATA[Come meet leading community members at CascadiaPHP]]></description>
      <pubDate>Sun, 29 Jul 2018 20:50:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/serious-concrete5-leaders-are-planning-cascadiaphp-are-you</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/serious-concrete5-leaders-are-planning-cascadiaphp-are-you</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5-Con!!!</title>
      <description><![CDATA[Come meet us, finally.]]></description>
      <pubDate>Mon, 16 Jul 2018 20:19:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/concrete5-con</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/concrete5-con</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.1 is Now Available!</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Fri, 13 Jul 2018 17:47:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-841-now-available</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-841-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>File Manager User Testing</title>
      <description><![CDATA[Some input on the file manager UX please?]]></description>
      <pubDate>Fri, 22 Jun 2018 03:04:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/file-manager-user-testing</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/file-manager-user-testing</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>New Director of UX - Gary Cribb</title>
      <description><![CDATA[We hired a new lead design resource.]]></description>
      <pubDate>Thu, 14 Jun 2018 02:15:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/new-director-ux-gary-cribb</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/new-director-ux-gary-cribb</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.0 is Now Available!</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Thu, 07 Jun 2018 23:48:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-840-now-available</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-840-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>BIG ANNOUNCEMENT</title>
      <description><![CDATA[Biggest announcement we've ever had.]]></description>
      <pubDate>Thu, 07 Jun 2018 02:18:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/big-announcement</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/big-announcement</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 and GDPR</title>
      <description><![CDATA[What we're doing about GDPR here.]]></description>
      <pubDate>Fri, 25 May 2018 01:31:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/concrete5-and-gdpr</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/concrete5-and-gdpr</guid>
      <slash:comments>0</slash:comments>
    </item>
  </channel>
</rss>
";

/* Child Type: integer */
$data['createdOn'] = 1547542699;
