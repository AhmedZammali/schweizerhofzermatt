<?php 
        $ref= $_REQUEST['ref'];
        $db =  \Database::connection();
        $query = $db->query("SELECT * FROM  SliderImageExpressSearchIndexAttributes WHERE exEntryID IN (SELECT id FROM ExpressEntityAssociationSelectedEntries WHERE  exSelectedEntryID='$ref'); ");
        $dataSector= $query->fetchAll();
        for($i=0;$i<sizeof($dataSector);$i++){
            $fileID = $dataSector[$i]['ak_slider_image'];
            $imageHelper = Core::make('helper/image');
            $fObj = File::getByID($fileID);
              $fileSrc = $imageHelper->getThumbnail($fObj,1170,650,true)->src;
              $fileSrcSet = $imageHelper->getThumbnail($fObj,2340,300,true)->src;            
        ?>
            <div class="swiper-slide" data-title="<?=$dataSector[$i]['ak_image_title'];?>">
              <div class="card card_full card_650">
                <!-- Attention: responsive image used. Require srcset -->
                <!-- image sizes: default - 1170x650, retina @2x - 2340x300 -->
                <div class="card__image">
                  <div class="image-inner" data-role="slide-inner">
                    <img data-src="<?=$fileSrc?>" draggable="false" alt="Schweizerhof Zermatt" class="swiper-lazy">
                  </div>
                  <div class="swiper-lazy-preloader"></div>
                </div>
              </div>
            </div>
      <?php }?>
