<?php 
namespace Concrete\Package\ImageWithAttributes\Attribute\ImageWithAttributes;

use Loader;
use View;
use Concrete\Core\Backup\ContentExporter;
use Concrete\Core\Backup\ContentImporter;
use \Concrete\Core\Attribute\Controller as AttributeTypeController;

class Controller extends AttributeTypeController
{
    protected $searchIndexFieldDefinition = array('type' => 'text', 'options' => array('length' => 4294967295, 'default' => null, 'notnull' => false));

    public function getValue()
    {
        $db = Loader::db();
        return $db->GetOne("select color from atColorpicker where avID = ?", array($this->getAttributeValueID()));
    }
    
    public function form()
    {
        $fm = Loader::helper('form');
        $form = '<div class="ccm-attribute ccm-attribute-colorpicker">';
        $this->htmlOutput($this->getValue());
        $form .= '</div>';
        print $form;
    }

    public function saveValue($color) {
        $db = Loader::db();
        $db->Replace('atColorpicker', array('avID' => $this->getAttributeValueID(), 'color' => $color), 'avID', true);
    }

    public function saveForm($data) {
        $this->saveValue($data['value']);
    }
    
    public function deleteValue() {
        $db = Loader::db();
        $db->Execute('delete from atColorpicker where avID = ?', array($this->getAttributeValueID()));
    }
    
    /**
     * This is code from Form/Service/Widget/Color
     * jQuery needs to grab the ID, which is the same as the name
     * but the name as brackets (akID[12][value]) which screws up the jQuery selector
     * so we output our own here, removing the brackets from the ID
     **/
    public function htmlOutput($value="",$options = array()) {
        $view = View::getInstance();
        $view->requireAsset('core/colorpicker');
        $form = Loader::helper('form');
        $base= str_replace("/index.php","",View::url(''));

        define('BASE',$base.'/');
        /** our hack **/
        $inputName = $this->field('value');
        $inputNameForID = str_replace("[","",$inputName);
        $inputNameForID = str_replace("]","",$inputNameForID);

        $strOptions = '';
        $i = 0;
        $defaults = array();
        $defaults['value'] = $value;
        $defaults['className'] = 'ccm-widget-colorpicker';
        $defaults['showInitial'] = true;
        $defaults['showInput'] = true;
        $defaults['allowEmpty'] = true;
        $defaults['cancelText'] = t('Cancel');
        $defaults['chooseText'] = t('Choose');
        $defaults['preferredFormat'] = 'rgb';
        $defaults['showAlpha'] = false;
        $defaults['clearText'] = t('Clear Color Selection');
        $defaults['appendTo'] = '.ui-dialog';
        $strOptions = json_encode(array_merge($defaults, $options));

?>

<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link href="<?=BASE?>icon.css" rel="stylesheet">
<style type="text/css">
      .ccm-ui input, .ccm-ui button, .ccm-ui select, .ccm-ui textarea{
        border: 1px solid #dedede;
    }
</style>
<?php print "<input type=\"hidden\" name=\"{$inputName}\" value=\"{$value}\"  id='myuniqid' />";?>
<?php  $n=0; if($value=='') { ?>
<div id="mysection0" style="border: solid 1px;padding: 24px;">
<label>Text</label>
<input type="text" name="mytext" value="" class="mytext" id="mytext0">
<label>Subtext</label>
<input type="text" name="subtext" value="" class="subtext" id="subtext0">
       <div id="icon0" class="icons"></div>
<select id="select0" name="threads-icon" data-val="0" onchange="changeIcon(0,this.value)" class="fa-select select"></select><br>
</div>
<?php $n=1;} else { ?>
<?php $str= explode("|",$value);

 $vals= explode("~",$value); 
 $m=0;

         foreach($vals as $val)
         { 
         
             if($val!=''){ ?>
            <?php $str1= explode("|",$val);?>
                <?php if($str1[0]!='undefined!#undefined' && $str1[1]!='undefined') { ?>
                    <div id="mysection<?=$n?>" style="border: solid 1px;padding: 24px;">
                        <?php $newstr1 = explode("!#",$str1[0])?>
                        <label>Text</label>
                        <input type="text" name="mytext" value="<?=$newstr1[0];?>" class="mytext" id="mytext<?=$n?>">
                        <label>Subtext</label>
                        <input type="text" name="subtext" value="<?=$newstr1[1]?>" class="subtext" id="subtext<?=$n?>">
                        
                        <div id="icon<?=$m?>" class="icons"><i id="myicondisp<?=$m?>" class="icon-<?=$str1[1];?>"></i></div> 
                        <select id="select<?=$n?>" name="threads-icon" data-val="<?=$m?>" onchange="changeIcon(<?=$n-1?>,this.value)" class="fa-select select"><option value="<?=$str1[1];?>"><?=$str1[1];?></option></select>
                        
                        <input class="btn btn-danger" onclick="delt(<?=$n?>)" type="button" value="delete">
                 </div><br>
         <?php $n++; } 
            } 
        } ?>
<?php  } ?>
<div id="addhere"></div>
<input type="button" onclick="addhtml(),load()" value="Add More" class="btn btn-primary">
<input type="button" class="btn btn-success" value="Save" onclick="done()">
<script>
    var add=0;
    var ids='';
    function delt(i){
        $("#mysection"+i).remove();
    }
    function addhtml(){
        var id=<?=$n?>;
        ids = parseInt(id+add); 
        $("#addhere").append('<br><div id="mysection'+ids+'" style="border: solid 1px;padding: 24px;"><label>Text</label><input type="text" name="mytext" value="" class="mytext"  id="mytext'+ids+'"><label>Subtext</label><input type="text" name="subtext" value="" class="subtext" id="subtext'+ids+'"><div id="icon'+ids+'"></div> <select id="select'+ids+'" onchange="changeIcon('+ids+',this.value)"  name="threads-icon"  class="fa-select select"></select><input class="btn btn-danger" onclick="delt('+ids+')" type="button" value="delete"></div></div>');
        add++;
    }
    function done(){
        var str='';
        var sign="";
        $("#myuniqid").val('');
        var numItems = $('.mytext').length
        //alert(numItems);
        for(var i=0;i<=numItems;i++)
        {   
            if(i==0){  sign="~";}else{ sign="~";}
            var vals=$("#mytext"+i).val();
            var subvals=$("#subtext"+i).val();
            if(vals!='undefined' || vals!='' || vals!=null)
            {
                vals=vals+"!#"+subvals+"|"+$("#select"+i).val();
                str=str+sign+vals;
            }
        }
       /* console.log(str);*/
        var mystr = str.replace('~undefined|undefined','');
        $("#myuniqid").val(mystr);
    }
</script>  
<script src="<?=BASE?>js-yaml.min.js"></script>
<script>
$.get('<?=BASE?>selection.yml', function(data) {
  var parsedYaml = jsyaml.load(data);
    $.each(parsedYaml.icons, function(index, icon){
    $('.select').append('<option value="' + icon.icon['tags'] + '">' + icon.icon['tags'] + '</option>');
  });
    $(".icons").html('<i class="icon-' + $('.select').val() + '"></i>');
});
function changeIcon(i,val){
  var icono = val;
        $("#myicondisp"+i).remove();
        $("#icon"+i).html('<i class="icon-' + icono + '"></i>');
}
function load(){
    $.get('<?=BASE?>selection.yml', function(data) {
  var parsedYaml = jsyaml.load(data);
    $.each(parsedYaml.icons, function(index, icon){
    $('.select').append('<option value="' + icon.icon['tags']+ '">' + icon.icon['tags'] + '</option>');
  });
    $(".icons").html('<i class="icon-' + $('.select').val() + '"></i>');
});
}
</script>
<?php } }?>