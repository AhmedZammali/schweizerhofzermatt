<?php  

namespace Concrete\Package\InvisibleRecaptcha;

use Concrete\Core\Logging\Logger;
use Package;
use Concrete\Core\Captcha\Library as CaptchaLibrary;

class Controller extends Package
{
    protected $pkgHandle = 'invisible_recaptcha';
    protected $appVersionRequired = '5.7.0.4';
    protected $pkgVersion = '1.1.1';

    protected $logger;

    public function getPackageName()
    {
        return t('Invisible reCAPTCHA');
    }

    public function getPackageDescription()
    {
        return t('Provides a Google Invisible reCAPTCHA powered captcha field.');
    }

    public function install()
    {
        $pkg = parent::install();
        CaptchaLibrary::add('invisible_recaptcha', t('Invisible reCAPTCHA'), $pkg);
        return $pkg;
    }

    /**
     * @return Logger;
     */
    public function getLogger()
    {
        if (!$this->logger) {
            $this->logger = new Logger('invisible_recaptcha', $this->getConfig()->get('debug.log_level', Logger::WARNING));
        }

        return $this->logger;
    }
}