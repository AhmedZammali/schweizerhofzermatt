<?php  

namespace Concrete\Package\InvisibleRecaptcha\Src\Captcha;

use AssetList;
use Concrete\Core\Captcha\Controller as CaptchaController;
use Concrete\Core\Http\ResponseAssetGroup;
use Package;
use Concrete\Core\Utility\IPAddress;
use Config;
use Core;
use Log;

class InvisibleRecaptchaController extends CaptchaController
{

    public function saveOptions($data)
    {   
        
        $config = Package::getByHandle('invisible_recaptcha')->getConfig();
        $config->save('captcha.site_key', $data['site']);
        $config->save('captcha.secret_key', $data['secret']);
        $config->save('captcha.theme', $data['theme']);
        $config->save('captcha.language', $data['language']);
    }

    /**
     * Shows an input for a particular captcha library
     */
    function showInput()
    {
         $config = Package::getByHandle('invisible_recaptcha')->getConfig();

        ?>

        <input type="hidden" name="site_key" id="site_key" value="<?=$config->get('captcha.site_key')?>">
        <?php
        $config = Package::getByHandle('invisible_recaptcha')->getConfig();

        $assetList = AssetList::getInstance();

        $lang = $config->get('captcha.language');
        
        $assetUrl = 'https://www.google.com/recaptcha/api.js?render='.$config->get('captcha.site_key').'';
        if ($lang !== 'auto') {
            $assetUrl .= '&hl=' . $lang;
        }
        $assetList->register('javascript', 'invisible_recaptcha_api', $assetUrl, array('local' => false));

        $assetList->register('javascript', 'invisible_recaptcha_render', 'assets/js/render.js', array(), 'invisible_recaptcha');

        $assetList->registerGroup(
            'invisible_recaptcha',
            array(
                array('javascript', 'invisible_recaptcha_render'),
                array('javascript', 'invisible_recaptcha_api'),
            )
        );

        $responseAssets = ResponseAssetGroup::get();
        $responseAssets->requireAsset('invisible_recaptcha');
        echo '<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">' ;    ?>
        <script src="https://www.google.com/recaptcha/api.js?render=<?php echo $config->get('captcha.site_key'); ?>"></script> 
        <script>      
            
          grecaptcha. ready ( function () {
            grecaptcha. execute ( '<?php echo $config->get('captcha.site_key'); ?>' , { action: 'homepage' } )
        . then ( function (token) {
            document. getElementById ( 'g-recaptcha-response' ) .value = token;
            console.log('t' + token);
        } );
} );
      
        </script>
                  
   <?php }

    /**
     * Displays the graphical portion of the captcha
     */
    function display()
    {
        return '';
    }

    /**
     * Displays the label for this captcha library
     */
    function label()
    {
        return '';
    }

    /**
     * Verifies the captcha submission
     * @return bool
     */
    public function check()
    {
        $pkg = Package::getByHandle('invisible_recaptcha');
        $config = $pkg->getConfig();
        /** @var \Concrete\Core\Permission\IPService $iph */
        $iph = Core::make('helper/validation/ip');

        $qsa = http_build_query(
            array(
                'secret' => $config->get('captcha.secret_key'),
                'response' => $_REQUEST['g-recaptcha-response'],
                'remoteip' => $iph->getRequestIP()->getIp(IPAddress::FORMAT_IP_STRING)
            )
        );

        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify?' . $qsa);

        if (Config::get('concrete.proxy.host') != null) {
            curl_setopt($ch, CURLOPT_PROXY, Config::get('concrete.proxy.host'));
            curl_setopt($ch, CURLOPT_PROXYPORT, Config::get('concrete.proxy.port'));

            // Check if there is a username/password to access the proxy
            if (Config::get('concrete.proxy.user') != null) {
                curl_setopt(
                    $ch,
                    CURLOPT_PROXYUSERPWD,
                    Config::get('concrete.proxy.user') . ':' . Config::get('concrete.proxy.password')
                );
            }
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, Config::get('app.curl.verifyPeer'));

        $response = curl_exec($ch);
        if ($response !== false) {
            $data = json_decode($response, true);
            if (isset($data['error-codes']) && (in_array('missing-input-secret', $data['error-codes']) || in_array('invalid-input-secret', $data['error-codes']))) {
                $pkg->getLogger()->addError(t('The reCAPTCHA secret parameter is invalid or malformed.'));
            }
            return $data['success'];
        } else {
            $pkg->getLogger()->addError(t('Error loading reCAPTCHA: %s', curl_error($ch)));
            return false;
        }
    }
}